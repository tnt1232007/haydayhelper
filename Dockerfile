# Stage 1
FROM microsoft/aspnetcore-build AS builder
WORKDIR /source
COPY . .
RUN dotnet restore HDH.Api/HDH.Api.csproj
RUN dotnet publish HDH.Api/HDH.Api.csproj --output /app/ --configuration Release

# Stage 2
FROM microsoft/aspnetcore
WORKDIR /app
COPY --from=builder /app .
ENTRYPOINT ["dotnet", "HDH.Api.dll"]