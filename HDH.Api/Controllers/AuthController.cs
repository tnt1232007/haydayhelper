﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Threading;
using System.Threading.Tasks;
using HDH.Api.ViewModel.Users;
using HDH.Common.Constants;
using HDH.Common.Extensions;
using HDH.Common.Settings;
using HDH.Core.Users;
using HDH.Service.Auths;
using HDH.Service.Emails;
using HDH.Service.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace HDH.Api.Controllers
{
    [AllowAnonymous]
    [Route("api/[controller]")]
    public class AuthController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IAuthService _authService;
        private readonly IEmailService _emailService;
        private readonly AppSettings _appSettings;

        public AuthController(IUserService userService, IAuthService authService, IEmailService emailService, IOptions<AppSettings> appSettings)
        {
            _userService = userService;
            _authService = authService;
            _emailService = emailService;
            _appSettings = appSettings.Value;
        }

        /// <summary>
        /// Register new user
        /// </summary>
        /// <param name="request">Register request</param>
        /// <returns>User data</returns>
        [HttpPut("[action]")]
        [ProducesResponseType(typeof(UserDto), 200)]
        public async Task<IActionResult> Register([FromBody] RegisterRequest request)
        {
            var entity = new User
            {
                UserName = request.Email,
                Email = request.Email,
                Enabled = true,
                Roles = new[] {Roles.User}
            };
            entity = await _userService.CreateAsync(entity, request.Password);
            return Ok(entity.Id);
        }

        /// <summary>
        /// Request token to login
        /// </summary>
        /// <param name="request">Login request</param>
        /// <param name="token"></param>
        /// <returns>User token</returns>
        [HttpPost("[action]")]
        [ProducesResponseType(typeof(UserTokenDto), 200)]
        public async Task<IActionResult> Login([FromBody] LoginRequest request, CancellationToken token)
        {
            var entity = await _userService.ValidateUsernamePasswordAsync(request.Email, request.Password, token);
            await _userService.UpdateLastLogin(entity);
            var securityToken = await _authService.GetJwtSecurityToken(entity);
            return Ok(new UserTokenDto
            {
                TokenType = "JWT Bearer",
                AccessToken = new JwtSecurityTokenHandler().WriteToken(securityToken),
                ExpiresIn = (int) (securityToken.ValidTo - DateTime.UtcNow).TotalSeconds
            });
        }

        /// <summary>
        /// [Authorize] Register new user
        /// </summary>
        /// <param name="dto">User data</param>
        /// <param name="token"></param>
        /// <returns>User token</returns>
        [Authorize]
        [HttpPost("[action]")]
        [ProducesResponseType(typeof(UserTokenDto), 200)]
        public async Task<IActionResult> LoginWithProvider([FromBody] UserDto dto, CancellationToken token)
        {
            var entity = await _userService.GetAsync(dto.Id, token);
            if (entity == null)
            {
                entity = new User
                {
                    Id = dto.Id,
                    UserName = dto.Email,
                    Email = dto.Email,
                    Enabled = dto.Enabled,
                    Name = dto.Name,
                    Dob = dto.Dob.FromUnixTimeSeconds(),
                    Address = dto.Address,
                    Avatar = dto.Avatar,
                    City = dto.City,
                    Country = dto.Country,
                    Phone = dto.Phone,
                    Roles = dto.Roles,
                    Level = dto.Level
                };
                entity = await _userService.CreateAsync(entity);
            }

            await _userService.UpdateLastLogin(entity);
            var securityToken = await _authService.GetJwtSecurityToken(entity);
            return Ok(new UserTokenDto
            {
                TokenType = "JWT Bearer",
                AccessToken = new JwtSecurityTokenHandler().WriteToken(securityToken),
                ExpiresIn = (int)(securityToken.ValidTo - DateTime.UtcNow).TotalSeconds
            });
        }

        /// <summary>
        /// Request token to reset password through email
        /// </summary>
        /// <param name="request">Forgot password request</param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> ForgotPassword([FromBody] ForgotPasswordRequest request)
        {
            var token = await _userService.GeneratePasswordResetTokenAsync(request.Email);
            var resetLink = $"{_appSettings.BaseUrls.Web}/reset-password?token={token}";
            await _emailService.SendResetPasswordAsync(request.Email, resetLink);

            return Ok();
        }

        /// <summary>
        /// Change password using token
        /// </summary>
        /// <param name="request">Reset password request</param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> ResetPassword([FromBody] ResetPasswordRequest request)
        {
            await _userService.ResetPasswordAsync(request.Email, request.Token, request.Password);
            return Ok();
        }
    }
}