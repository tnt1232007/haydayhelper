﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using HDH.Api.ViewModel.Products;
using HDH.Common.Extensions;
using HDH.Service.Products;
using Microsoft.AspNetCore.Mvc;

namespace HDH.Api.Controllers
{
    [Route("api/[controller]")]
    public class ChartController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IProductService _productService;

        public ChartController(IMapper mapper, IProductService productService)
        {
            _mapper = mapper;
            _productService = productService;
        }

        /// <summary>
        /// Calculate product usage (usage count and time required)
        /// </summary>
        /// <returns>List of product usage</returns>
        [HttpGet("[action]")]
        [ProducesResponseType(typeof(List<ProductUsageDto>), 200)]
        public async Task<IActionResult> CalcProductUsage(CancellationToken token)
        {
            var allProducts = await _productService.AllReadOnlyAsync(token);
            var allIngredients = (await _productService.GetIngredientsAsync(token: token)).ToList();
            var allProductRatings = allProducts.Select(o => _mapper.Map<ProductUsageDto>(o)).ToList();
            var remainingIngredients = allProductRatings;

            List<ProductUsageDto> CalcProductRating(IEnumerable<ProductUsageDto> products)
            {
                var ingredients = allIngredients.Where(o => products.Select(m => m.Key).Contains(o.ProductKey)).ToList();
                return ingredients
                    .Join(products, l => l.ProductKey, r => r.Key, (l, r) => new ProductUsageDto {Key = l.IngredientKey, UsedCount = l.Quantity * r.UsedCount})
                    .GroupBy(o => o.Key)
                    .Select(o => new ProductUsageDto {Key = o.Key, UsedCount = o.Sum(m => m.UsedCount)})
                    .ToList();
            }

            while (remainingIngredients.Count != 0)
            {
                remainingIngredients = CalcProductRating(remainingIngredients);
                allProductRatings = allProductRatings.LeftJoin(remainingIngredients, l => l.Key, r => r.Key,
                    (l, r) => _mapper.Map<ProductUsageDto>(l).SetUsedCount(l.UsedCount + (r?.UsedCount ?? 0))).ToList();
            }

            allProductRatings.ForEach(o => o.TimeRequired = new TimeSpan((long) (o.TimeRequired ?? 0)).TotalHours * o.UsedCount);
            return Ok(allProductRatings);
        }

        /// <summary>
        /// Calculate product relationship (node:product and edge:relationship)
        /// </summary>
        /// <returns>Product Relationship</returns>
        [HttpGet("[action]")]
        [ProducesResponseType(typeof(ProductRelationshipDto), 200)]
        public async Task<IActionResult> CalcProductRelationship(CancellationToken token)
        {
            var allProducts = await _productService.AllReadOnlyAsync(token);
            var allIngredients = await _productService.GetIngredientsAsync(token: token);
            var relationshipDto = new ProductRelationshipDto
            {
                Nodes = allProducts.Select(o => new ProductUsageDto {Key = o.Key, Name = o.Name, SourceKey = o.SourceKey}).ToList(),
                Edges = allIngredients.Select(o => new ProductIngredientDto {ProductKey = o.ProductKey, IngredientKey = o.IngredientKey, Quantity = o.Quantity}).ToList()
            };
            foreach (var group in relationshipDto.Nodes.GroupBy(o => o.SourceKey))
            {
                var edges = group.Pairwise((l, r) => new ProductIngredientDto {ProductKey = l.Key, IngredientKey = r.Key}).ToList();
                relationshipDto.Edges.AddRange(edges);
                if (group.Count() > 2)
                    relationshipDto.Edges.Add(new ProductIngredientDto {IngredientKey = edges.First().ProductKey, ProductKey = edges.Last().IngredientKey});
            }
            return Ok(relationshipDto);
        }
    }
}