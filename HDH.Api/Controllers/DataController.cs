﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using HDH.Common.Constants;
using HDH.Common.Extensions;
using HDH.Core.Products;
using HDH.Core.Sources;
using HDH.Service.Csv;
using HDH.Service.Products;
using HDH.Service.Sources;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HDH.Api.Controllers
{
    [Authorize(Roles = Roles.Administrator)]
    [Route("api/[controller]")]
    public class DataController : ControllerBase
    {
        private readonly ICsvService _csvService;
        private readonly IMapper _mapper;
        private readonly IProductService _productService;
        private readonly ISourceService _sourceService;

        public DataController(IMapper mapper, ICsvService csvService, IProductService productService, ISourceService sourceService)
        {
            _mapper = mapper;
            _csvService = csvService;
            _productService = productService;
            _sourceService = sourceService;
        }

        /// <summary>
        /// [Authorize(Administrator)] Reset data and Import new data from csv files
        /// </summary>
        /// <param name="sourceFile"></param>
        /// <param name="productFile"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        [ProducesResponseType(typeof(string), 200)]
        public async Task<IActionResult> Import([FromForm] IFormFile sourceFile, [FromForm] IFormFile productFile)
        {
            if (sourceFile == null || productFile == null)
                return BadRequest();

            await _productService.DeleteAllAsync(User);
            await _sourceService.DeleteAllAsync(User);
            try
            {
                using (var stream = sourceFile.OpenReadStream())
                {
                    var sources = _csvService.Read<Source>(stream);
                    await _sourceService.ImportAsync(User, sources);
                }

                using (var stream = productFile.OpenReadStream())
                {
                    var products = _csvService.Read<Product>(stream);
                    await _productService.ImportAsync(User, products);
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                throw;
            }
            return Ok();
        }

        /// <summary>
        /// [Authorize(Administrator)] Export products as csv file
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> ExportProducts(CancellationToken token)
        {
            var products = await _productService.ExportAsync(User, token);
            var records = _mapper.Map<List<CsvProductExport>>(products).Select(o => o.ToExpando()).ToList();

            // Setup headers
            IDictionary<string, object> record = records.FirstOrDefault();
            if (record != null)
            {
                for (var i = 0; i < 4; i++)
                {
                    record[$"{nameof(ProductIngredient.Ingredient)} {i + 1}"] = null;
                    record[$"{nameof(ProductIngredient.Quantity)} {i + 1}"] = null;
                }
            }

            // Setup dynamic columns
            for (var i = 0; i < records.Count; i++)
            {
                record = records[i];
                var ingredients = products[i].Ingredients.ToList();
                for (var j = 0; j < ingredients.Count; j++)
                {
                    record[$"{nameof(ProductIngredient.Ingredient)} {j + 1}"] = ingredients[j].IngredientKey;
                    record[$"{nameof(ProductIngredient.Quantity)} {j + 1}"] = ingredients[j].Quantity;
                }
            }

            var bytes = _csvService.Write(records);
            return File(bytes, "text/csv", $"{nameof(Product)}s.csv");
        }

        /// <summary>
        /// [Authorize(Administrator)] Export sources as csv file
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> ExportSources(CancellationToken token)
        {
            var sources = await _sourceService.ExportAsync(User, token);
            var records = _mapper.Map<List<CsvSourceExport>>(sources).Select(o => o.ToExpando()).ToList();

            // Setup headers
            IDictionary<string, object> record = records.FirstOrDefault();
            if (record != null)
            {
                for (var i = 0; i < 7; i++)
                {
                    record[$"{nameof(SourceInstance.Cost)} {i + 1}"] = null;
                    record[$"{nameof(SourceInstance.BuildTime)} {i + 1}"] = null;
                }
            }

            // Setup dynamic columns
            for (var i = 0; i < records.Count; i++)
            {
                record = records[i];
                var sourceInstances = sources[i].SourceInstances.ToList();
                for (var j = 0; j < sourceInstances.Count; j++)
                {
                    record[$"{nameof(SourceInstance.Cost)} {j + 1}"] = sourceInstances[j].Cost;
                    record[$"{nameof(SourceInstance.BuildTime)} {j + 1}"] = sourceInstances[j].BuildTime;
                }
            }

            var bytes = _csvService.Write(records);
            return File(bytes, "text/csv", $"{nameof(Source)}s.csv");
        }
    }
}