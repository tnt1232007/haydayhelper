﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using HDH.Common.Constants;
using HDH.Common.RestApi;
using HDH.Common.Settings;
using HDH.Common.Views;
using HDH.Core.Static;
using HDH.Service.Products;
using HDH.Service.Sources;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace HDH.Api.Controllers
{
    [Route("api/[controller]")]
    public class GlobalController : ControllerBase
    {
        private readonly AppSettings _appSettings;
        private readonly IProductService _productService;
        private readonly ISourceService _sourceService;

        public GlobalController(IProductService productService, ISourceService sourceService, IOptions<AppSettings> appSettings)
        {
            _productService = productService;
            _sourceService = sourceService;
            _appSettings = appSettings.Value;
        }

        /// <summary>
        /// Get all countries as options
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        [ProducesResponseType(typeof(List<CheckBoxModel>), 200)]
        public async Task<IActionResult> CountryOptions([FromQuery] string keyword, CancellationToken token)
        {
            keyword = keyword?.Trim().ToLower() ?? string.Empty;
            var result = await RestClient.GetAsync<List<Country>>(_appSettings.CountryApi, token);
            var list = result.Where(o => o.Alpha3Code.ToLower() == keyword ||
                                         o.Name.ToLower().Contains(keyword) ||
                                         o.Region.ToLower().Contains(keyword))
                .OrderBy(o => o.Name).Select(o => new CheckBoxModel(o.Alpha3Code, o.Name, o.Region, o.Flag));
            return Ok(list);
        }

        /// <summary>
        /// Get all products as options
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        [ProducesResponseType(typeof(List<CheckBoxModel>), 200)]
        public async Task<IActionResult> ProductOptions(CancellationToken token)
        {
            var entities = await _productService.AllReadOnlyAsync(token);
            return Ok(entities.Select(o => new CheckBoxModel(o.Key, o.Name, o.SourceKey, null, true)));
        }

        /// <summary>
        /// Get all sources as options
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        [ProducesResponseType(typeof(List<CheckBoxModel>), 200)]
        public async Task<IActionResult> SourceOptions(CancellationToken token)
        {
            var entities = await _sourceService.AllReadOnlyAsync(token);
            return Ok(entities.Select(o => new CheckBoxModel(o.Key, o.Name, null, null, true)));
        }

        /// <summary>
        /// Get all permissions as options
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="token"></param>
        /// <returns>List of permissions</returns>
        [HttpGet("[action]")]
        [ProducesResponseType(typeof(List<CheckBoxModel>), 200)]
        public IActionResult PermissionOptions([FromQuery] string keyword, CancellationToken token)
        {
            keyword = keyword?.ToLower() ?? string.Empty;
            var list = Enum.GetNames(typeof(Permission))
                .Where(o => o.ToLower().Contains(keyword))
                .Select(o => new CheckBoxModel(o, Regex.Replace(o, "([a-z])([A-Z])", "$1 $2")));
            return Ok(list);
        }
    }
}