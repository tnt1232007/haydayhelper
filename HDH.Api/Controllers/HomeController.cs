﻿using Microsoft.AspNetCore.Mvc;

namespace HDH.Api.Controllers
{
    [Route("")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class HomeController : ControllerBase
    {
        /// <summary>
        ///     Redirects to the swagger page.
        /// </summary>
        /// <returns>A 301 Moved Permanently response.</returns>
        [HttpGet]
        public IActionResult Index() => RedirectPermanent("/swagger");
    }
}