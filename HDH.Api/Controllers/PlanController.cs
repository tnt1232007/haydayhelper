﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using HDH.Api.ViewModel.Plans;
using HDH.Api.ViewModel.Products;
using HDH.Common.Comparer;
using HDH.Common.Extensions;
using HDH.Core.Plans;
using HDH.Service.Plans;
using HDH.Service.Products;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace HDH.Api.Controllers
{
    [Route("api/[controller]s")]
    public class PlanController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IPlanService _planService;
        private readonly IProductService _productService;

        public PlanController(IMapper mapper, IPlanService planService, IProductService productService)
        {
            _mapper = mapper;
            _planService = planService;
            _productService = productService;
        }

        /// <summary>
        /// [Authorize] [PlanRead] Get all user plans
        /// </summary>
        /// <param name="token"></param>
        /// <returns>List of plans detail</returns>
        [Authorize]
        [HttpGet]
        [ProducesResponseType(typeof(List<PlanDto>), 200)]
        public async Task<IActionResult> Get(CancellationToken token)
        {
            // Get all
            var plans = await _planService.GetAllAsync(User, token);

            // Map
            var planDtos = _mapper.Map<List<PlanDto>>(plans);
            return Ok(planDtos);
        }

        /// <summary>
        /// Calculate plan required ingredients
        /// </summary>
        /// <remarks>[ProductPreferenceRead] if logged in</remarks>
        /// <param name="dto"></param>
        /// <param name="token"></param>
        /// <returns>List of required ingredients</returns>
        [HttpPost(nameof(Calculate))]
        [ProducesResponseType(typeof(PlanResultDto), 200)]
        public async Task<IActionResult> Calculate([FromBody] PlanDto dto, CancellationToken token)
        {
            var products = dto.Products.Select(o => Tuple.Create(o.Key, o.Quantity)).ToList();
            var allIngredients = (await _productService.GetIngredientsAsync(token: token)).ToList();
            var allProducts = await _productService.Query().Include(o => o.Source).ToListAsync(token);
            await _productService.LoadAllPreferencesAsync(User, token);
            var allProductDtos = Mapper.Map<List<ProductExtendDto>>(allProducts);

            IEnumerable<Tuple<string, double>> ExtractIngredients(IEnumerable<Tuple<string, double>> list)
            {
                return list
                    .GroupJoin(allIngredients, l => l.Item1, r => r.ProductKey, (l, r) => new {l, r})
                    .SelectMany(o => o.r.DefaultIfEmpty(), (o, r) => r != null ? Tuple.Create(r.IngredientKey, o.l.Item2 * r.Quantity) : o.l)
                    .GroupBy(o => o.Item1).Select(o => Tuple.Create(o.Key, o.Sum(m => m.Item2)));
            }

            List<PlanProductDto> MapToIngredientDto(IEnumerable<Tuple<string, double>> list)
            {
                return list
                    .Join(allProductDtos, l => l.Item1, r => r.Key, (l, r) => new PlanProductDto
                    {
                        PlanId = dto.Id,
                        Key = r.Key,
                        Quantity = l.Item2
                    })
                    .OrderByDescending(o => o.Quantity)
                    .ToList();
            }

            List<PlanSourceDto> MapToProductionTimeDto(IEnumerable<Tuple<string, double>> list)
            {
                return list
                    .Join(allProductDtos, l => l.Item1, r => r.Key, (l, r) => new
                    {
                        r.Source?.Key,
                        ProductionTime = r.RealProductionTimeTicks.HasValue ? r.RealProductionTimeTicks.Value * l.Item2 : 0
                    })
                    .GroupBy(o => o.Key)
                    .Select(o => new PlanSourceDto
                    {
                        PlanId = dto.Id,
                        Key = o.Key,
                        ProductionTime = new TimeSpan((long) o.Sum(m => m.ProductionTime))
                    })
                    .OrderByDescending(o => o.ProductionTime)
                    .ToList();
            }

            // Extract direct ingredients (1 level)
            var directIngredients = ExtractIngredients(products);

            // Extract core ingredients (all levels)
            var coreIngredients = products.ToList();
            while (coreIngredients.Any(o => allIngredients.Any(m => m.ProductKey == o.Item1)))
                coreIngredients = ExtractIngredients(coreIngredients).ToList();
            
            return Ok(new PlanResultDto
            {
                PlanId = dto.Id,
                DirectIngredients = MapToIngredientDto(directIngredients),
                CoreIngredients = MapToIngredientDto(coreIngredients),
                ProductionTimes = MapToProductionTimeDto(products)
            });
        }

        /// <summary>
        /// [Authorize] [PlanCreate] Create new plan
        /// </summary>
        /// <param name="dto"></param>
        /// <returns>Plan</returns>
        [Authorize]
        [HttpPost]
        [ProducesResponseType(typeof(PlanDto), 201)]
        public async Task<IActionResult> Post([FromBody] PlanDto dto)
        {
            var entity = new Plan
            {
                UserId = User.GetId(),
                PlanProducts = dto.Products.Select(o => new PlanProduct
                {
                    ProductKey = o.Key,
                    Quantity = o.Quantity
                }).ToList()
            };
            entity = await _planService.CreateAsync(User, entity);
            return Ok(_mapper.Map<PlanDto>(entity));
        }

        /// <summary>
        /// [Authorize] [PlanRead] [PlanUpdate] Update plan
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dto"></param>
        /// <returns>Plan</returns>
        [Authorize]
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(PlanDto), 200)]
        public async Task<IActionResult> Put([FromRoute] int id, [FromBody] PlanDto dto)
        {
            var exist = await _planService.GetAsync(User, id);
            if (exist == null)
                return NotFound();

            var dtoPlanProducts = dto.Products.Select(o => new PlanProduct
            {
                ProductKey = o.Key,
                Quantity = o.Quantity
            }).ToList();

            // Delete
            foreach (var planProduct in exist.PlanProducts.Except(dtoPlanProducts, new PlanProductComparer()).ToList())
                exist.PlanProducts.Remove(planProduct);

            // Update
            foreach (var pair in exist.PlanProducts.Join(dtoPlanProducts, left => left.ProductKey, right => right.ProductKey, (left, right) => new {Exist = left, Entity = right}))
                pair.Exist.Quantity = pair.Entity.Quantity;

            // Insert
            foreach (var planProduct in dtoPlanProducts.Except(exist.PlanProducts, new PlanProductComparer()))
                exist.PlanProducts.Add(planProduct);

            exist = await _planService.UpdateAsync(User, exist);
            return Ok(_mapper.Map<PlanDto>(exist));
        }

        /// <summary>
        /// [Authorize] [PlanRead] [PlanDelete] Delete plan
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Plan</returns>
        [Authorize]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete([FromRoute] int id)
        {
            var exist = await _planService.GetAsync(User, id);
            if (exist == null)
                return NotFound();

            await _planService.DeleteAsync(User, exist);
            return Ok();
        }
    }
}