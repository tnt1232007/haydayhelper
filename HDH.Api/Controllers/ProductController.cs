﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using HDH.Api.ViewModel.Products;
using HDH.Common.Exceptions;
using HDH.Common.Extensions;
using HDH.Common.Pagings;
using HDH.Core.Products;
using HDH.Service.Products;
using HDH.Service.Sources;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HDH.Api.Controllers
{
    [Route("api/[controller]s")]
    public class ProductController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IProductService _productService;
        private readonly ISourceService _sourceService;

        public ProductController(IMapper mapper, IProductService productService, ISourceService sourceService)
        {
            _mapper = mapper;
            _productService = productService;
            _sourceService = sourceService;
        }

        /// <summary>
        /// Get products
        /// </summary>
        /// <remarks>[ProductPreferenceRead] if logged in</remarks>
        /// <param name="pagingDto"></param>
        /// <param name="keyword">Keyword</param>
        /// <param name="token"></param>
        /// <returns>Paging results contain list of products</returns>
        [HttpGet]
        [ProducesResponseType(typeof(PagingList<ProductExtendDto>), 200)]
        public async Task<IActionResult> Get([FromQuery] string keyword, [FromQuery] PagingDto pagingDto, CancellationToken token)
        {
            var paging = _mapper.Map<Paging>(pagingDto);
            try
            {
                keyword = keyword ?? string.Empty;
                var parts = keyword.Split(' ', ',').Select(o => new
                {
                    Expression = o,
                    IsFilter = new Paging {Filter = o}.ExtractFilters(out _, out _)
                }).ToList();
                var extractKeyword = string.Join(' ', parts.Where(o => !o.IsFilter).Select(o => o.Expression));
                paging.Filter = string.Join(',', parts.Where(o => o.IsFilter).Select(o => o.Expression));

                // Get by keyword
                var products = await _productService.QueryByKeywordAsync(extractKeyword, paging, token);

                // Get preference
                await _productService.LoadAllPreferencesAsync(User, token);

                // Map
                var productDtos = _mapper.Map<List<ProductExtendDto>>(products);
                return Ok(new PagingList<ProductExtendDto>(productDtos, paging) { Keyword = keyword + paging.Filter });
            }
            catch (PagingException)
            {
                return Ok(new PagingList<ProductExtendDto>(new List<ProductExtendDto>(), paging) { Keyword = keyword });
            }
        }

        /// <summary>
        /// Get products by source
        /// </summary>
        /// <remarks>[ProductPreferenceRead] if logged in</remarks>
        /// <param name="sourceKey"></param>
        /// <param name="sort"></param>
        /// <param name="token"></param>
        /// <returns>Paging results contain list of products</returns>
        [HttpGet("/api/Sources/{sourceKey}/[controller]s")]
        [ProducesResponseType(typeof(PagingList<ProductExtendDto>), 200)]
        public async Task<IActionResult> GetBySource([FromRoute] string sourceKey, [FromQuery] string sort, CancellationToken token)
        {
            var paging = new Paging { Sort = sort };

            //Check if source exist
            var source = await _sourceService.GetAsync(sourceKey, token);
            if (source == null)
                return NotFound();
            
            // Get by source
            var products = await _productService.QueryBySourceAsync(sourceKey, paging, token);

            // Get preference
            await _productService.LoadAllPreferencesAsync(User, token);

            // Map
            var productDtos = _mapper.Map<List<ProductExtendDto>>(products);
            return Ok(new PagingList<ProductExtendDto>(productDtos, paging));
        }

        /// <summary>
        /// Get product detail
        /// </summary>
        /// <remarks>[ProductPreferenceRead] if logged in</remarks>
        /// <param name="key"></param>
        /// <param name="token"></param>
        /// <returns>Product detail</returns>
        [HttpGet("{key}")]
        [ProducesResponseType(typeof(ProductDetailDto), 200)]
        public async Task<IActionResult> Get([FromRoute] string key, CancellationToken token)
        {
            // Get product detail
            var product = await _productService.GetAsync(key, token);
            if (product == null)
                return NotFound();

            // Get related products
            var relatives = await _productService.GetRelativesAsync(product, token);

            // Get preference
            await _productService.LoadAllPreferencesAsync(User, token);

            // Map
            var productDetailDto = _mapper.Map<ProductDetailDto>(product);
            productDetailDto.Relatives = _mapper.Map<List<ProductDto>>(relatives);
            return Ok(productDetailDto);
        }

        /// <summary>
        /// [Authorize] [ProductCreate] [ProductManage] Create or update product
        /// </summary>
        /// <param name="key"></param>
        /// <param name="dto"></param>
        /// <returns>Product detail</returns>
        [Authorize]
        [HttpPost("{key}")]
        [ProducesResponseType(typeof(ProductDto), 200)]
        public async Task<IActionResult> Post([FromRoute] string key, [FromBody] ProductDto dto)
        {
            var entity = _mapper.Map<Product>(dto);
            entity.Key = key;
            entity = await _productService.CreateOrUpdateAsync(User, entity);
            return Ok(_mapper.Map<ProductDetailDto>(entity));
        }

        /// <summary>
        /// [Authorize] [ProductPreferenceUpdate] Update product preference
        /// </summary>
        /// <param name="key"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("[action]/{key}")]
        public async Task<IActionResult> Preference([FromRoute] string key, [FromBody] ProductPreferenceDto dto)
        {
            return Ok(await _productService.UpdatePreferenceAsync(User, key, _mapper.Map<ProductPreference>(dto)));
        }
    }
}