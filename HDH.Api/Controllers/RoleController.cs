﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using HDH.Api.ViewModel.Users;
using HDH.Core.Users;
using HDH.Service.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HDH.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]s")]
    public class RoleController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IRoleService _roleService;

        public RoleController(IMapper mapper, IRoleService roleService)
        {
            _mapper = mapper;
            _roleService = roleService;
        }

        /// <summary>
        /// [Authorize] [RoleManage] Get all role data
        /// </summary>
        /// <returns>List of role data</returns>
        [HttpGet]
        [ProducesResponseType(typeof(List<RoleDto>), 200)]
        public async Task<IActionResult> Get(CancellationToken token)
        {
            var entity = await _roleService.GetAllAsync(User, token);
            return Ok(_mapper.Map<List<RoleDto>>(entity));
        }

        /// <summary>
        /// [Authorize] [RoleRead] Get role data
        /// </summary>
        /// <param name="name">Role name</param>
        /// <param name="token"></param>
        /// <returns>Role data</returns>
        [HttpGet("{name}")]
        [ProducesResponseType(typeof(RoleDto), 200)]
        public async Task<IActionResult> Get([FromRoute] string name, CancellationToken token)
        {
            var entity = await _roleService.GetAsync(User, name, token);
            if (entity == null)
                return NotFound();

            return Ok(_mapper.Map<RoleDto>(entity));
        }

        /// <summary>
        /// [Authorize] [RoleCreate] Create new role
        /// </summary>
        /// <param name="dto">Role data</param>
        /// <returns>Role data</returns>
        [HttpPost]
        [ProducesResponseType(typeof(RoleDto), 201)]
        public async Task<IActionResult> Post([FromBody] RoleDto dto)
        {
            if (string.IsNullOrEmpty(dto.Name))
            {
                ModelState.AddModelError(nameof(dto.Name), "The Name field is required.");
                return BadRequest(ModelState);
            }

            var entity = new Role
            {
                Id = dto.Name,
                Name = dto.Name,
                Permissions = dto.Permissions
            };
            entity = await _roleService.CreateAsync(User, entity);
            return Ok(_mapper.Map<RoleDto>(entity));
        }

        /// <summary>
        /// [Authorize] [RoleRead] [RoleManage] Update role data
        /// </summary>
        /// <param name="name"></param>
        /// <param name="dto"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPut("{name}")]
        [ProducesResponseType(typeof(RoleDto), 200)]
        public async Task<IActionResult> Put([FromRoute] string name, [FromBody] RoleDto dto, CancellationToken token)
        {
            var entity = await _roleService.GetAsync(User, name, token);
            if (entity == null)
                return NotFound();

            entity.Permissions = dto.Permissions;
            await _roleService.UpdateAsync(User, entity);
            return Ok(_mapper.Map<RoleDto>(entity));
        }

        /// <summary>
        /// [Authorize] [RoleRead] [RoleDelete] Delete role
        /// </summary>
        /// <param name="name">Role name</param>
        /// <returns></returns>
        [HttpDelete("{name}")]
        public async Task<IActionResult> Delete([FromRoute] string name)
        {
            var entity = await _roleService.GetAsync(User, name);
            if (entity == null)
                return NotFound();

            await _roleService.DeleteAsync(User, entity);
            return Ok();
        }
    }
}