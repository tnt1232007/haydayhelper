using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using HDH.Api.ViewModel.Sources;
using HDH.Common.Exceptions;
using HDH.Common.Extensions;
using HDH.Common.Pagings;
using HDH.Core.Sources;
using HDH.Service.Sources;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HDH.Api.Controllers
{
    [Route("api/[controller]s")]
    public class SourceController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ISourceService _sourceService;

        public SourceController(IMapper mapper, ISourceService sourceService)
        {
            _mapper = mapper;
            _sourceService = sourceService;
        }

        /// <summary>
        /// Get sources
        /// </summary>
        /// <remarks>[SourcePreferenceRead] if logged in</remarks>
        /// <param name="pagingDto"></param>
        /// <param name="keyword"></param>
        /// <param name="token"></param>
        /// <returns>Paging results contain list of sources</returns>
        [HttpGet]
        [ProducesResponseType(typeof(PagingList<SourceExtendDto>), 200)]
        public async Task<IActionResult> Get([FromQuery] string keyword, [FromQuery] Paging pagingDto, CancellationToken token)
        {
            var paging = _mapper.Map<Paging>(pagingDto);
            try
            {
                keyword = keyword ?? string.Empty;
                var parts = keyword.Split(' ', ',').Select(o => new
                {
                    Expression = o,
                    IsFilter = new Paging {Filter = o}.ExtractFilters(out var _, out var _)
                }).ToList();
                var extractKeyword = string.Join(' ', parts.Where(o => !o.IsFilter).Select(o => o.Expression));
                paging.Filter = string.Join(',', parts.Where(o => o.IsFilter).Select(o => o.Expression));

                // Get by keyword
                var sources = await _sourceService.QueryByKeywordAsync(extractKeyword, paging, token);

                // Get preference
                await _sourceService.LoadAllPreferencesAsync(User, token);

                // Map
                var sourceDtos = _mapper.Map<List<SourceExtendDto>>(sources);
                return Ok(new PagingList<SourceExtendDto>(sourceDtos, paging) {Keyword = keyword});
            }
            catch (PagingException)
            {
                return Ok(new PagingList<SourceExtendDto>(new List<SourceExtendDto>(), paging) {Keyword = keyword});
            }
        }

        /// <summary>
        /// Get source detail
        /// </summary>
        /// <remarks>[SourcePreferenceRead] if logged in</remarks>
        /// <param name="key"></param>
        /// <param name="token"></param>
        /// <returns>Source detail</returns>
        [HttpGet("{key}")]
        [ProducesResponseType(typeof(SourceDetailDto), 200)]
        public async Task<IActionResult> Get([FromRoute] string key, CancellationToken token)
        {
            // Get source detail
            var source = await _sourceService.GetAsync(key, token);
            if (source == null)
                return NotFound();
            
            // Get preference
            await _sourceService.LoadAllPreferencesAsync(User, token);
            source.Products = null;

            // Map
            var sourceDetailDto = _mapper.Map<SourceDetailDto>(source);

            return Ok(sourceDetailDto);
        }

        /// <summary>
        /// [Authorize] [SourceCreate] [SourceManage] Create or update source
        /// </summary>
        /// <param name="key"></param>
        /// <param name="dto"></param>
        /// <returns>Source detail</returns>
        [Authorize]
        [HttpPost("{key}")]
        [ProducesResponseType(typeof(SourceDetailDto), 200)]
        public async Task<IActionResult> Post([FromRoute] string key, [FromBody] SourceDetailDto dto)
        {
            var entity = _mapper.Map<Source>(dto);
            entity.Key = key;
            entity = await _sourceService.CreateOrUpdateAsync(User, entity);
            return Ok(_mapper.Map<SourceDetailDto>(entity));
        }

        /// <summary>
        /// [Authorize] [SourcePreferenceUpdate] Update source preference
        /// </summary>
        /// <param name="key"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("[action]/{key}")]
        public async Task<IActionResult> Preference([FromRoute] string key, [FromBody] SourcePreferenceDto dto)
        {
            return Ok(await _sourceService.UpdatePreferenceAsync(User, key, _mapper.Map<SourcePreference>(dto)));
        }
    }
}