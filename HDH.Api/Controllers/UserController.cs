﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using HDH.Api.ViewModel.Users;
using HDH.Common.Extensions;
using HDH.Core.Users;
using HDH.Service.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HDH.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]s")]
    public class UserController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IUserService _userService;

        public UserController(IMapper mapper, IUserService userService)
        {
            _mapper = mapper;
            _userService = userService;
        }

        /// <summary>
        /// [Authorize] [UserManage] Get all user data
        /// </summary>
        /// <returns>List of user data</returns>
        [HttpGet]
        [ProducesResponseType(typeof(List<UserDto>), 200)]
        public async Task<IActionResult> Get(CancellationToken token)
        {
            var entity = await _userService.GetAllAsync(User, token);
            return Ok(_mapper.Map<List<UserDto>>(entity));
        }

        /// <summary>
        /// [Authorize] [UserRead] Get user data
        /// </summary>
        /// <remarks>Only allow get own data. Require [UserManage] to get others</remarks>
        /// <param name="id">User id</param>
        /// <param name="token"></param>
        /// <returns>User data</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(UserDto), 200)]
        public async Task<IActionResult> Get([FromRoute] string id, CancellationToken token)
        {
            var entity = await _userService.GetAsync(User, id, token);
            if (entity == null)
                return NotFound();

            return Ok(_mapper.Map<UserDto>(entity));
        }
        
        /// <summary>
        /// [Authorize] [UserCreate] Create new user
        /// </summary>
        /// <remarks>User created with random password. User can reset password through email.</remarks>
        /// <param name="dto">User data</param>
        /// <returns>User data</returns>
        [HttpPost]
        [ProducesResponseType(typeof(UserDto), 201)]
        public async Task<IActionResult> Post([FromBody] UserDto dto)
        {
            var entity = new User
            {
                UserName = dto.Email,
                Email = dto.Email,
                Enabled = dto.Enabled,
                Name = dto.Name,
                Dob = dto.Dob.FromUnixTimeSeconds(),
                Address = dto.Address,
                Avatar = dto.Avatar,
                City = dto.City,
                Country = dto.Country,
                Phone = dto.Phone,
                Roles = dto.Roles,
                Level = dto.Level
            };
            entity = await _userService.CreateAsync(User, entity);
            return Ok(_mapper.Map<UserDto>(entity));
        }

        /// <summary>
        /// [Authorize] [UserRead] [UserUpdate] Update user data
        /// </summary>
        /// <remarks>Only allow update own data. Require [UserManage] to update others</remarks>
        /// <param name="id">User id</param>
        /// <param name="dto">New user data</param>
        /// <returns>Updated user data</returns>
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(UserDto), 200)]
        public async Task<IActionResult> Put([FromRoute] string id, [FromBody] UserDto dto)
        {
            var entity = await _userService.GetAsync(User, id);
            if (entity == null)
                return NotFound();

            entity.Name = dto.Name;
            entity.Dob = dto.Dob.FromUnixTimeSeconds();
            entity.Address = dto.Address;
            entity.Avatar = dto.Avatar;
            entity.City = dto.City;
            entity.Country = dto.Country;
            entity.Phone = dto.Phone;
            entity.Roles = dto.Roles;
            entity.Level = dto.Level;
            await _userService.UpdateAsync(User, entity);
            return Ok(_mapper.Map<UserDto>(entity));
        }

        /// <summary>
        /// [Authorize] [UserRead] [UserUpdate] Update user password
        /// </summary>
        /// <param name="id">User id</param>
        /// <param name="request">Current and new password</param>
        /// <returns></returns>
        [HttpPatch("{id}/[action]")]
        public async Task<IActionResult> ChangePassword([FromRoute] string id, [FromBody] ChangePasswordRequest request)
        {
            var entity = await _userService.GetAsync(User, id);
            if (entity == null)
                return NotFound();

            await _userService.ChangePasswordAsync(User, entity, request.CurrentPassword, request.NewPassword);
            return Ok();
        }

        /// <summary>
        /// [Authorize] [UserRead] [UserDelete] Delete user
        /// </summary>
        /// <remarks>Only allow delete own profile. Require [UserManage] to delete others</remarks>
        /// <param name="id">User id</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete([FromRoute] string id)
        {
            var entity = await _userService.GetAsync(User, id);
            if (entity == null)
                return NotFound();

            await _userService.DeleteAsync(User, entity);
            return Ok();
        }
    }
}