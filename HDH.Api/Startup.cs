﻿using HDH.Api.Startups;
using HDH.Common.Middleware;
using HDH.Common.Settings;
using HDH.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace HDH.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var connStr = Configuration.GetConnectionString(nameof(AppDbContext));
            var appSettings = new AppSettings();
            var jwtSettings = new JwtSettings();
            Configuration.GetSection(nameof(AppSettings)).Bind(appSettings);
            Configuration.GetSection(nameof(JwtSettings)).Bind(jwtSettings);
            services.Configure<AppSettings>(Configuration.GetSection(nameof(AppSettings)));
            services.Configure<JwtSettings>(Configuration.GetSection(nameof(JwtSettings)));

            AppLogging.Initialize(services);
            AppDb.Initialize(services, connStr);
            AppCors.Initialize(services, appSettings);
            AppIdentity.Initialize(services);
            AppAuthentication.Initialize(services, jwtSettings);
            AppDenpendencyInjection.Initialize(services);
            SwaggerGen.Initialize(services);
            AutoMapperProfile.Initialize(services);

            services.AddMvc(o =>
            {
                o.Filters.Add(typeof(HandleExceptionAttribute));
                o.Filters.Add(typeof(ValidModelStateAttribute));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app)
        {
            app.UseSwagger();
            app.UseSwaggerUI(o => o.SwaggerEndpoint("/swagger/haydayhelper-api/swagger.json", "HaydayHelper API"));
            app.UseCors(nameof(Startup));
            app.UseAuthentication();
            app.UseMvc();
        }
    }
}