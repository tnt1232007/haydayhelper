﻿using HDH.Common.Settings;
using Microsoft.Extensions.DependencyInjection;

namespace HDH.Api.Startups
{
    public class AppCors
    {
        public static void Initialize(IServiceCollection services, AppSettings settings)
        {
            services.AddCors(o => o.AddPolicy(nameof(Startup),
                m => m.WithOrigins(settings.BaseUrls.Web)
                    .AllowAnyMethod()
                    .AllowAnyHeader()));
        }
    }
}