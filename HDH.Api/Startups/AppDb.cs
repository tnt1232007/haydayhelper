﻿using HDH.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace HDH.Api.Startups
{
    public class AppDb
    {
        public static void Initialize(IServiceCollection services, string connStr)
        {
            //services.AddDbContext<AppDbContext>(o => o.UseMySql(connStr));
            //services.AddDbContext<AppDbContext>(o => o.UseSqlServer(connStr));
            services.AddDbContext<AppDbContext>(o => o.UseNpgsql(connStr));
        }
    }
}