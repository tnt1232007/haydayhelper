﻿using HDH.Common.Middleware;
using HDH.Core.Users;
using HDH.Data;
using HDH.Service.Auths;
using HDH.Service.Csv;
using HDH.Service.Emails;
using HDH.Service.Plans;
using HDH.Service.Products;
using HDH.Service.Sources;
using HDH.Service.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;

namespace HDH.Api.Startups
{
    public static class AppDenpendencyInjection
    {
        public static void Initialize(IServiceCollection services)
        {
            services.AddSingleton<IAuthorizationHandler, PermissionAuthorizationHandler>();
            services.AddSingleton<IAuthorizationHandler, UserAuthorizationHandler>();
            services.AddSingleton<IAuthorizationHandler, ProductPreferenceAuthorizationHandler>();
            services.AddSingleton<IAuthorizationHandler, SourcePreferenceAuthorizationHandler>();

            services.AddScoped<IPasswordValidator<User>, PasswordValidator<User>>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            
            services.AddScoped<IRoleService, RoleService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IAuthService, AuthService>();
            
            services.AddScoped<IEmailService, EmailService>();
            services.AddScoped<ICsvService, CsvService>();

            services.AddScoped<ISourceService, SourceService>();
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<IPlanService, PlanService>();
        }
    }
}