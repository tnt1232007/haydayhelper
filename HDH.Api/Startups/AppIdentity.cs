﻿using HDH.Common.Passwords;
using HDH.Core.Users;
using HDH.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;

namespace HDH.Api.Startups
{
    public class AppIdentity
    {
        public static void Initialize(IServiceCollection services)
        {
            services.AddIdentity<User, Role>(
                    o =>
                    {
                        o.Password.RequiredLength = 7;
                        o.Password.RequiredUniqueChars = 4;
                        o.Password.RequireDigit = false;
                        o.Password.RequireLowercase = false;
                        o.Password.RequireNonAlphanumeric = false;
                        o.Password.RequireUppercase = false;
                        o.SignIn.RequireConfirmedEmail = false;
                        o.SignIn.RequireConfirmedPhoneNumber = false;
                        o.User.RequireUniqueEmail = true;
                    })
                .AddEntityFrameworkStores<AppDbContext>()
                .AddDefaultTokenProviders()
                .AddPasswordValidator<EmailAsPasswordValidator<User>>();
        }
    }
}