﻿using System;
using Karambolo.Extensions.Logging.File;
using Microsoft.Extensions.DependencyInjection;

namespace HDH.Api.Startups
{
    public class AppLogging
    {
        public static void Initialize(IServiceCollection services)
        {
            services.AddLogging(b => b.AddFile(new FileLoggerContext(AppContext.BaseDirectory, $"{typeof(AppLogging).Namespace}.log")));
        }
    }
}
