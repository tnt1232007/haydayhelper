﻿using System;
using System.Linq;
using AutoMapper;
using HDH.Api.ViewModel.Plans;
using HDH.Api.ViewModel.Products;
using HDH.Api.ViewModel.Sources;
using HDH.Api.ViewModel.Users;
using HDH.Common.Pagings;
using HDH.Core.Plans;
using HDH.Core.Products;
using HDH.Core.Sources;
using HDH.Core.Users;
using HDH.Service.Csv;
using Microsoft.Extensions.DependencyInjection;

namespace HDH.Api.Startups
{
    public static class AutoMapperProfile
    {
        public static void Initialize(IServiceCollection services)
        {
            Mapper.Initialize(o =>
            {
                o.AddProfile<GlobalProfile>();
                o.AddProfile<UserProfile>();
                o.AddProfile<ProductProfile>();
                o.AddProfile<SourceProfile>();
                o.AddProfile<PlanProfile>();
            });
            Mapper.AssertConfigurationIsValid();
            services.AddSingleton(Mapper.Instance);
        }

        private class GlobalProfile : Profile
        {
            public GlobalProfile()
            {
                CreateMap<DateTimeOffset?, long?>()
                    .ConvertUsing(o => o?.ToUnixTimeSeconds());
                // BUG: _mapper.Map<DateTimeOffset?>(1507206653) not working
                CreateMap<long?, DateTimeOffset?>()
                    .ConvertUsing(o => o.HasValue ? DateTimeOffset.FromUnixTimeSeconds(o.Value) : default(DateTimeOffset?));

                CreateMap<PagingDto, Paging>()
                    .ForMember(o => o.Total, o => o.Ignore());
            }
        }

        private class UserProfile : Profile
        {
            public UserProfile()
            {
                CreateMap<User, UserDto>();
                CreateMap<Role, RoleDto>();
            }
        }

        private class ProductProfile : Profile
        {
            public ProductProfile()
            {
                CreateMap<Product, ProductDto>().MaxDepth(2)
                    .ForMember(o => o.Ingredients, o => o.ResolveUsing(m => m.Ingredients?.OrderBy(n => n.Ingredient?.Level)))
                    .ForMember(o => o.ProductionTimeTicks, o => o.ResolveUsing(m => m.ProductionTime))
                    .ForMember(o => o.ProductionTime, o => o.ResolveUsing(m => m.ProductionTime.HasValue ? new TimeSpan(m.ProductionTime.Value) : default(TimeSpan?)));
                CreateMap<Product, ProductExtendDto>()
                    .IncludeBase<Product, ProductDto>()
                    .ForMember(o => o.Rating, o => o.ResolveUsing(m => m.ProductPreferences?.FirstOrDefault()?.Rating))
                    .ForMember(o => o.Stock, o => o.ResolveUsing(m => m.ProductPreferences?.FirstOrDefault()?.Stock))
                    .ForMember(o => o.ProductPrice, o => o.ResolveUsing(m => $"{m.DefaultPrice} - {m.MaxPrice}"))
                    .ForMember(o => o.PerBoatCrate, o => o.ResolveUsing(m => $"{m.MinPerCrate}{(m.MaxPerCrate.HasValue ? $" - {m.MaxPerCrate}" : string.Empty)}"))
                    .ForMember(o => o.IsMastered, o => o.ResolveUsing(m => m.Source?.SourcePreferences?.FirstOrDefault()?.IsMastered))
                    .ForMember(o => o.Cost, o => o.Ignore());
                CreateMap<Product, ProductDetailDto>()
                    .IncludeBase<Product, ProductExtendDto>()
                    .ForMember(o => o.Products, o => o.ResolveUsing(m => m.Products?.OrderBy(n => n.Product?.Level)))
                    .ForMember(o => o.Relatives, o => o.Ignore());
                CreateMap<ProductIngredient, ProductIngredientDto>()
                    .ForMember(o => o.IngredientName, o => o.ResolveUsing(m => m.Ingredient?.Name))
                    .ForMember(o => o.ProductName, o => o.ResolveUsing(m => m.Product?.Name))
                    .ForMember(o => o.Rating, o => o.ResolveUsing(m => m.Ingredient?.ProductPreferences?.FirstOrDefault()?.Rating));
                CreateMap<Product, ProductUsageDto>()
                    .ForMember(o => o.Key, o => o.ResolveUsing(m => m.Key))
                    .ForMember(o => o.TimeRequired, o => o.ResolveUsing(m => m.ProductionTime))
                    .ForMember(o => o.UsedCount, o => o.Ignore());
                CreateMap<ProductUsageDto, ProductUsageDto>();
                CreateMap<Product, CsvProductExport>()
                    .ForMember(o => o.Order, o => o.Ignore());

                CreateMap<ProductPreferenceDto, ProductPreference>()
                    .ForMember(o => o.UserId, o => o.Ignore())
                    .ForMember(o => o.ProductKey, o => o.Ignore())
                    .ForMember(o => o.Product, o => o.Ignore())
                    .ForMember(o => o.CreatedBy, o => o.Ignore())
                    .ForMember(o => o.CreatedAt, o => o.Ignore())
                    .ForMember(o => o.ModifiedBy, o => o.Ignore())
                    .ForMember(o => o.ModifiedAt, o => o.Ignore());
                CreateMap<ProductIngredientDto, ProductIngredient>()
                    .ForMember(o => o.Product, o => o.Ignore())
                    .ForMember(o => o.Ingredient, o => o.Ignore());
                CreateMap<ProductDto, Product>()
                    .ForMember(o => o.SourceKey, o => o.ResolveUsing(m => m.Source.Key))
                    .ForMember(o => o.ProductionTime, o => o.ResolveUsing(m => m.ProductionTime?.Ticks))
                    .ForMember(o => o.Products, o => o.Ignore())
                    .ForMember(o => o.ProductPreferences, o => o.Ignore())
                    .ForMember(o => o.CreatedBy, o => o.Ignore())
                    .ForMember(o => o.CreatedAt, o => o.Ignore())
                    .ForMember(o => o.ModifiedBy, o => o.Ignore())
                    .ForMember(o => o.ModifiedAt, o => o.Ignore());
                CreateMap<ProductExtendDto, Product>()
                    .IncludeBase<ProductDto, Product>();
                CreateMap<ProductDetailDto, Product>()
                    .IncludeBase<ProductExtendDto, Product>();
            }
        }

        private class SourceProfile : Profile
        {
            public SourceProfile()
            {
                CreateMap<Source, SourceDto>();
                CreateMap<Source, SourceExtendDto>()
                    .IncludeBase<Source, SourceDto>()
                    .ForMember(o => o.Products, o => o.ResolveUsing(m => m.Products?.OrderBy(n => n.Level)))
                    .ForMember(o => o.SourceInstances, o => o.ResolveUsing(m => m.SourceInstances?.OrderBy(n => n.Order)))
                    .ForMember(o => o.IsMastered, o => o.ResolveUsing(m => m.SourcePreferences?.FirstOrDefault()?.IsMastered));
                CreateMap<Source, SourceDetailDto>()
                    .IncludeBase<Source, SourceExtendDto>()
                    .ForMember(o => o.Products, o => o.ResolveUsing(m => m.Products?.OrderBy(n => n.Level)));
                CreateMap<SourceInstance, SourceInstanceDto>()
                    .ForMember(o => o.BuildTime, o => o.ResolveUsing(m => m.BuildTime.HasValue ? new TimeSpan(m.BuildTime.Value) : default(TimeSpan?)));
                CreateMap<Source, CsvSourceExport>()
                    .ForMember(o => o.Order, o => o.Ignore());

                CreateMap<SourcePreferenceDto, SourcePreference>()
                    .ForMember(o => o.UserId, o => o.Ignore())
                    .ForMember(o => o.SourceKey, o => o.Ignore())
                    .ForMember(o => o.Source, o => o.Ignore())
                    .ForMember(o => o.CreatedBy, o => o.Ignore())
                    .ForMember(o => o.CreatedAt, o => o.Ignore())
                    .ForMember(o => o.ModifiedBy, o => o.Ignore())
                    .ForMember(o => o.ModifiedAt, o => o.Ignore());
                CreateMap<SourceInstanceDto, SourceInstance>()
                    .ForMember(o => o.BuildTime, o => o.ResolveUsing(m => m.BuildTime?.Ticks))
                    .ForMember(o => o.SourceKey, o => o.Ignore())
                    .ForMember(o => o.Order, o => o.Ignore())
                    .ForMember(o => o.Source, o => o.Ignore());
                CreateMap<SourceDto, Source>()
                    .ForMember(o => o.Products, o => o.Ignore())
                    .ForMember(o => o.SourceInstances, o => o.Ignore())
                    .ForMember(o => o.SourcePreferences, o => o.Ignore())
                    .ForMember(o => o.CreatedBy, o => o.Ignore())
                    .ForMember(o => o.CreatedAt, o => o.Ignore())
                    .ForMember(o => o.ModifiedBy, o => o.Ignore())
                    .ForMember(o => o.ModifiedAt, o => o.Ignore());
                CreateMap<SourceExtendDto, Source>()
                    .ForMember(o => o.Products, o => o.ResolveUsing(m => m.Products.Select(n => new Product { Key = n.Key })))
                    .IncludeBase<SourceDto, Source>();
                CreateMap<SourceDetailDto, Source>()
                    .IncludeBase<SourceExtendDto, Source>();
            }
        }

        private class PlanProfile : Profile
        {
            public PlanProfile()
            {
                CreateMap<PlanProduct, PlanProductDto>()
                    .ForMember(o => o.Key, o => o.ResolveUsing(m => m.ProductKey))
                    .ForMember(o => o.Name, o => o.ResolveUsing(m => m.Product?.Name));
                CreateMap<Plan, PlanDto>()
                    .ForMember(o => o.Products, o => o.ResolveUsing(m => m.PlanProducts.OrderBy(n => n.Product.Level)));
            }
        }
    }
}