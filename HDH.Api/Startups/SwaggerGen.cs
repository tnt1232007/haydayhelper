﻿using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace HDH.Api.Startups
{
    public static class SwaggerGen
    {
        public static void Initialize(IServiceCollection services)
        {
            services.AddSwaggerGen(o =>
            {
                o.SwaggerDoc("haydayhelper-api", new Info {Title = "HaydayHelper API", Version = "v2"});
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                o.IncludeXmlComments(Path.Combine(basePath, "HDH.Api.xml"));
                o.IncludeXmlComments(Path.Combine(basePath, "HDH.Common.xml"));
                o.DescribeAllEnumsAsStrings();
                o.DescribeAllParametersInCamelCase();
                o.AddSecurityDefinition("JWT", new ApiKeyScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"
                });
                o.OperationFilter<FileOperationFilter>();
            });
        }

        private class FileOperationFilter : IOperationFilter
        {
            public void Apply(Operation operation, OperationFilterContext context)
            {
                const int defaultFileParamsCount = 6;
                var paras = context.ApiDescription.ActionDescriptor.Parameters;
                var removeIndices = paras.Select((v, i) => new {Value = v, Index = i})
                    .Where(o => o.Value.ParameterType == typeof(IFormFile)).Select(o => o.Index).ToList();
                for (var i = 0; i < removeIndices.Count; i++)
                {
                    for (var j = defaultFileParamsCount - 1; j >= 0; j--)
                        operation.Parameters.RemoveAt(i + j);
                    operation.Parameters.Insert(i, new NonBodyParameter
                    {
                        Name = paras[i].Name,
                        In = "formData",
                        Description = "Upload File",
                        Required = true,
                        Type = "file"
                    });
                }
                if (removeIndices.Any())
                    operation.Consumes.Add("application/form-data");
            }
        }
    }
}