﻿using System.Collections.Generic;

namespace HDH.Api.ViewModel.Plans
{
    public class PlanDto
    {
        public int Id { get; set; }
        public List<PlanProductDto> Products { get; set; }
    }
}
