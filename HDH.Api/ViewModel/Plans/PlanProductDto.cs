﻿namespace HDH.Api.ViewModel.Plans
{
    public class PlanProductDto
    {
        public int PlanId { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
        public double Quantity { get; set; }
    }
}
