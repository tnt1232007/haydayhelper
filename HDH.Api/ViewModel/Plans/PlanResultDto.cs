﻿using System.Collections.Generic;

namespace HDH.Api.ViewModel.Plans
{
    public class PlanResultDto
    {
        public int PlanId { get; set; }
        public List<PlanProductDto> DirectIngredients { get; set; }
        public List<PlanProductDto> CoreIngredients { get; set; }
        public List<PlanSourceDto> ProductionTimes { get; set; }
    }
}
