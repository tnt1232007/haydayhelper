using System;

namespace HDH.Api.ViewModel.Plans
{
    public class PlanSourceDto
    {
        public int PlanId { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
        public TimeSpan ProductionTime { get; set; }
    }
}