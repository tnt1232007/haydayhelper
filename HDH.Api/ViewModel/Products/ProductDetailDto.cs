using System.Collections.Generic;

namespace HDH.Api.ViewModel.Products
{
    public class ProductDetailDto : ProductExtendDto
    {
        public List<ProductIngredientDto> Products { get; set; }

        public List<ProductDto> Relatives { get; set; }
    }
}