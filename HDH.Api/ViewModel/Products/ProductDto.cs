﻿using System;
using System.Collections.Generic;
using HDH.Api.ViewModel.Sources;

namespace HDH.Api.ViewModel.Products
{
    public class ProductDto
    {
        public string Key { get; set; }
        public string Name { get; set; }
        public int Level { get; set; }
        public long DefaultPrice { get; set; }
        public long MaxPrice { get; set; }
        public long? ProductionTimeTicks { get; set; }
        public TimeSpan? ProductionTime { get; set; }
        public int? ExperienceGained { get; set; }
        public int? MinPerCrate { get; set; }
        public int? MaxPerCrate { get; set; }
        public SourceDto Source { get; set; }
        public List<ProductIngredientDto> Ingredients { get; set; }
    }
}