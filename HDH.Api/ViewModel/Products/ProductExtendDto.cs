﻿using System;

namespace HDH.Api.ViewModel.Products
{
    public class ProductExtendDto : ProductDto
    {
        public string ProductPrice { get; set; }
        public string PerBoatCrate { get; set; }
        public long Cost { get; set; } = 0;
        public int? Rating { get; set; }
        public int? Stock { get; set; }
        public bool IsMastered { get; set; } = false;
        public long? RealProductionTimeTicks => ProductionTimeTicks.HasValue ? IsMastered ? ProductionTimeTicks.Value * 85 / 100 : ProductionTimeTicks.Value : default(long?);
        public TimeSpan? RealProductionTime => RealProductionTimeTicks.HasValue ? new TimeSpan(RealProductionTimeTicks.Value) : (TimeSpan?)null;
        public double? Profit => (MaxPrice - Cost) / RealProductionTime?.TotalMinutes;
    }
}