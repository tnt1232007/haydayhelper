﻿namespace HDH.Api.ViewModel.Products
{
    public class ProductIngredientDto
    {
        public string IngredientKey { get; set; }

        public string IngredientName { get; set; }

        public string ProductKey { get; set; }

        public string ProductName { get; set; }

        public double Quantity { get; set; }

        public int? Rating { get; set; }
    }
}