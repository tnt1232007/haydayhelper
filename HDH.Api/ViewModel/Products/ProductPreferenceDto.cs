﻿using System.ComponentModel.DataAnnotations;

namespace HDH.Api.ViewModel.Products
{
    public class ProductPreferenceDto
    {
        [Range(0, 5)]
        public int? Rating { get; set; }

        public int? Stock { get; set; }
    }
}