﻿using System.Collections.Generic;

namespace HDH.Api.ViewModel.Products
{
    public class ProductRelationshipDto
    {
        public List<ProductUsageDto> Nodes { get; set; }

        public List<ProductIngredientDto> Edges { get; set; }
    }
}