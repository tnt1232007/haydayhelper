﻿namespace HDH.Api.ViewModel.Products
{
    public class ProductUsageDto
    {
        public string Key { get; set; }

        public string Name { get; set; }

        public int Level { get; set; }

        public string SourceKey { get; set; }

        public double UsedCount { get; set; } = 1;

        public double? TimeRequired { get; set; } = 0;

        public ProductUsageDto SetUsedCount(double usedCount)
        {
            UsedCount = usedCount;
            return this;
        }
    }
}