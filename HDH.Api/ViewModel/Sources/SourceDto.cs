﻿namespace HDH.Api.ViewModel.Sources
{
    public class SourceDto
    {
        public string Key { get; set; }

        public string Name { get; set; }
        
        public int Level { get; set; }
    }
}