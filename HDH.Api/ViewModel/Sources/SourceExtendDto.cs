﻿using System.Collections.Generic;
using HDH.Api.ViewModel.Products;

namespace HDH.Api.ViewModel.Sources
{
    public class SourceExtendDto : SourceDto
    {
        public bool IsMastered { get; set; }
        public List<ProductExtendDto> Products { get; set; }
        public List<SourceInstanceDto> SourceInstances { get; set; }
    }
}