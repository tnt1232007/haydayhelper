using System;

namespace HDH.Api.ViewModel.Sources
{
    public class SourceInstanceDto
    {
        public long Cost { get; set; }

        public TimeSpan? BuildTime { get; set; }
    }
}