﻿namespace HDH.Api.ViewModel.Sources
{
    public class SourcePreferenceDto
    {
        public bool IsMastered { get; set; }
    }
}