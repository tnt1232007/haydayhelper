﻿using System.ComponentModel.DataAnnotations;

namespace HDH.Api.ViewModel.Users
{
    public class ForgotPasswordRequest
    {
        [Required]
        [EmailAddress(ErrorMessage = "Email is not a valid email address.")]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}