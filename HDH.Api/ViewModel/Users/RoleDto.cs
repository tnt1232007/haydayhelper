﻿namespace HDH.Api.ViewModel.Users
{
    public class RoleDto
    {
        public string Name { get; set; }
        public string[] Permissions { get; set; }
    }
}