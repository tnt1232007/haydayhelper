﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HDH.Api.ViewModel.Users
{
    public class UserDto
    {
        public string Id { get; set; }

        [Required]
        public string Email { get; set; }

        public bool Enabled { get; set; }

        [Required]
        public string[] Roles { get; set; }

        public long? LastSignedInAt { get; set; }

        public string Name { get; set; }
        public long? Dob { get; set; }
        public string Avatar { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        public int? Level { get; set; }

        public string CreatedBy { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset ModifiedAt { get; set; }
    }
}