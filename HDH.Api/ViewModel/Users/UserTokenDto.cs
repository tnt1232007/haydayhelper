﻿namespace HDH.Api.ViewModel.Users
{
    public class UserTokenDto
    {
        public string AccessToken { get; set; }

        public string TokenType { get; set; }

        public int ExpiresIn { get; set; }
    }
}