﻿using System.Collections.Generic;
using HDH.Core.Plans;

namespace HDH.Common.Comparer
{
    public class PlanProductComparer : IEqualityComparer<PlanProduct>
    {
        public bool Equals(PlanProduct x, PlanProduct y)
        {
            return x.ProductKey == y.ProductKey;
        }

        public int GetHashCode(PlanProduct obj)
        {
            return obj.ProductKey.GetHashCode();
        }
    }
}
