﻿using System.Collections.Generic;
using HDH.Core.Products;

namespace HDH.Common.Comparer
{
    public class ProductComparer : IEqualityComparer<Product>
    {
        public bool Equals(Product x, Product y)
        {
            return x.Key == y.Key;
        }

        public int GetHashCode(Product obj)
        {
            return obj.Key.GetHashCode();
        }
    }
}
