﻿using System.Collections.Generic;
using HDH.Core.Products;

namespace HDH.Common.Comparer
{
    public class ProductIngredientComparer : IEqualityComparer<ProductIngredient>
    {
        public bool Equals(ProductIngredient x, ProductIngredient y)
        {
            return x.IngredientKey == y.IngredientKey;
        }

        public int GetHashCode(ProductIngredient obj)
        {
            return obj.IngredientKey.GetHashCode();
        }
    }
}
