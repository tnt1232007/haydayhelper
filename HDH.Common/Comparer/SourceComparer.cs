﻿using System.Collections.Generic;
using HDH.Core.Sources;

namespace HDH.Common.Comparer
{
    public class SourceComparer : IEqualityComparer<Source>
    {
        public bool Equals(Source x, Source y)
        {
            return x.Key == y.Key;
        }

        public int GetHashCode(Source obj)
        {
            return obj.Key.GetHashCode();
        }
    }
}
