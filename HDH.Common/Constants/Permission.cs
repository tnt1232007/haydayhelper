﻿namespace HDH.Common.Constants
{
    /// <summary>
    /// RUD: On own entity only
    /// Manage: On other users entities
    /// </summary>
    public enum Permission
    {
        UserCreate,
        UserRead,
        UserUpdate,
        UserDelete,
        UserManage,

        RoleCreate,
        RoleRead,
        RoleDelete,
        RoleManage,

        ProductCreate,
        ProductManage,

        SourceCreate,
        SourceManage,

        ProductPreferenceRead,
        ProductPreferenceUpdate,
        ProductPreferenceManage,

        SourcePreferenceRead,
        SourcePreferenceUpdate,
        SourcePreferenceManage,

        PlanCreate,
        PlanRead,
        PlanUpdate,
        PlanDelete,

        SettingRead,
        SettingUpdate,
        SettingManage
    }
}