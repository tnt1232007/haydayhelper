﻿namespace HDH.Common.Constants
{
    public static class Roles
    {
        public const string Administrator = "Administrator";
        public const string Staff = "Staff";
        public const string User = "User";
    }
}