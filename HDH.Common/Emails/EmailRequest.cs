﻿using System.Collections.Generic;

namespace HDH.Common.Emails
{
    public class EmailRequest
    {
        public string FromAddress { get; set; }
        public string[] ToAddress { get; set; } = new string[0];
        public string[] Cc { get; set; } = new string[0];
        public string[] Bcc { get; set; } = new string[0];

        public string Subject { get; set; }
        public string Content { get; set; }

        public long TemplateId { get; set; }
        public Dictionary<string, object> TemplateModel { get; set; }
    }
}
