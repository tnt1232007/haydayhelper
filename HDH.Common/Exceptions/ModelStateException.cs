﻿using System;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace HDH.Common.Exceptions
{
    public class ModelStateException : Exception
    {
        public ModelStateException(string key, string errorMessage)
        {
            ModelState = new ModelStateDictionary();
            ModelState.AddModelError(key, errorMessage);
        }

        public ModelStateException(ModelStateDictionary modelState)
        {
            ModelState = modelState;
        }

        public ModelStateDictionary ModelState { get; }
    }
}