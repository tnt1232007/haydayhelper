﻿using System;

namespace HDH.Common.Exceptions
{
    public class PagingException : Exception
    {
        public PagingException() { }

        public PagingException(string message) : base(message) { }
    }
}