﻿using System;

namespace HDH.Common.Exceptions
{
    public class ResourceNotFoundException : Exception
    {
        public ResourceNotFoundException() { }

        public ResourceNotFoundException(string paramName, string value) : base($"{paramName} not found. Parameter value: {value}") { }
    }
}