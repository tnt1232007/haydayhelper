﻿using System;

namespace HDH.Common.Exceptions
{
    public class ThirdPartyException : Exception
    {
        public ThirdPartyException() { }

        public ThirdPartyException(string name, string message) : base(message)
        {
            Name = name;
        }

        public string Name { get; set; }
    }
}