﻿using System.Security.Claims;
using System.Threading.Tasks;
using HDH.Common.Constants;
using HDH.Common.Middleware;
using HDH.Core.Base;
using Microsoft.AspNetCore.Authorization;

namespace HDH.Common.Extensions
{
    public static class AuthorizationExtension
    {
        public static async Task<bool> AuthorizeAsync(this IAuthorizationService service, ClaimsPrincipal user, params Permission[] permission)
        {
            var authorizationResult = await service.AuthorizeAsync(user, null, new PermissionAuthorizationRequirement(permission));
            return authorizationResult.Succeeded;
        }

        public static async Task<bool> AuthorizeAsync<T>(this IAuthorizationService service, ClaimsPrincipal user, T resource, params Permission[] permission) where T : IEntity
        {
            var authorizationResult = await service.AuthorizeAsync(user, resource, new PermissionAuthorizationRequirement(permission));
            return authorizationResult.Succeeded;
        }
    }
}