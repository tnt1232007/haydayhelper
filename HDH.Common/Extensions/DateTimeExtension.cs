﻿using System;

namespace HDH.Common.Extensions
{
    public static class DateTimeExtension
    {
        public static DateTimeOffset? FromUnixTimeSeconds(this long? value)
        {
            return value.HasValue ? DateTimeOffset.FromUnixTimeSeconds(value.Value) : default(DateTimeOffset?);
        }

        public static long? ToUnixTimeSeconds(this DateTimeOffset? value)
        {
            return value?.ToUnixTimeSeconds();
        }
    }
}
