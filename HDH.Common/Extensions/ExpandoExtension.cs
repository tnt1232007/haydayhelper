﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Dynamic;
using System.Linq;

namespace HDH.Common.Extensions
{
    public static class ExpandoExtension
    {
        // Extension method that coverts an concrete object to a dynamic object.
        public static dynamic ToExpando(this object o, params string[] propertiesToIgnore)
        {
            var result = new ExpandoObject();
            var d = (IDictionary<string, object>) result; //work with the Expando as a Dictionary
            if (o is ExpandoObject) return o; //shouldn't have to... but just in case
            if (o.GetType() == typeof(NameValueCollection) || o.GetType().IsSubclassOf(typeof(NameValueCollection)))
            {
                var nv = (NameValueCollection) o;
                nv.Cast<string>().Where(x => !propertiesToIgnore.Contains(x))
                    .Select(key => new KeyValuePair<string, object>(key, nv[key])).ToList().ForEach(i => d.Add(i));
            }
            else
            {
                var props = o.GetType().GetProperties();
                foreach (var item in props)
                    if (!propertiesToIgnore.Contains(item.Name))
                        d.Add(item.Name, item.GetValue(o, null));
            }
            return result;
        }

        //  Maps the properties of a dynamic object to a concrete class.
        public static T ToConcrete<T>(this ExpandoObject dynObject)
        {
            var instance = Activator.CreateInstance<T>();
            var dict = dynObject as IDictionary<string, object>;
            var targetProperties = instance.GetType().GetProperties();
            
            if (dict == null) return default(T);

            foreach (var property in targetProperties)
            {
                if (!dict.TryGetValue(property.Name, out var propVal)) continue;
                if (property.PropertyType == typeof(DateTime) || property.PropertyType == typeof(DateTime?))
                    property.SetValue(instance, propVal == null ? new DateTime?() : DateTime.Parse(propVal.ToString()), null);
                else
                    property.SetValue(instance, propVal, null);
            }

            return instance;
        }
    }
}