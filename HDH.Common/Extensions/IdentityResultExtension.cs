﻿using System;
using System.Linq;
using System.Threading.Tasks;
using HDH.Common.Exceptions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace HDH.Common.Extensions
{
    public static class IdentityResultExtension
    {
        public static async Task Handled(this Task<IdentityResult> task, Action action = null)
        {
            var result = await task;
            if (!result.Succeeded)
            {
                action?.Invoke();
                var modelState = new ModelStateDictionary();
                foreach (var error in result.Errors.GroupBy(o => o.Code).Select(o => o.First()))
                    modelState.AddModelError(error.Code, error.Description);
                throw new ModelStateException(modelState);
            }
        }
    }
}