﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HDH.Common.Extensions
{
    public static class LinqExtension
    {
        public static IEnumerable<TResult> LeftJoin<TOuter, TInner, TKey, TResult>(
            this IEnumerable<TOuter> outer,
            IEnumerable<TInner> inner,
            Func<TOuter, TKey> outerKeySelector,
            Func<TInner, TKey> innerKeySelector,
            Func<TOuter, TInner, TResult> resultSelector)
        {
            return outer.GroupJoin(inner, outerKeySelector, innerKeySelector,
                    (outerObj, innerList) => new {outerObj, innerList = innerList.DefaultIfEmpty()})
                .SelectMany(o => o.innerList.Select(innerObj => resultSelector(o.outerObj, innerObj)));
        }

        public static IEnumerable<TResult> Pairwise<TSource, TResult>(this IEnumerable<TSource> enumerable, Func<TSource, TSource, TResult> resultSelector)
        {
            var list = enumerable.ToList();
            return list.Zip(list.Skip(1), resultSelector);
        }

    }
}