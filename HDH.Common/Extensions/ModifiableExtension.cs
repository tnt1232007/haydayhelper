﻿using System;
using System.Security.Claims;
using HDH.Core.Base;

namespace HDH.Common.Extensions
{
    public static class ModifiableExtension
    {
        public static void CreatedBy(this IModifiable entity, ClaimsPrincipal user)
        {
            entity.CreatedAt = entity.ModifiedAt = DateTimeOffset.UtcNow;
            entity.CreatedBy = entity.ModifiedBy = user?.GetId();
        }

        public static void UpdatedBy(this IModifiable entity, ClaimsPrincipal user)
        {
            entity.ModifiedAt = DateTimeOffset.UtcNow;
            entity.ModifiedBy = user?.GetId();
        }
    }
}