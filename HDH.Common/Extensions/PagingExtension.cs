﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using HDH.Common.Pagings;

namespace HDH.Common.Extensions
{
    public static class PagingExtension
    {
        public static bool ExtractFilters(this Paging paging, out string expression, out object[] values)
        {
            expression = string.Empty;
            values = new object[0];
            if (string.IsNullOrWhiteSpace(paging.Filter))
                return false;

            var i = 0;
            var list = new List<object>();
            expression = Regex.Replace(paging.Filter.ToLower(), @"(?<field>.*?) ?(?<op>[<=>]?[<=>]) ?(?<value>\w+)", o =>
            {
                list.Add(o.Groups["value"].Value);
                return $"{o.Groups["field"]}{o.Groups["op"]}@{i++}";
            }).Replace(",", " and ");
            values = list.ToArray();

            return !string.IsNullOrWhiteSpace(expression) && values.Any();
        }

        public static bool ExtractSorts(this Paging paging, out string sorts)
        {
            sorts = string.Empty;
            if (string.IsNullOrWhiteSpace(paging.Sort))
                return false;

            sorts = Regex.Replace(paging.Sort.ToLower(), @"([+-])(\w*)", "$2 $1")
                .Replace("+", "asc").Replace("-", "desc");
            return !string.IsNullOrWhiteSpace(sorts);
        }
    }
}