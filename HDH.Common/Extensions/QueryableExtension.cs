﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Linq.Dynamic.Core.Exceptions;
using System.Threading;
using System.Threading.Tasks;
using HDH.Common.Exceptions;
using HDH.Common.Pagings;
using Microsoft.EntityFrameworkCore;

namespace HDH.Common.Extensions
{
    public static class QueryableExtension
    {
        public static async Task<IEnumerable<T>> FindAllAsync<T>(this IQueryable<T> query, Paging paging, CancellationToken token) where T : class
        {
            if (paging == null)
                return await query.ToListAsync(token);

            if (paging.ExtractFilters(out var expression, out var values))
                try
                {
                    query = query.Where(expression, values);
                }
                catch (Exception ex) when (ex is ParseException || ex is FormatException)
                {
                    throw new PagingException(ex.Message);
                }

            if (paging.ExtractSorts(out var sorts))
                try
                {
                    query = query.OrderBy(sorts);
                }
                catch (ParseException ex)
                {
                    throw new PagingException(ex.Message);
                }

            paging.Total = await query.CountAsync(token);

            if (paging.PageSize <= 0)
                paging.PageSize = 0;

            if (paging.Page <= 0)
                paging.Page = 1;

            if (paging.Page > paging.TotalPages)
                paging.Page = paging.TotalPages;

            if (paging.Page > 1)
                query = query.Skip((paging.Page - 1) * paging.PageSize);

            if (paging.PageSize > 0)
                query = query.Take(paging.PageSize);

            return await query.ToListAsync(token);
        }
    }
}