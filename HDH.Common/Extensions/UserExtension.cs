﻿using System.Security.Claims;
using IdentityModel;

namespace HDH.Common.Extensions
{
    public static class UserExtension
    {
        public static string GetId(this ClaimsPrincipal user)
        {
            return user.FindFirstValue(JwtClaimTypes.Subject);
        }
    }
}