﻿using System;
using System.Linq;
using HDH.Common.Constants;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace HDH.Common.Middleware
{
    [Obsolete]
    public class RequirePermissionFilter : IAuthorizationFilter
    {
        private readonly Permission[] _permission;

        public RequirePermissionFilter(Permission[] permission)
        {
            _permission = permission;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var user = context.HttpContext.User;
            if (user.IsInRole(Roles.Administrator))
                return;

            if (!_permission.All(o => user.HasClaim(CustomClaimTypes.Permission, o.ToString())))
                context.Result = new ForbidResult();
        }
    }
}