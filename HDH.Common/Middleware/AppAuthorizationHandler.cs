using System.Linq;
using System.Threading.Tasks;
using HDH.Common.Constants;
using HDH.Common.Extensions;
using HDH.Core.Products;
using HDH.Core.Sources;
using HDH.Core.Users;
using Microsoft.AspNetCore.Authorization;

namespace HDH.Common.Middleware
{
    public class PermissionAuthorizationHandler : AuthorizationHandler<PermissionAuthorizationRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, PermissionAuthorizationRequirement requirement)
        {
            var user = context.User;
            if (user.IsInRole(Roles.Administrator))
                context.Succeed(requirement);
            else if (requirement.RequiredPermissions.All(o => user.HasClaim(CustomClaimTypes.Permission, o.ToString())))
                context.Succeed(requirement);

            return Task.CompletedTask;
        }
    }

    public class UserAuthorizationHandler : AuthorizationHandler<PermissionAuthorizationRequirement, User>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, PermissionAuthorizationRequirement requirement, User resource)
        {
            var user = context.User;
            // Administrator can do anything
            if (user.IsInRole(Roles.Administrator))
                context.Succeed(requirement);
            // User must have all required permissions and (the resource must belong to user or user has Manage Permission)
            else if (requirement.RequiredPermissions.All(o => user.HasClaim(CustomClaimTypes.Permission, o.ToString()))
                     && (resource.Id == user.GetId() || user.HasClaim(CustomClaimTypes.Permission, Permission.UserManage.ToString())))
                context.Succeed(requirement);
            else
                context.Fail();

            return Task.CompletedTask;
        }
    }

    public class ProductPreferenceAuthorizationHandler : AuthorizationHandler<PermissionAuthorizationRequirement, ProductPreference>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, PermissionAuthorizationRequirement requirement, ProductPreference resource)
        {
            var user = context.User;
            // Administrator can do anything
            if (user.IsInRole(Roles.Administrator))
                context.Succeed(requirement);
            // ProductPreference must have all required permissions and (the resource must belong to user or user has Manage Permission)
            else if (requirement.RequiredPermissions.All(o => user.HasClaim(CustomClaimTypes.Permission, o.ToString()))
                     && (resource.UserId == user.GetId() || user.HasClaim(CustomClaimTypes.Permission, Permission.ProductPreferenceManage.ToString())))
                context.Succeed(requirement);
            else
                context.Fail();

            return Task.CompletedTask;
        }
    }


    public class SourcePreferenceAuthorizationHandler : AuthorizationHandler<PermissionAuthorizationRequirement, SourcePreference>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, PermissionAuthorizationRequirement requirement, SourcePreference resource)
        {
            var user = context.User;
            // Administrator can do anything
            if (user.IsInRole(Roles.Administrator))
                context.Succeed(requirement);
            // SourcePreference must have all required permissions and (the resource must belong to user or user has Manage Permission)
            else if (requirement.RequiredPermissions.All(o => user.HasClaim(CustomClaimTypes.Permission, o.ToString()))
                     && (resource.UserId == user.GetId() || user.HasClaim(CustomClaimTypes.Permission, Permission.SourcePreferenceManage.ToString())))
                context.Succeed(requirement);
            else
                context.Fail();

            return Task.CompletedTask;
        }
    }
}