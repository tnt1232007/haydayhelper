﻿using System.Collections.Generic;
using HDH.Common.Constants;
using Microsoft.AspNetCore.Authorization;

namespace HDH.Common.Middleware
{
    public class PermissionAuthorizationRequirement : IAuthorizationRequirement
    {
        public PermissionAuthorizationRequirement(IEnumerable<Permission> requiredPermissions)
        {
            RequiredPermissions = requiredPermissions;
        }

        public IEnumerable<Permission> RequiredPermissions { get; }
    }
}