﻿using System;
using System.Net;
using System.Threading.Tasks;
using HDH.Common.Constants;
using HDH.Common.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace HDH.Common.Middleware
{
    [Obsolete]
    public class RequirePermissionAttribute : TypeFilterAttribute
    {
        public RequirePermissionAttribute(params Permission[] permission) : base(typeof(RequirePermissionFilter))
        {
            Arguments = new object[] {permission};
            Order = 1;
        }
    }

    public class ValidModelStateAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
                context.Result = new BadRequestObjectResult(context.ModelState);

            base.OnActionExecuting(context);
        }
    }

    public class HandleExceptionAttribute : ExceptionFilterAttribute
    {
        private readonly ILogger<HandleExceptionAttribute> _logger;
        private readonly Random _random;

        public HandleExceptionAttribute(ILogger<HandleExceptionAttribute> logger)
        {
            _logger = logger;
            _random = new Random();
        }

        public override void OnException(ExceptionContext context)
        {
            var exception = context.Exception.GetBaseException();
            var apiException = new ApiException
            {
                DeveloperMessage = exception.Message
            };

            switch (context.Exception)
            {
                case ModelStateException modelStateException:
                    apiException.UserMessage = new SerializableError(modelStateException.ModelState);
                    apiException.Code = HttpStatusCode.BadRequest;
                    break;
                case TaskCanceledException _:
                case OperationCanceledException _:
                    apiException.UserMessage = string.Empty;
                    apiException.Code = HttpStatusCode.BadRequest;
                    break;
                case PagingException _:
                    apiException.UserMessage = apiException.DeveloperMessage;
                    apiException.Code = HttpStatusCode.BadRequest;
                    break;
                case ResourceNotFoundException _:
                    apiException.UserMessage = "Not found. The requested resource does not exist.";
                    apiException.Code = HttpStatusCode.BadRequest;
                    break;
                case UnauthorizedAccessException _:
                    apiException.UserMessage = "Access denied. You do not have permission to perform this action or access this resource.";
                    apiException.Code = HttpStatusCode.Forbidden;
                    break;
                default:
                    EventId eventId = _random.Next();
                    apiException.UserMessage = $"Unhandled exception. Please contact administrator (EventId:{eventId}).";
                    apiException.Code = HttpStatusCode.InternalServerError;
                    _logger.LogError(eventId, exception, apiException.DeveloperMessage);
                    break;
            }

            context.Result = new JsonResult(apiException);
            context.HttpContext.Response.StatusCode = (int) apiException.Code;
            base.OnException(context);
        }

        private class ApiException
        {
            public dynamic UserMessage { get; set; }
            public string DeveloperMessage { get; set; }

            [JsonIgnore]
            public HttpStatusCode Code { get; set; }
        }
    }
}