﻿namespace HDH.Common.Pagings
{
    public class PagingDto
    {
        /// <summary>
        /// E.g level&gt;80,maxPrice&lt;400
        /// </summary>
        public string Filter { get; set; }

        /// <summary>
        /// E.g +level,-maxPrice
        /// </summary>
        public string Sort { get; set; }

        /// <summary>
        /// Default 1
        /// </summary>
        public int Page { get; set; } = 1;

        /// <summary>
        /// Default 10, set 0 for unlimited
        /// </summary>
        public int PageSize { get; set; } = 10;
    }
}
