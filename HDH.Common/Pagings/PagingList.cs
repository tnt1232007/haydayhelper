﻿using System.Collections.Generic;

namespace HDH.Common.Pagings
{
    public class PagingList<T>
    {
        public PagingList(IEnumerable<T> items, Paging paging)
        {
            Items = items;
            Paging = paging;
        }

        public string Keyword { get; set; }
        public Paging Paging { get; set; }
        public IEnumerable<T> Items { get; set; }
    }
}