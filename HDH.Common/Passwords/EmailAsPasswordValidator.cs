﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace HDH.Common.Passwords
{
    public class EmailAsPasswordValidator<TUser> : IPasswordValidator<TUser> where TUser : IdentityUser
    {
        public Task<IdentityResult> ValidateAsync(UserManager<TUser> manager, TUser user, string password)
        {
            var email = user.Email ?? user.UserName ?? string.Empty;
            if (string.Equals(email, password, StringComparison.OrdinalIgnoreCase)
                || string.Equals(email.Split('@').FirstOrDefault(), password, StringComparison.OrdinalIgnoreCase))
                return Task.FromResult(IdentityResult.Failed(new IdentityError
                {
                    Code = "EmailAsPassword",
                    Description = "You cannot use your email as your password"
                }));
            return Task.FromResult(IdentityResult.Success);
        }
    }
}