﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Identity;

namespace HDH.Common.Passwords
{
    public static class PasswordGenerator
    {
        public const string Uppercase = "ABCDEFGHJKLMNOPQRSTUVWXYZ";
        public const string Lowercase = "abcdefghijkmnopqrstuvwxyz";
        public const string Digits = "0123456789";
        public const string NonAlphanumeric = "!@$?_-";

        public static string GenerateRandomPassword(PasswordOptions opts = null)
        {
            var result = new List<char>();
            const string allChars = Uppercase + Lowercase + Digits + NonAlphanumeric;
            opts = opts ?? new PasswordOptions
            {
                RequiredLength = 7,
                RequiredUniqueChars = 4,
                RequireDigit = true,
                RequireLowercase = true,
                RequireNonAlphanumeric = true,
                RequireUppercase = true
            };
            
            var rand = new Random(Environment.TickCount);
            int RandomNumber(int max) => rand.Next(0, max);
            char RandomCharacter(string chars) => chars[RandomNumber(chars.Length)];

            if (opts.RequireUppercase)
                result.Insert(RandomNumber(result.Count), RandomCharacter(Uppercase));

            if (opts.RequireLowercase)
                result.Insert(RandomNumber(result.Count), RandomCharacter(Lowercase));

            if (opts.RequireDigit)
                result.Insert(RandomNumber(result.Count), RandomCharacter(Digits));

            if (opts.RequireNonAlphanumeric)
                result.Insert(RandomNumber(result.Count), RandomCharacter(NonAlphanumeric));

            for (var i = result.Count; i < opts.RequiredLength || result.Distinct().Count() < opts.RequiredUniqueChars; i++)
                result.Insert(RandomNumber(result.Count), RandomCharacter(allChars));

            return new string(result.ToArray());
        }
    }
}