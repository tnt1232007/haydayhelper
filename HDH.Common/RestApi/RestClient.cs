﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace HDH.Common.RestApi
{
    public static class RestClient
    {
        public static Task<T> GetAsync<T>(string url, CancellationToken token = default(CancellationToken))
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            return SendRequestAsync<T>(request, token);
        }

        public static Task<T> PostAsync<T>(string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, url);
            return SendRequestAsync<T>(request);
        }

        public static Task<T> PutAsync<T>(string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Put, url);
            return SendRequestAsync<T>(request);
        }

        public static Task<T> DeleteAsync<T>(string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Delete, url);
            return SendRequestAsync<T>(request);
        }

        private static async Task<T> SendRequestAsync<T>(HttpRequestMessage request, CancellationToken token = default(CancellationToken))
        {
            var client = new HttpClient();
            var result = await client.SendAsync(request, token);
            var content = await result.Content.ReadAsStringAsync();
            if (typeof(T) == typeof(string))
                return (T) Convert.ChangeType(content, typeof(T));
            if (typeof(T) == typeof(JObject))
                return (T) Convert.ChangeType(JObject.Parse(content), typeof(T));
            return JsonConvert.DeserializeObject<T>(content);
        }
    }
}