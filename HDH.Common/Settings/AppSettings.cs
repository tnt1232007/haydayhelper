﻿namespace HDH.Common.Settings
{
    public class AppSettings
    {
        public BaseUrls BaseUrls { get; set; }

        public string CountryApi { get; set; }
    }

    public class BaseUrls
    {
        public string Api { get; set; }

        public string Web { get; set; }
    }
}