﻿namespace HDH.Common.Settings
{
    public class JwtSettings
    {
        public string SecretKey { get; set; }
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public double Expiration { get; set; }
    }
}