﻿namespace HDH.Common.Views
{
    public class CheckBoxModel
    {
        public CheckBoxModel(string key, string name, string group = null, string icon = null, bool @checked = false)
        {
            Key = key;
            Name = name;
            Group = group;
            Icon = icon;
            Checked = @checked;
        }

        public string Key { get; set; }
        public string Name { get; set; }
        public string Group { get; set; }
        public string Icon { get; set; }
        public bool Checked { get; set; }
    }
}
