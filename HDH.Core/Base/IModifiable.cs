﻿using System;

namespace HDH.Core.Base
{
    public interface IModifiable
    {
        string CreatedBy { get; set; }
        DateTimeOffset CreatedAt { get; set; }
        string ModifiedBy { get; set; }
        DateTimeOffset ModifiedAt { get; set; }
    }
}