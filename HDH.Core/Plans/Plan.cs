﻿using System;
using System.Collections.Generic;
using HDH.Core.Base;

namespace HDH.Core.Plans
{
    public class Plan : IEntity<int>, IModifiable
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public virtual List<PlanProduct> PlanProducts { get; set; }

        public string CreatedBy { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset ModifiedAt { get; set; }
    }
}