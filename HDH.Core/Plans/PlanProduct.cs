﻿using HDH.Core.Base;
using HDH.Core.Products;

namespace HDH.Core.Plans
{
    public class PlanProduct : IEntity
    {
        public int PlanId { get; set; }
        public virtual Plan Plan { get; set; }
        public string ProductKey { get; set; }
        public virtual Product Product { get; set; }
        public double Quantity { get; set; }
    }
}
