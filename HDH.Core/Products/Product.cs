﻿using System;
using System.Collections.Generic;
using HDH.Core.Base;
using HDH.Core.Sources;

namespace HDH.Core.Products
{
    public class Product : IEntity, IModifiable
    {
        public string Key { get; set; }
        public string Name { get; set; }
        public int Level { get; set; }
        public string SourceKey { get; set; }
        public long DefaultPrice { get; set; }
        public long MaxPrice { get; set; }
        public long? ProductionTime { get; set; }
        public int? ExperienceGained { get; set; }
        public int? MinPerCrate { get; set; }
        public int? MaxPerCrate { get; set; }

        public virtual Source Source { get; set; }
        public virtual ICollection<ProductIngredient> Ingredients { get; set; }
        public virtual ICollection<ProductIngredient> Products { get; set; }
        public virtual ICollection<ProductPreference> ProductPreferences { get; set; }

        public string CreatedBy { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset ModifiedAt { get; set; }
    }
}