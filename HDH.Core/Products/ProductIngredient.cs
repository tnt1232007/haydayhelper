﻿using HDH.Core.Base;

namespace HDH.Core.Products
{
    public class ProductIngredient : IEntity
    {
        public string ProductKey { get; set; }
        public string IngredientKey { get; set; }
        public double Quantity { get; set; }

        public virtual Product Product { get; set; }
        public virtual Product Ingredient { get; set; }
    }
}