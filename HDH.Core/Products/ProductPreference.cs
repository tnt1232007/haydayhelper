﻿using System;
using HDH.Core.Base;

namespace HDH.Core.Products
{
    public class ProductPreference : IEntity, IModifiable
    {
        public string UserId { get; set; }
        public string ProductKey { get; set; }
        public int? Rating { get; set; }
        public int? Stock { get; set; }

        public virtual Product Product { get; set; }

        public string CreatedBy { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset ModifiedAt { get; set; }
    }
}