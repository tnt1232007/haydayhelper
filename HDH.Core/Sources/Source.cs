﻿using System;
using System.Collections.Generic;
using HDH.Core.Base;
using HDH.Core.Products;

namespace HDH.Core.Sources
{
    public class Source : IEntity, IModifiable
    {
        public string Key { get; set; }
        public string Name { get; set; }
        public int Level { get; set; }

        public virtual ICollection<Product> Products { get; set; }
        public virtual ICollection<SourceInstance> SourceInstances { get; set; }
        public virtual ICollection<SourcePreference> SourcePreferences { get; set; }

        public string CreatedBy { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset ModifiedAt { get; set; }
    }
}