﻿using HDH.Core.Base;

namespace HDH.Core.Sources
{
    public class SourceInstance : IEntity
    {
        public string SourceKey { get; set; }
        public int Order { get; set; }
        public long Cost { get; set; }
        public long? BuildTime { get; set; }

        public Source Source { get; set; }
    }
}