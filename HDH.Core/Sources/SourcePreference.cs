﻿using System;
using HDH.Core.Base;

namespace HDH.Core.Sources
{
    public class SourcePreference : IEntity, IModifiable
    {
        public string UserId { get; set; }
        public string SourceKey { get; set; }
        public bool IsMastered { get; set; } = false;

        public Source Source { get; set; }

        public string CreatedBy { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset ModifiedAt { get; set; }
    }
}