﻿using System.Collections.Generic;

namespace HDH.Core.Static
{
    public class Country
    {
        public string Alpha2Code { get; set; }
        public string Alpha3Code { get; set; }
        public string[] CallingCodes { get; set; }
        public string Name { get; set; }
        public string Capital { get; set; }
        public string Region { get; set; }
        public string SubRegion { get; set; }
        public int? Population { get; set; }
        public double? Area { get; set; }
        public string[] Timezones { get; set; }
        public string Flag { get; set; }
        public List<Currency> Currencies { get; set; }
        public List<Language> Languages { get; set; }
    }
}
