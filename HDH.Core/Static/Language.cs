﻿namespace HDH.Core.Static
{
    public class Language
    {
        public string Iso6391 { get; set; }
        public string Iso6392 { get; set; }
        public string Name { get; set; }
    }
}