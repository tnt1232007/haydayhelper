﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using HDH.Core.Base;
using Microsoft.AspNetCore.Identity;

namespace HDH.Core.Users
{
    public class Role : IdentityRole, IEntity<string>, IModifiable
    {
        public string CreatedBy { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset ModifiedAt { get; set; }

        [NotMapped]
        public virtual IList<string> Permissions { get; set; }
    }
}