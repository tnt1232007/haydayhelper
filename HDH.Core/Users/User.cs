﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using HDH.Core.Base;
using Microsoft.AspNetCore.Identity;

namespace HDH.Core.Users
{
    public class User : IdentityUser, IEntity<string>, IModifiable
    {
        public bool Enabled { get; set; }
        public DateTimeOffset? LastSignedInAt { get; set; }

        public string Name { get; set; }
        public DateTimeOffset? Dob { get; set; }
        public string Avatar { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        public int? Level { get; set; }

        public string CreatedBy { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset ModifiedAt { get; set; }

        [NotMapped]
        public virtual IList<string> Roles { get; set; }
    }
}