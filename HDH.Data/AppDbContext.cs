﻿using HDH.Core.Plans;
using HDH.Core.Products;
using HDH.Core.Sources;
using HDH.Core.Users;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace HDH.Data
{
    public class AppDbContext : IdentityDbContext<User, Role, string>
    {
        public AppDbContext(DbContextOptions options) : base(options) { }

        public DbSet<Source> Sources { get; set; }
        public DbSet<SourceInstance> SourceInstances { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductIngredient> ProductIngredients { get; set; }
        public DbSet<ProductPreference> ProductPreferences { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>(o =>
            {
                o.HasIndex(m => m.Email).IsUnique();
            });
            modelBuilder.Entity<Product>(o =>
            {
                o.HasKey(m => m.Key);
                o.Property(m => m.SourceKey).IsRequired();
            });
            modelBuilder.Entity<ProductIngredient>(o =>
            {
                o.HasKey(m => new {m.ProductKey, m.IngredientKey});
                o.HasOne(m => m.Ingredient).WithMany(m => m.Products).OnDelete(DeleteBehavior.Restrict);
                o.HasOne(m => m.Product).WithMany(m => m.Ingredients).OnDelete(DeleteBehavior.Restrict);
            });
            modelBuilder.Entity<ProductPreference>(o =>
            {
                o.HasKey(m => new {m.UserId, m.ProductKey});
                o.HasOne(m => m.Product).WithMany(m => m.ProductPreferences);
            });

            modelBuilder.Entity<Source>(o =>
            {
                o.HasKey(m => m.Key);
            });
            modelBuilder.Entity<SourceInstance>(o =>
            {
                o.HasKey(m => new {m.SourceKey, m.Order});
            });
            modelBuilder.Entity<SourcePreference>(o =>
            {
                o.HasKey(m => new {m.UserId, m.SourceKey});
                o.HasOne(m => m.Source).WithMany(m => m.SourcePreferences);
            });

            modelBuilder.Entity<PlanProduct>(o =>
            {
                o.HasKey(m => new { m.PlanId, m.ProductKey });
                o.HasOne(m => m.Plan).WithMany(m => m.PlanProducts);
                o.HasOne(m => m.Product).WithMany();
            });
        }
    }
}