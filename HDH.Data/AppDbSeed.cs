﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using HDH.Common.Constants;
using HDH.Common.Extensions;
using HDH.Core.Users;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace HDH.Data
{
    public static class AppDbSeed
    {
        public static IWebHost SeedDb(this IWebHost host)
        {
            ILogger<AppDbContext> logger = null;
            try
            {
                using (var scope = host.Services.CreateScope())
                {
                    logger = scope.ServiceProvider.GetService<ILoggerFactory>().CreateLogger<AppDbContext>();

                    var context = scope.ServiceProvider.GetRequiredService<AppDbContext>();
                    context.Database.Migrate();

                    var roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<Role>>();
                    CreateRolesAsync(roleManager).Wait();

                    var userManager = scope.ServiceProvider.GetRequiredService<UserManager<User>>();
                    CreateUsersAsync(userManager).Wait();
                }
            }
            catch (Exception ex)
            {
                logger?.LogError(ex, "Failed to initilize database");
            }
            return host;
        }

        private static async Task CreateRolesAsync(RoleManager<Role> roleManager)
        {
            await CreateRoleAsync(
                roleManager,
                Roles.Administrator);

            await CreateRoleAsync(
                roleManager,
                Roles.Staff,
                Permission.UserRead,
                Permission.UserUpdate,
                Permission.UserManage,
                Permission.ProductCreate,
                Permission.ProductManage,
                Permission.SourceCreate,
                Permission.SourceManage,
                Permission.SettingRead,
                Permission.SettingUpdate,
                Permission.SettingManage);

            await CreateRoleAsync(
                roleManager,
                Roles.User,
                Permission.UserRead,
                Permission.UserUpdate,
                Permission.ProductPreferenceRead,
                Permission.ProductPreferenceUpdate,
                Permission.SourcePreferenceRead,
                Permission.SourcePreferenceUpdate,
                Permission.PlanCreate,
                Permission.PlanRead,
                Permission.PlanUpdate,
                Permission.PlanDelete,
                Permission.SettingRead,
                Permission.SettingUpdate);
        }

        private static async Task CreateUsersAsync(UserManager<User> userManager)
        {
            await CreateUserAsync(userManager, "admin", "admin@haydayhelper-api.com", Roles.Administrator);
        }

        private static async Task<Role> CreateRoleAsync(RoleManager<Role> roleManager, string roleName, params Permission[] permissions)
        {
            var role = await roleManager.FindByIdAsync(roleName);
            if (role != null) return role;

            role = new Role
            {
                Id = roleName,
                Name = roleName
            };
            role.CreatedBy(null);
            await roleManager.CreateAsync(role);
            foreach (var permission in permissions)
                await roleManager.AddClaimAsync(role, new Claim(CustomClaimTypes.Permission, permission.ToString()));
            return role;
        }

        private static async Task<User> CreateUserAsync(UserManager<User> userManager, string name, string email, string role)
        {
            var user = await userManager.FindByNameAsync(name);
            if (user != null) return user;

            user = new User
            {
                UserName = name,
                Email = email,
                Enabled = true
            };
            user.CreatedBy(null);
            await userManager.CreateAsync(user, "p@ssword");
            await userManager.AddToRoleAsync(user, role);
            return user;
        }
    }
}