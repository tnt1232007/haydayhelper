﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using HDH.Common.Pagings;
using HDH.Core.Base;

namespace HDH.Data
{
    public interface IRepository<T> where T : IEntity
    {
        IQueryable<T> Table { get; }
        IQueryable<T> TableNoTracking { get; }
        Task<T> FindAsync(object[] ids, CancellationToken token = default(CancellationToken));
        Task<T> FindAsync(Expression<Func<T, bool>> predicate, CancellationToken token = default(CancellationToken));
        Task<IEnumerable<T>> FindAllAsync(Paging paging, CancellationToken token = default(CancellationToken));
        Task AddAsync(T entity);
        void Update(T entity);
        void Delete(T entity);
    }

    public interface IRepository<T, in TKey> : IRepository<T>
        where T : class, IEntity<TKey> where TKey : IEquatable<TKey> { }
}