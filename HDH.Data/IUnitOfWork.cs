﻿using System.Threading.Tasks;
using HDH.Core.Plans;
using HDH.Core.Products;
using HDH.Core.Sources;

namespace HDH.Data
{
    public interface IUnitOfWork
    {
        IRepository<Product> ProductRepository { get; }
        IRepository<ProductIngredient> ProductIngredientRepository { get; }
        IRepository<ProductPreference> ProductPreferenceRepository { get; }

        IRepository<Source> SourceRepository { get; }
        IRepository<SourceInstance> SourceInstanceRepository { get; }
        IRepository<SourcePreference> SourcePreferenceRepository { get; }

        IRepository<Plan> PlanRepository { get; }

        Task CommitAsync();
    }
}