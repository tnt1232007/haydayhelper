﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace HDH.Data.Migrations
{
    public partial class RenameColumnToRating : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Rank",
                table: "ProductPreferences");

            migrationBuilder.AddColumn<int>(
                name: "Rating",
                table: "ProductPreferences",
                type: "int4",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Rating",
                table: "ProductPreferences");

            migrationBuilder.AddColumn<int>(
                name: "Rank",
                table: "ProductPreferences",
                nullable: true);
        }
    }
}
