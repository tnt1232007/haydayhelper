﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace HDH.Data.Migrations
{
    public partial class UpdatePlanTables_2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PlanProduct_Plan_PlanId",
                table: "PlanProduct");

            migrationBuilder.DropForeignKey(
                name: "FK_PlanProduct_Products_ProductKey",
                table: "PlanProduct");

            migrationBuilder.AddForeignKey(
                name: "FK_PlanProduct_Plan_PlanId",
                table: "PlanProduct",
                column: "PlanId",
                principalTable: "Plan",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PlanProduct_Products_ProductKey",
                table: "PlanProduct",
                column: "ProductKey",
                principalTable: "Products",
                principalColumn: "Key",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PlanProduct_Plan_PlanId",
                table: "PlanProduct");

            migrationBuilder.DropForeignKey(
                name: "FK_PlanProduct_Products_ProductKey",
                table: "PlanProduct");

            migrationBuilder.AddForeignKey(
                name: "FK_PlanProduct_Plan_PlanId",
                table: "PlanProduct",
                column: "PlanId",
                principalTable: "Plan",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PlanProduct_Products_ProductKey",
                table: "PlanProduct",
                column: "ProductKey",
                principalTable: "Products",
                principalColumn: "Key",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
