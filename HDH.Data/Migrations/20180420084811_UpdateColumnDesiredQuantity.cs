﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace HDH.Data.Migrations
{
    public partial class UpdateColumnDesiredQuantity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DesiredQuantity",
                table: "PlanProduct");

            migrationBuilder.AddColumn<double>(
                name: "Quantity",
                table: "PlanProduct",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Quantity",
                table: "PlanProduct");

            migrationBuilder.AddColumn<int>(
                name: "DesiredQuantity",
                table: "PlanProduct",
                nullable: false,
                defaultValue: 0);
        }
    }
}
