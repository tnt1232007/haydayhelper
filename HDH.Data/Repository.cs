using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using HDH.Common.Extensions;
using HDH.Common.Pagings;
using HDH.Core.Base;
using Microsoft.EntityFrameworkCore;

namespace HDH.Data
{
    public class Repository<T> : IRepository<T> where T : class, IEntity
    {
        private readonly DbContext _context;

        public Repository(DbContext context)
        {
            _context = context;
        }

        private DbSet<T> Entities => _context.Set<T>();

        public virtual Task<T> FindAsync(object[] ids, CancellationToken token)
        {
            return Entities.FindAsync(ids, token);
        }

        public virtual Task<T> FindAsync(Expression<Func<T, bool>> predicate, CancellationToken token)
        {
            return Entities.FirstOrDefaultAsync(predicate, token);
        }

        public virtual Task<IEnumerable<T>> FindAllAsync(Paging paging, CancellationToken token)
        {
            return Entities.FindAllAsync(paging, token);
        }

        public virtual async Task AddAsync(T entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            await Entities.AddAsync(entity);
        }

        public virtual void Update(T entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            Entities.Update(entity);
        }

        public virtual void Delete(T entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            Entities.Remove(entity);
        }

        public virtual IQueryable<T> Table => Entities;

        public virtual IQueryable<T> TableNoTracking => Entities.AsNoTracking();
    }

    public class Repository<T, TKey> : Repository<T>, IRepository<T, TKey>
        where T : class, IEntity<TKey> where TKey : IEquatable<TKey>
    {
        public Repository(DbContext context) : base(context) { }
    }
}