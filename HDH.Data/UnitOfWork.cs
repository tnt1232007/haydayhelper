﻿using System.Threading.Tasks;
using HDH.Core.Plans;
using HDH.Core.Products;
using HDH.Core.Sources;

namespace HDH.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _context;

        public UnitOfWork(AppDbContext context)
        {
            _context = context;

            ProductRepository = new Repository<Product>(context);
            ProductIngredientRepository = new Repository<ProductIngredient>(context);
            ProductPreferenceRepository = new Repository<ProductPreference>(context);

            SourceRepository = new Repository<Source>(context);
            SourceInstanceRepository = new Repository<SourceInstance>(context);
            SourcePreferenceRepository = new Repository<SourcePreference>(context);

            PlanRepository = new Repository<Plan>(context);
        }

        public IRepository<Product> ProductRepository { get; }
        public IRepository<ProductIngredient> ProductIngredientRepository { get; }
        public IRepository<ProductPreference> ProductPreferenceRepository { get; }

        public IRepository<Source> SourceRepository { get; }
        public IRepository<SourceInstance> SourceInstanceRepository { get; }
        public IRepository<SourcePreference> SourcePreferenceRepository { get; }

        public IRepository<Plan> PlanRepository { get; }

        public Task CommitAsync() => _context.SaveChangesAsync();
    }
}