﻿using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;
using HDH.Core.Users;

namespace HDH.Service.Auths
{
    public interface IAuthService
    {
        Task<JwtSecurityToken> GetJwtSecurityToken(User user);
    }
}