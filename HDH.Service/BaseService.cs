﻿using HDH.Data;
using Microsoft.AspNetCore.Authorization;

namespace HDH.Service
{
    public class BaseService
    {
        protected readonly IAuthorizationService AuthorizationService;
        protected readonly IUnitOfWork UnitOfWork;

        protected BaseService(IAuthorizationService authorizationService, IUnitOfWork unitOfWork)
        {
            AuthorizationService = authorizationService;
            UnitOfWork = unitOfWork;
        }
    }
}