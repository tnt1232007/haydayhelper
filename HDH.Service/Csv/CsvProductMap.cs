﻿using System;
using System.Collections.Generic;
using System.Linq;
using CsvHelper;
using CsvHelper.Configuration;
using HDH.Core.Products;
using HDH.Core.Sources;

namespace HDH.Service.Csv
{
    public sealed class CsvProductMap : ClassMap<Product>
    {
        public CsvProductMap()
        {
            Map(o => o.Source).ConvertUsing(row => new Source {Name = row.GetField("Production Category")});
            Map(o => o.Name).Name("Product");
            Map(o => o.Level).Name("Level");
            Map(o => o.DefaultPrice).Name("Default Price");
            Map(o => o.MaxPrice).Name("Max Price");
            Map(o => o.ProductionTime).ConvertUsing(row =>
                double.TryParse(row.GetField("Production Time"), out var result) && result > 0
                    ? TimeSpan.FromMinutes(result).Ticks
                    : (long?) null);
            Map(o => o.ExperienceGained).ConvertUsing(row =>
                int.TryParse(row.GetField("Experience"), out var result) ? result : (int?) null);
            Map(o => o.MinPerCrate).ConvertUsing(
                row =>
                {
                    var min = row.GetField("# Per Crate")?.Split('-').ElementAtOrDefault(0)?.Trim();
                    return min != null && int.TryParse(min, out var result) ? result : (int?) null;
                });
            Map(o => o.MaxPerCrate).ConvertUsing(
                row =>
                {
                    var min = row.GetField("# Per Crate")?.Split('-').ElementAtOrDefault(1)?.Trim();
                    return min != null && int.TryParse(min, out var result) ? result : (int?) null;
                });
            Map(o => o.Ingredients).ConvertUsing(
                row =>
                {
                    var list = new List<ProductIngredient>();
                    for (var i = 1; i <= 4; i++)
                    {
                        if (!double.TryParse(row.GetField($"Qty {i}"), out var quantity))
                            continue;
                        list.Add(
                            new ProductIngredient
                            {
                                Quantity = CalculateQuantity(row, i, quantity),
                                Product = new Product {Name = row.GetField("Product")},
                                Ingredient = new Product {Name = row.GetField($"Ingredient {i}")}
                            });
                    }
                    return list;
                });
            Map(o => o.Key).ConvertUsing(row => row.GetField("Product").Replace(" ", "-").ToLower());
        }

        private static double CalculateQuantity(IReaderRow row, int i, double quantity)
        {
            var source = row.GetField("Production Category");
            var ingredient = row.GetField($"Ingredient {i}");

            switch (source)
            {
                case "Tree or Bush" when ingredient == "Coin":
                    quantity = quantity * 13;
                    quantity = Math.Round(quantity / 5) * 5;
                    quantity = quantity / 13;
                    break;
                case "Tree or Bush" when ingredient == "Axe" || ingredient == "Saw":
                    quantity = quantity * 13;
                    quantity = Math.Round(quantity);
                    quantity = quantity / 13;
                    break;
                case "Feed Mill":
                    quantity = quantity * 3;
                    quantity = Math.Round(quantity);
                    quantity = quantity / 3;
                    break;
            }
            return quantity;
        }
    }

    public class CsvProductExport
    {
        public int Order { get; set; }
        public string Key { get; set; }
        public int Level { get; set; }
        public string SourceKey { get; set; }
        public long DefaultPrice { get; set; }
        public long MaxPrice { get; set; }
        public long? ProductionTime { get; set; }
        public int? ExperienceGained { get; set; }
        public int? MinPerCrate { get; set; }
        public int? MaxPerCrate { get; set; }
    }
}