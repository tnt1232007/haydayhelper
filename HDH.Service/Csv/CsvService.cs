﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using CsvHelper;

namespace HDH.Service.Csv
{
    public class CsvService : ICsvService
    {
        public List<T> Read<T>(Stream stream)
        {
            using (var reader = new StreamReader(stream))
            {
                var csv = new CsvReader(reader);
                csv.Configuration.HeaderValidated = null;
                csv.Configuration.RegisterClassMap<CsvProductMap>();
                csv.Configuration.RegisterClassMap<CsvSourceMap>();
                return csv.GetRecords<T>().ToList();
            }
        }

        public byte[] Write<T>(IEnumerable<T> records)
        {
            using (var memoryStream = new MemoryStream())
            using (var streamWriter = new StreamWriter(memoryStream))
            using (var csv = new CsvWriter(streamWriter))
            {
                csv.WriteRecords(records);
                streamWriter.Flush();
                return memoryStream.ToArray();
            }
        }
    }
}