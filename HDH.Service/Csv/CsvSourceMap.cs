﻿using System;
using System.Collections.Generic;
using CsvHelper.Configuration;
using HDH.Core.Sources;

namespace HDH.Service.Csv
{
    public sealed class CsvSourceMap : ClassMap<Source>
    {
        public CsvSourceMap()
        {
            Map(m => m.Name).Index(0);
            Map(m => m.Level).Name("UnlockLvl");
            Map(m => m.SourceInstances).ConvertUsing(
                row => new List<SourceInstance>
                {
                    new SourceInstance
                    {
                        BuildTime = double.TryParse(row.GetField("CompleteTime"), out var result) && result > 0
                            ? TimeSpan.FromMinutes(result).Ticks
                            : (long?) null,
                        Cost = row.GetField<long>("MCost")
                    }
                });
        }
    }

    public class CsvSourceExport
    {
        public int Order { get; set; }
        public string Key { get; set; }
        public int Level { get; set; }
    }
}