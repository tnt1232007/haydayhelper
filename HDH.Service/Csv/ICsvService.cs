﻿using System.Collections.Generic;
using System.IO;

namespace HDH.Service.Csv
{
    public interface ICsvService
    {
        List<T> Read<T>(Stream stream);

        byte[] Write<T>(IEnumerable<T> records);
    }
}