﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Threading.Tasks;
using HDH.Common.Exceptions;
using HDH.Common.Extensions;
using HDH.Common.Models;

namespace HDH.Service.Demo
{
    [Obsolete]
    internal class DemoService : IDemoService
    {
        private readonly IUnitOfWork UnitOfWork;

        public DemoService(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;
        }

        public Task<Demo> FindAsync(params object[] ids)
        {
            return UnitOfWork.DemoRepository.FindAsync(ids);
        }

        public Task<Demo> FindAsync(Expression<Func<Demo, bool>> predicate)
        {
            return UnitOfWork.DemoRepository.FindAsync(predicate);
        }

        public Task<IEnumerable<Demo>> FindAllAsync(Paging paging)
        {
            return UnitOfWork.DemoRepository.FindAllAsync(paging);
        }

        public async Task<Demo> CreateAsync(ClaimsPrincipal user, Demo entity)
        {
            entity.CreatedBy(user);
            await UnitOfWork.DemoRepository.Add(entity);
            await UnitOfWork.CommitAsync();
            return entity;
        }

        public async Task<Demo> UpdateAsync(ClaimsPrincipal user, Demo entity)
        {
            entity.UpdatedBy(user);
            await UnitOfWork.CommitAsync();
            return entity;
        }

        public async Task DeleteAsync(Demo entity)
        {
            UnitOfWork.DemoRepository.Delete(entity);
            await UnitOfWork.CommitAsync();
        }

        public async Task DeleteAsync(params object[] ids)
        {
            var entity = await FindAsync(ids);
            if (entity == null) throw new ResourceNotFoundException();

            await DeleteAsync(entity);
        }
    }
}