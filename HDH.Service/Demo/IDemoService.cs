using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Threading.Tasks;
using HDH.Common.Models;
using HDH.Core.Base;
using HDH.Data;

namespace HDH.Service.Demo
{
    [Obsolete]
    internal interface IDemoService
    {
        Task<Demo> FindAsync(params object[] ids);
        Task<Demo> FindAsync(Expression<Func<Demo, bool>> predicate);
        Task<IEnumerable<Demo>> FindAllAsync(Paging paging);
        Task<Demo> CreateAsync(ClaimsPrincipal user, Demo entity);
        Task<Demo> UpdateAsync(ClaimsPrincipal user, Demo entity);
        Task DeleteAsync(Demo entity);
        Task DeleteAsync(params object[] ids);
    }

    #region Mock

    [Obsolete]
    internal interface IUnitOfWork : Data.IUnitOfWork
    {
        Repository<Demo> DemoRepository { get; }

    }

    [Obsolete]
    internal abstract class Demo : IEntity, IModifiable
    {
        public string CreatedBy { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset ModifiedAt { get; set; }
    }

    #endregion
}