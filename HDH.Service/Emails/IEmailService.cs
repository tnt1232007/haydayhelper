﻿using System.Threading.Tasks;
using HDH.Common.Emails;

namespace HDH.Service.Emails
{
    public interface IEmailService
    {
        Task SendAsync(EmailRequest emailRequest);
        Task SendResetPasswordAsync(string email, string resetLink);
    }
}