﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using HDH.Core.Plans;

namespace HDH.Service.Plans {
    public interface IPlanService
    {
        Task<IEnumerable<Plan>> GetAllAsync(ClaimsPrincipal user, CancellationToken token = default(CancellationToken));
        Task<Plan> GetAsync(ClaimsPrincipal user, int id, CancellationToken token = default(CancellationToken));
        Task<Plan> CreateAsync(ClaimsPrincipal user, Plan entity);
        Task<Plan> UpdateAsync(ClaimsPrincipal user, Plan entity);
        Task DeleteAsync(ClaimsPrincipal user, Plan entity);
    }
}