﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using HDH.Common.Constants;
using HDH.Common.Exceptions;
using HDH.Common.Extensions;
using HDH.Core.Plans;
using HDH.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;

namespace HDH.Service.Plans
{
    public class PlanService : BaseService, IPlanService
    {
        private readonly IRepository<Plan> _repository;

        public PlanService(IAuthorizationService authorizationService, IUnitOfWork unitOfWork) : base(authorizationService, unitOfWork)
        {
            _repository = UnitOfWork.PlanRepository;
        }

        public async Task<IEnumerable<Plan>> GetAllAsync(ClaimsPrincipal user, CancellationToken token = default(CancellationToken))
        {
            if (!await AuthorizationService.AuthorizeAsync(user, Permission.PlanRead))
                throw new UnauthorizedAccessException();

            return await UnitOfWork.PlanRepository
                .TableNoTracking.Include(o => o.PlanProducts).ThenInclude(o => o.Product)
                .Where(o => o.UserId == user.GetId() || user.IsInRole(Roles.Administrator)).ToListAsync(token);
        }

        public async Task<Plan> GetAsync(ClaimsPrincipal user, int id, CancellationToken token = default(CancellationToken))
        {
            if (!await AuthorizationService.AuthorizeAsync(user, Permission.PlanRead))
                throw new UnauthorizedAccessException();

            return await UnitOfWork.PlanRepository
                .TableNoTracking.Include(o => o.PlanProducts).ThenInclude(o => o.Product)
                .FirstOrDefaultAsync(o => (o.UserId == user.GetId() || user.IsInRole(Roles.Administrator)) && o.Id == id, token);
        }

        public async Task<Plan> CreateAsync(ClaimsPrincipal user, Plan entity)
        {
            if (!await AuthorizationService.AuthorizeAsync(user, Permission.PlanCreate))
                throw new UnauthorizedAccessException();

            foreach (var planProduct in entity.PlanProducts)
            {
                planProduct.Product = await UnitOfWork.ProductRepository.FindAsync(new object[] { planProduct.ProductKey });
                if (planProduct.Product == null)
                    throw new ResourceNotFoundException(nameof(planProduct.Product), planProduct.ProductKey);
            }

            entity.CreatedBy(user);
            await _repository.AddAsync(entity);
            await UnitOfWork.CommitAsync();
            return entity;
        }

        public async Task<Plan> UpdateAsync(ClaimsPrincipal user, Plan entity)
        {
            if (!await AuthorizationService.AuthorizeAsync(user, Permission.PlanUpdate))
                throw new UnauthorizedAccessException();

            foreach (var planProduct in entity.PlanProducts)
            {
                planProduct.Product = await UnitOfWork.ProductRepository.FindAsync(new object[] { planProduct.ProductKey });
                if (planProduct.Product == null)
                    throw new ResourceNotFoundException(nameof(planProduct.Product), planProduct.ProductKey);
            }

            entity.UpdatedBy(user);
            _repository.Update(entity);
            await UnitOfWork.CommitAsync();
            return entity;
        }
        
        public async Task DeleteAsync(ClaimsPrincipal user, Plan entity)
        {
            if (!await AuthorizationService.AuthorizeAsync(user, Permission.PlanDelete))
                throw new UnauthorizedAccessException();

            _repository.Delete(entity);
            await UnitOfWork.CommitAsync();
        }
    }
}
