﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using HDH.Common.Pagings;
using HDH.Core.Products;

namespace HDH.Service.Products
{
    public interface IProductService
    {
        IQueryable<Product> Query();
        Task<IEnumerable<Product>> AllReadOnlyAsync(CancellationToken token = default(CancellationToken));
        Task<IEnumerable<Product>> QueryByKeywordAsync(string keyword, Paging paging, CancellationToken token = default(CancellationToken));
        Task<IEnumerable<Product>> QueryBySourceAsync(string sourceKey, Paging paging, CancellationToken token = default(CancellationToken));
        Task<IEnumerable<Product>> GetRelativesAsync(Product product, CancellationToken token = default(CancellationToken));
        Task<IEnumerable<ProductIngredient>> GetIngredientsAsync(Expression<Func<ProductIngredient, bool>> predicate = null, CancellationToken token = default(CancellationToken));
        Task<Product> GetAsync(string key, CancellationToken token = default(CancellationToken));
        Task LoadAllPreferencesAsync(ClaimsPrincipal user, CancellationToken token = default(CancellationToken));
        Task<Product> CreateOrUpdateAsync(ClaimsPrincipal user, Product entity);
        Task<ProductPreference> UpdatePreferenceAsync(ClaimsPrincipal user, string key, ProductPreference productPreference);
        Task DeleteAllAsync(ClaimsPrincipal user);

        Task ImportAsync(ClaimsPrincipal user, List<Product> products);
        Task<List<Product>> ExportAsync(ClaimsPrincipal user, CancellationToken token);
    }
}