﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using HDH.Common.Comparer;
using HDH.Common.Constants;
using HDH.Common.Exceptions;
using HDH.Common.Extensions;
using HDH.Common.Pagings;
using HDH.Core.Products;
using HDH.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;

namespace HDH.Service.Products
{
    public class ProductService : BaseService, IProductService
    {
        private readonly IRepository<Product> _repository;

        public ProductService(IAuthorizationService authorizationService, IUnitOfWork unitOfWork)
            : base(authorizationService, unitOfWork)
        {
            _repository = UnitOfWork.ProductRepository;
        }

        public IQueryable<Product> Query() => UnitOfWork.ProductRepository.Table;

        public async Task<IEnumerable<Product>> AllReadOnlyAsync(CancellationToken token)
        {
            return await UnitOfWork.ProductRepository.TableNoTracking.OrderBy(o => o.Level).ToListAsync(token);
        }

        public async Task<IEnumerable<Product>> QueryByKeywordAsync(string keyword, Paging paging, CancellationToken token)
        {
            keyword = keyword?.ToLower().Trim() ?? string.Empty;
            var entities = await _repository.Table
                .Where(o => o.Name.ToLower().Contains(keyword)
                            || o.Source.Name.ToLower().Contains(keyword)
                            || o.Ingredients.Any(m => m.Ingredient.Name.ToLower().Contains(keyword)))
                .OrderBy(o => o.Name.ToLower() == keyword ? 0 : 1)
                .ThenBy(o => o.Name.ToLower().StartsWith(keyword) ? 0 : 1)
                .ThenBy(o => o.Name.ToLower().Contains(keyword) ? 0 : 1)
                .ThenBy(o => keyword != string.Empty ? o.Name : string.Empty)
                .ThenBy(o => o.Source.Name.ToLower().Contains(keyword) ? 0 : 1)
                .ThenBy(o => o.Level)
                .Include(o => o.Source)
                .FindAllAsync(paging, token);

            // HACK: .Include(o => o.Ingredients).ThenInclude(o => o.Ingredient) not working with .Where(o => o.Ingredients...)
            var ingredients = await UnitOfWork.ProductIngredientRepository.Table
                .Where(o => entities.Select(m => m.Key).Contains(o.ProductKey))
                .ToListAsync(token);
            await _repository.Table
                .Where(o => ingredients.Select(m => m.IngredientKey).Contains(o.Key))
                .LoadAsync(token);

            return entities;
        }

        public Task<IEnumerable<Product>> QueryBySourceAsync(string sourceKey, Paging paging, CancellationToken token)
        {
            return _repository.Table
                .Where(o => o.SourceKey == sourceKey)
                .OrderBy(o => o.Level)
                .Include(o => o.Ingredients).ThenInclude(o => o.Ingredient)
                .FindAllAsync(paging, token);
        }

        public async Task<IEnumerable<Product>> GetRelativesAsync(Product product, CancellationToken token)
        {
            return await _repository.Table
                .Where(o => o.SourceKey == product.SourceKey && o.Key != product.Key)
                .OrderBy(o => o.Level).ToListAsync(token);
        }

        public async Task<IEnumerable<ProductIngredient>> GetIngredientsAsync(Expression<Func<ProductIngredient, bool>> predicate, CancellationToken token)
        {
            var query = UnitOfWork.ProductIngredientRepository.TableNoTracking;
            query = predicate != null ? query.Where(predicate) : query;
            return await query.ToListAsync(token);
        }

        public async Task<Product> GetAsync(string key, CancellationToken token)
        {
            return await _repository.Table
                .Include(o => o.Source)
                .Include(o => o.Ingredients).ThenInclude(o => o.Ingredient)
                .Include(o => o.Products).ThenInclude(o => o.Product)
                .FirstOrDefaultAsync(o => o.Key == key, token);
        }
        
        public async Task LoadAllPreferencesAsync(ClaimsPrincipal user, CancellationToken token)
        {
            if (await AuthorizationService.AuthorizeAsync(user, Permission.ProductPreferenceRead))
            {
                await UnitOfWork.ProductPreferenceRepository.Table.Include(o => o.Product).Where(o => o.UserId == user.GetId()).LoadAsync(token);
                await UnitOfWork.SourcePreferenceRepository.Table.Include(o => o.Source).Where(o => o.UserId == user.GetId()).LoadAsync(token);
            }
        }

        public async Task<Product> CreateOrUpdateAsync(ClaimsPrincipal user, Product entity)
        {
            if (!await AuthorizationService.AuthorizeAsync(user, Permission.ProductCreate, Permission.ProductManage))
                throw new UnauthorizedAccessException();

            var exist = await _repository.Table
                .Include(o => o.Ingredients)
                .FirstOrDefaultAsync(o => o.Key == entity.Key);
            if (exist == null)
            {
                entity.Source = await UnitOfWork.SourceRepository.FindAsync(new object[] {entity.SourceKey});
                if (entity.Source == null)
                    throw new ResourceNotFoundException(nameof(entity.Source), entity.SourceKey);

                entity.CreatedBy(user);
                await _repository.AddAsync(entity);
                await UnitOfWork.CommitAsync();
                return entity;
            }
            else
            {
                exist.Name = entity.Name;
                exist.Level = entity.Level;
                exist.SourceKey = entity.SourceKey;
                exist.DefaultPrice = entity.DefaultPrice;
                exist.MaxPrice = entity.MaxPrice;
                exist.ProductionTime = entity.ProductionTime;
                exist.ExperienceGained = entity.ExperienceGained;
                exist.MinPerCrate = entity.MinPerCrate;
                exist.MaxPerCrate = entity.MinPerCrate;
                foreach (var ingredient in exist.Ingredients.Except(entity.Ingredients, new ProductIngredientComparer()).ToList())
                    exist.Ingredients.Remove(ingredient);
                foreach (var ingredient in entity.Ingredients.Except(exist.Ingredients, new ProductIngredientComparer()))
                    exist.Ingredients.Add(ingredient);

                entity.UpdatedBy(user);
                _repository.Update(exist);
                await UnitOfWork.CommitAsync();
                return exist;
            }
        }

        public async Task<ProductPreference> UpdatePreferenceAsync(ClaimsPrincipal user, string key, ProductPreference productPreference)
        {
            if (!await AuthorizationService.AuthorizeAsync(user, Permission.ProductPreferenceUpdate))
                throw new UnauthorizedAccessException();

            var exist = await UnitOfWork.ProductPreferenceRepository.FindAsync(new object[] {user.GetId(), key});
            if (exist != null)
            {
                exist.Rating = productPreference.Rating;
                exist.Stock = productPreference.Stock;
                exist.UpdatedBy(user);
                await UnitOfWork.CommitAsync();
                return exist;
            }
            productPreference.UserId = user.GetId();
            productPreference.ProductKey = key;
            productPreference.CreatedBy(user);
            await UnitOfWork.ProductPreferenceRepository.AddAsync(productPreference);
            await UnitOfWork.CommitAsync();
            return productPreference;
        }
        
        public async Task DeleteAllAsync(ClaimsPrincipal user)
        {
            if (!await AuthorizationService.AuthorizeAsync(user, Permission.ProductManage))
                throw new UnauthorizedAccessException();

            foreach (var entity in UnitOfWork.ProductIngredientRepository.Table)
                UnitOfWork.ProductIngredientRepository.Delete(entity);
            foreach (var entity in _repository.Table)
                _repository.Delete(entity);
            await UnitOfWork.CommitAsync();
        }

        public async Task ImportAsync(ClaimsPrincipal user, List<Product> products)
        {
            if (!await AuthorizationService.AuthorizeAsync(user, Permission.ProductCreate))
                throw new UnauthorizedAccessException();

            var ingredients = products.SelectMany(o => o.Ingredients).ToList();

            // Insert product information
            var sources = await UnitOfWork.SourceRepository.TableNoTracking.ToListAsync();
            products.ForEach(o =>
            {
                var source = sources.Find(m => m.Name == o.Source.Name);
                if (source == null)
                    throw new Exception($"Invalid source name \"{o.Source.Name}\"");
                o.SourceKey = source.Key;
                o.Source = null;
                o.Ingredients = null;
            });

            var orderedProducts = products.OrderBy(o => o.Level).ToList();
            foreach (var product in orderedProducts)
                product.CreatedBy(user);

            var tasks = orderedProducts.Select(o => _repository.AddAsync(o));
            await Task.WhenAll(tasks);
            await UnitOfWork.CommitAsync();

            // Insert product ingredients
            ingredients.ForEach(o =>
            {
                var ingredient = products.Find(m => m.Name == o.Ingredient.Name);
                if (ingredient == null)
                    throw new Exception($"Invalid product name \"{o.Ingredient.Name}\"");
                o.IngredientKey = ingredient.Key;
                o.Ingredient = null;

                var product = products.Find(m => m.Name == o.Product.Name);
                if (product == null)
                    throw new Exception($"Invalid product name \"{o.Product.Name}\"");
                o.ProductKey = product.Key;
                o.Product = null;
            });

            tasks = ingredients.Select(o => UnitOfWork.ProductIngredientRepository.AddAsync(o));
            await Task.WhenAll(tasks);
            await UnitOfWork.CommitAsync();
        }

        public async Task<List<Product>> ExportAsync(ClaimsPrincipal user, CancellationToken token)
        {
            if (!await AuthorizationService.AuthorizeAsync(user, Permission.ProductManage))
                throw new UnauthorizedAccessException();

            return await _repository.TableNoTracking
                .Include(o => o.Ingredients)
                .OrderBy(o => o.Level)
                .ToListAsync(token);
        }
    }
}