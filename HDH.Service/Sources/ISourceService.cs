﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using HDH.Common.Pagings;
using HDH.Core.Sources;

namespace HDH.Service.Sources
{
    public interface ISourceService
    {
        Task<IEnumerable<Source>> AllReadOnlyAsync(CancellationToken token = default(CancellationToken));
        Task<IEnumerable<Source>> QueryByKeywordAsync(string keyword, Paging paging, CancellationToken token = default(CancellationToken));
        Task<Source> GetAsync(string key, CancellationToken token = default(CancellationToken));
        Task LoadAllPreferencesAsync(ClaimsPrincipal user, CancellationToken token = default(CancellationToken));
        Task<Source> CreateOrUpdateAsync(ClaimsPrincipal user, Source entity);
        Task<SourcePreference> UpdatePreferenceAsync(ClaimsPrincipal user, string key, SourcePreference entity);
        Task DeleteAllAsync(ClaimsPrincipal user);
        Task ImportAsync(ClaimsPrincipal user, List<Source> sources);
        Task<List<Source>> ExportAsync(ClaimsPrincipal user, CancellationToken token);
    }
}