﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using HDH.Common.Constants;
using HDH.Common.Extensions;
using HDH.Common.Pagings;
using HDH.Core.Sources;
using HDH.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;

namespace HDH.Service.Sources
{
    public class SourceService : BaseService, ISourceService
    {
        public SourceService(IAuthorizationService authorizationService, IUnitOfWork unitOfWork)
            : base(authorizationService, unitOfWork) { }

        public async Task<IEnumerable<Source>> AllReadOnlyAsync(CancellationToken token)
        {
            return await UnitOfWork.SourceRepository.TableNoTracking.OrderBy(o => o.Level).ToListAsync(token);
        }

        public async Task<IEnumerable<Source>> QueryByKeywordAsync(string keyword, Paging paging, CancellationToken token)
        {
            keyword = keyword?.ToLower().Trim() ?? string.Empty;
            var entities = await UnitOfWork.SourceRepository.Table
                .Where(o => o.Name.ToLower().Contains(keyword) || o.Products.Any(m => m.Name.ToLower().Contains(keyword)))
                .OrderBy(o => o.Name.ToLower() == keyword ? 0 : 1)
                .ThenBy(o => o.Name.ToLower().StartsWith(keyword) ? 0 : 1)
                .ThenBy(o => o.Name.ToLower().Contains(keyword) ? 0 : 1)
                .ThenBy(o => keyword == string.Empty ? string.Empty : o.Name)
                .ThenBy(o => o.Level)
                .Include(o => o.SourceInstances)
                .FindAllAsync(paging, token);

            // HACK: .Include(o => o.Products) not working with .Where(o => o.Products...)
            await UnitOfWork.ProductRepository.Table
                .Where(o => entities.Select(m => m.Key).Contains(o.SourceKey))
                .LoadAsync(token);

            return entities;
        }

        public Task<Source> GetAsync(string key, CancellationToken token)
        {
            return UnitOfWork.SourceRepository.Table
                .Include(o => o.SourceInstances)
                .FirstOrDefaultAsync(o => o.Key == key, token);
        }
        
        public async Task LoadAllPreferencesAsync(ClaimsPrincipal user, CancellationToken token)
        {
            if (await AuthorizationService.AuthorizeAsync(user, Permission.SourcePreferenceRead))
            {
                await UnitOfWork.ProductPreferenceRepository.Table.Include(o => o.Product).Where(o => o.UserId == user.GetId()).LoadAsync(token);
                await UnitOfWork.SourcePreferenceRepository.Table.Include(o => o.Source).Where(o => o.UserId == user.GetId()).LoadAsync(token);
            }
        }

        public async Task<Source> CreateOrUpdateAsync(ClaimsPrincipal user, Source entity)
        {
            if (!await AuthorizationService.AuthorizeAsync(user, Permission.SourceCreate, Permission.SourceManage))
                throw new UnauthorizedAccessException();

            var exist = await UnitOfWork.SourceRepository.Table
                .Include(o => o.Products)
                .Include(o => o.SourceInstances)
                .FirstOrDefaultAsync(o => o.Key == entity.Key);
            if (exist == null)
            {
                entity.CreatedBy(user);
                await UnitOfWork.SourceRepository.AddAsync(entity);
                await UnitOfWork.CommitAsync();
                return entity;
            }
            else
            {
                exist.Name = entity.Name;
                exist.Level = entity.Level;
                exist.CreatedBy(user);
                UnitOfWork.SourceRepository.Update(exist);
                await UnitOfWork.CommitAsync();
                return exist;
            }
        }

        public async Task<SourcePreference> UpdatePreferenceAsync(ClaimsPrincipal user, string key, SourcePreference entity)
        {
            if (!await AuthorizationService.AuthorizeAsync(user, Permission.SourcePreferenceUpdate))
                throw new UnauthorizedAccessException();

            var exist = await UnitOfWork.SourcePreferenceRepository.FindAsync(new object[] {user.GetId(), key});
            if (exist != null)
            {
                exist.IsMastered = entity.IsMastered;
                exist.UpdatedBy(user);
                await UnitOfWork.CommitAsync();
                return exist;
            }
            else
            {
                entity.UserId = user.GetId();
                entity.SourceKey = key;
                entity.CreatedBy(user);
                await UnitOfWork.SourcePreferenceRepository.AddAsync(entity);
                await UnitOfWork.CommitAsync();
                return entity;
            }
        }

        public async Task DeleteAllAsync(ClaimsPrincipal user)
        {
            if (!await AuthorizationService.AuthorizeAsync(user, Permission.SourceManage))
                throw new UnauthorizedAccessException();

            foreach (var entity in UnitOfWork.SourceRepository.Table)
                UnitOfWork.SourceRepository.Delete(entity);
            await UnitOfWork.CommitAsync();
        }

        public async Task ImportAsync(ClaimsPrincipal user, List<Source> sources)
        {
            if (!await AuthorizationService.AuthorizeAsync(user, Permission.SourceCreate))
                throw new UnauthorizedAccessException();

            sources.ForEach(o =>
            {
                o.Key = o.Name.Replace(" ", "-").ToLower();
                o.SourceInstances.First().Order = 0;
            });

            var sourceInstances = sources.Where(o => o.Name.Contains("#")).ToList();
            foreach (var item in sourceInstances)
            {
                var instanceName = item.Name.Split('#')[0]?.Trim();
                var source = sources.First(o => instanceName == o.Name);
                var sourceInstance = new SourceInstance
                {
                    Order = source.SourceInstances.Count,
                    BuildTime = item.SourceInstances.First().BuildTime,
                    Cost = item.SourceInstances.First().Cost
                };
                source.SourceInstances.Add(sourceInstance);
                sources.Remove(item);
            }
            var orderedSources = sources.OrderBy(o => o.Level).ToList();
            foreach (var source in orderedSources)
                source.CreatedBy(user);

            var tasks = orderedSources.Select(o => UnitOfWork.SourceRepository.AddAsync(o));
            await Task.WhenAll(tasks);
            await UnitOfWork.CommitAsync();
        }

        public async Task<List<Source>> ExportAsync(ClaimsPrincipal user, CancellationToken token)
        {
            if (!await AuthorizationService.AuthorizeAsync(user, Permission.SourceManage))
                throw new UnauthorizedAccessException();

            return await UnitOfWork.SourceRepository.TableNoTracking
                .Include(o => o.SourceInstances)
                .OrderBy(o => o.Level)
                .ToListAsync(token);
        }
    }
}