﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using HDH.Core.Users;

namespace HDH.Service.Users
{
    public interface IRoleService
    {
        Task<Role> CreateAsync(ClaimsPrincipal user, Role entity);
        Task<IEnumerable<Role>> GetAllAsync(ClaimsPrincipal user, CancellationToken token = default(CancellationToken));
        Task<Role> GetAsync(ClaimsPrincipal user, string name, CancellationToken token = default(CancellationToken));
        Task<Role> UpdateAsync(ClaimsPrincipal user, Role entity);
        Task DeleteAsync(ClaimsPrincipal user, Role entity);
    }
}