﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using HDH.Core.Users;

namespace HDH.Service.Users
{
    public interface IUserService
    {
        Task<IEnumerable<User>> GetAllAsync(ClaimsPrincipal user, CancellationToken token = default(CancellationToken));
        Task<User> GetAsync(string id, CancellationToken token = default(CancellationToken));
        Task<User> GetAsync(ClaimsPrincipal user, string id, CancellationToken token = default(CancellationToken));
        Task<User> ValidateUsernamePasswordAsync(string email, string password, CancellationToken token = default(CancellationToken));
        Task<User> CreateAsync(User entity, string password = null);
        Task<User> CreateAsync(ClaimsPrincipal user, User entity, string password = null);
        Task<User> UpdateAsync(ClaimsPrincipal user, User entity);
        Task UpdateLastLogin(User entity);
        Task ChangePasswordAsync(ClaimsPrincipal user, User entity, string currentPassword, string newPassword);
        Task<string> GeneratePasswordResetTokenAsync(string email);
        Task ResetPasswordAsync(string email, string token, string newPassword);
        Task DeleteAsync(ClaimsPrincipal user, User entity);
    }
}