﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using HDH.Common.Constants;
using HDH.Common.Exceptions;
using HDH.Common.Extensions;
using HDH.Core.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace HDH.Service.Users
{
    public class RoleService : IRoleService
    {
        private readonly IAuthorizationService _authorizationService;
        private readonly RoleManager<Role> _roleManager;

        public RoleService(IAuthorizationService authorizationService, RoleManager<Role> roleManager)
        {
            _authorizationService = authorizationService;
            _roleManager = roleManager;
        }

        public async Task<IEnumerable<Role>> GetAllAsync(ClaimsPrincipal user, CancellationToken token)
        {
            if (!await _authorizationService.AuthorizeAsync(user, Permission.RoleManage))
                throw new UnauthorizedAccessException();
            
            var entities = await _roleManager.Roles.ToListAsync(token);
            foreach (var entity in entities)
                entity.Permissions = (await _roleManager.GetClaimsAsync(entity)).Where(o => o.Type == CustomClaimTypes.Permission).Select(o => o.Value).ToList();
            return entities;
        }

        public async Task<Role> GetAsync(ClaimsPrincipal user, string name, CancellationToken token)
        {
            if (name == null) throw new ArgumentNullException(nameof(name));
            var entity = await _roleManager.FindByNameAsync(name);
            if (entity != null)
                entity.Permissions = (await _roleManager.GetClaimsAsync(entity)).Where(o => o.Type == CustomClaimTypes.Permission).Select(o => o.Value).ToList();

            if (!await _authorizationService.AuthorizeAsync(user, entity, Permission.RoleRead))
                throw new UnauthorizedAccessException();
            return entity;
        }

        public async Task<Role> CreateAsync(ClaimsPrincipal user, Role entity)
        {
            if (!await _authorizationService.AuthorizeAsync(user, Permission.RoleCreate))
                throw new UnauthorizedAccessException();

            foreach (var permission in entity.Permissions)
                if (!Enum.TryParse<Permission>(permission, out _))
                    throw new ModelStateException(nameof(entity.Permissions), $"Permission {permission} an not a valid permission");

            if (await _roleManager.RoleExistsAsync(entity.Name))
                throw new ModelStateException(nameof(Role), $"Role {entity.Name} is already taken");

            entity.CreatedBy(user);
            await _roleManager.CreateAsync(entity).Handled();

            foreach (var permission in entity.Permissions)
                await _roleManager.AddClaimAsync(entity, new Claim(CustomClaimTypes.Permission, permission)).Handled();
            return entity;
        }

        public async Task<Role> UpdateAsync(ClaimsPrincipal user, Role entity)
        {
            if (!await _authorizationService.AuthorizeAsync(user, entity, Permission.RoleManage))
                throw new UnauthorizedAccessException();
            
            foreach (var permission in entity.Permissions)
                if (!Enum.TryParse<Permission>(permission, out _))
                    throw new ModelStateException(nameof(entity.Permissions), $"Permission {permission} an not a valid permission");
            
            entity.UpdatedBy(user);
            await _roleManager.UpdateAsync(entity).Handled();

            var currentPermissions = (await _roleManager.GetClaimsAsync(entity)).Where(o => o.Type == CustomClaimTypes.Permission).Select(o => o.Value).ToList();
            foreach (var permission in entity.Permissions.Except(currentPermissions))
                await _roleManager.AddClaimAsync(entity, new Claim(CustomClaimTypes.Permission, permission)).Handled();
            foreach (var permission in currentPermissions.Except(entity.Permissions))
                await _roleManager.RemoveClaimAsync(entity, new Claim(CustomClaimTypes.Permission, permission)).Handled();
            return entity;
        }

        public async Task DeleteAsync(ClaimsPrincipal user, Role entity)
        {
            if (!await _authorizationService.AuthorizeAsync(user, entity, Permission.RoleDelete))
                throw new UnauthorizedAccessException();

            await _roleManager.DeleteAsync(entity).Handled();
        }
    }
}
