﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using HDH.Common.Constants;
using HDH.Common.Exceptions;
using HDH.Common.Extensions;
using HDH.Core.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace HDH.Service.Users
{
    public class UserService : IUserService
    {
        private readonly IAuthorizationService _authorizationService;
        private readonly IPasswordValidator<User> _passwordValidator;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> _roleManager;

        public UserService(IAuthorizationService authorizationService, IPasswordValidator<User> passwordValidator, UserManager<User> userManager, RoleManager<Role> roleManager)
        {
            _authorizationService = authorizationService;
            _passwordValidator = passwordValidator;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public async Task<IEnumerable<User>> GetAllAsync(ClaimsPrincipal user, CancellationToken token)
        {
            if (!await _authorizationService.AuthorizeAsync(user, Permission.UserManage))
                throw new UnauthorizedAccessException();

            var entities = await _userManager.Users.ToListAsync(token);
            foreach (var entity in entities)
                entity.Roles = await _userManager.GetRolesAsync(entity);
            return entities;
        }

        public async Task<User> GetAsync(string id, CancellationToken token)
        {
            if (id == null) throw new ArgumentNullException(nameof(id));
            var entity = await _userManager.FindByIdAsync(id);
            if (entity == null)
                return null;

            entity.Roles = await _userManager.GetRolesAsync(entity);
            return entity;
        }

        public async Task<User> GetAsync(ClaimsPrincipal user, string id, CancellationToken token)
        {
            if (id == null) throw new ArgumentNullException(nameof(id));
            var entity = await _userManager.FindByIdAsync(id);
            if (entity == null)
                return null;

            entity.Roles = await _userManager.GetRolesAsync(entity);
            if (!await _authorizationService.AuthorizeAsync(user, entity, Permission.UserRead))
                throw new UnauthorizedAccessException();
            return entity;
        }
        
        public async Task<User> ValidateUsernamePasswordAsync(string email, string password, CancellationToken token)
        {
            if (email == null) throw new ArgumentNullException(nameof(email));
            var entity = await _userManager.FindByEmailAsync(email);
            if (entity == null || !await _userManager.CheckPasswordAsync(entity, password))
                throw new ModelStateException(nameof(password), "Invalid username or password");
            return entity;
        }

        public Task<User> CreateAsync(User entity, string password = null)
        {
            entity.CreatedBy(null);
            return InternalCreateAsync(entity, password);
        }

        public async Task<User> CreateAsync(ClaimsPrincipal user, User entity, string password = null)
        {
            if (!await _authorizationService.AuthorizeAsync(user, Permission.UserCreate))
                throw new UnauthorizedAccessException();

            entity.CreatedBy(user);
            return await InternalCreateAsync(entity, password);
        }

        public async Task<User> UpdateAsync(ClaimsPrincipal user, User entity)
        {
            if (!await _authorizationService.AuthorizeAsync(user, entity, Permission.UserUpdate))
                throw new UnauthorizedAccessException();

            foreach (var newRole in entity.Roles)
                if (!await _roleManager.RoleExistsAsync(newRole))
                    throw new ModelStateException(nameof(entity.Roles), $"Role {newRole} is not a valid role");

            entity.UpdatedBy(user);
            await _userManager.UpdateAsync(entity);
            
            var currentRoles = await _userManager.GetRolesAsync(entity);
            await _userManager.RemoveFromRolesAsync(entity, currentRoles.Except(entity.Roles)).Handled();
            await _userManager.AddToRolesAsync(entity, entity.Roles.Except(currentRoles)).Handled();
            return entity;
        }

        public async Task UpdateLastLogin(User entity)
        {
            entity.LastSignedInAt = DateTimeOffset.UtcNow;
            await _userManager.UpdateAsync(entity);
        }

        public async Task ChangePasswordAsync(ClaimsPrincipal user, User entity, string currentPassword, string newPassword)
        {
            if (!await _authorizationService.AuthorizeAsync(user, entity, Permission.UserUpdate))
                throw new UnauthorizedAccessException();

            entity.UpdatedBy(user);
            await _passwordValidator.ValidateAsync(_userManager, entity, newPassword).Handled();
            await _userManager.ChangePasswordAsync(entity, currentPassword, newPassword).Handled();
        }

        public async Task<string> GeneratePasswordResetTokenAsync(string email)
        {
            if (email == null) throw new ArgumentNullException(nameof(email));
            var entity = await _userManager.FindByEmailAsync(email);
            if (entity == null)
                throw new ResourceNotFoundException(nameof(User), email);

            return await _userManager.GeneratePasswordResetTokenAsync(entity);
        }

        public async Task ResetPasswordAsync(string email, string token, string newPassword)
        {
            if (email == null) throw new ArgumentNullException(nameof(email));
            var entity = await _userManager.FindByEmailAsync(email);
            if (entity == null)
                throw new ResourceNotFoundException(nameof(User), email);
            
            await _passwordValidator.ValidateAsync(_userManager, entity, newPassword).Handled();
            await _userManager.ResetPasswordAsync(entity, token, newPassword).Handled();
        }

        public async Task DeleteAsync(ClaimsPrincipal user, User entity)
        {
            if (!await _authorizationService.AuthorizeAsync(user, entity, Permission.UserDelete))
                throw new UnauthorizedAccessException();

            await _userManager.DeleteAsync(entity).Handled();
        }

        private async Task<User> InternalCreateAsync(User entity, string password = null)
        {
            foreach (var role in entity.Roles)
                if (!await _roleManager.RoleExistsAsync(role))
                    throw new ModelStateException(nameof(entity.Roles), $"Role {role} is not a valid role");

            if (await _userManager.FindByEmailAsync(entity.Email) != null)
                throw new ModelStateException(nameof(entity.Email), $"Email {entity.Email} already taken");

            if (password != null)
            {
                await _passwordValidator.ValidateAsync(_userManager, entity, password).Handled();
                await _userManager.CreateAsync(entity, password).Handled();
            }
            else
            {
                await _userManager.CreateAsync(entity).Handled();
            }
            await _userManager.AddToRolesAsync(entity, entity.Roles).Handled(async () => await _userManager.DeleteAsync(entity));
            return entity;
        }
    }
}