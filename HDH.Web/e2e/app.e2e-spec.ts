import { HDHWebPage } from './app.po';

describe('hdh.web App', () => {
  let page: HDHWebPage;

  beforeEach(() => {
    page = new HDHWebPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
