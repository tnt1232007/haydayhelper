import { Directive, ElementRef, Input, Output, OnInit } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

@Directive({
  selector: '[appDatePicker]',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: DatePickerDirective,
      multi: true
    }]
})
export class DatePickerDirective implements ControlValueAccessor, OnInit {
  @Input() public options: Pickadate.DateOptions;

  private picker: any;
  private innerValue: number;
  private onChange = (_: any) => { };
  private onTouched = () => { };

  constructor(private element: ElementRef) { }

  public ngOnInit() {
    if (!this.options)
      this.options = {};
    this.options.selectYears = this.options.selectYears || 40; // Creates dropdown of 40 years to control years
    this.options.selectMonths = this.options.selectMonths || true; // Creates dropdown to control months
    this.options.firstDay = this.options.firstDay || true; // true is Monday, false is Sunday

    const input = $(this.element.nativeElement).pickadate(this.options);
    this.picker = input.pickadate('picker');

    this.picker.on('set', value => {
      this.value = value ? value.select / 1000 : null;
    });
  }

  get value(): number {
    return this.innerValue;
  }

  set value(value: number) {
    if (value !== this.innerValue) {
        this.innerValue = value;
        this.onChange(value);
    }
  }

  public writeValue(value: number) {
    this.picker.set('select', value * 1000);
  }

  public registerOnChange(fn: (_: any) => {}) {
    this.onChange = fn;
  }

  public registerOnTouched(fn: () => {}) {
    this.onTouched = fn;
  }
}
