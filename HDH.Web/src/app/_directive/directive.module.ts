import { NgModule } from '@angular/core';

import { WindowRef } from 'app/_shared/window-ref';
import { DatePickerDirective } from 'app/_directive/datepicker.directive';
import { ScrollDirective } from 'app/_directive/scroll.directive';
import { HighlightDirective } from 'app/_directive/highlight.directive';
import { MaterialBoxDirective } from 'app/_directive/material-box.directive';
import { TapTargetDirective } from 'app/_directive/tap-target.directive';

@NgModule({
  imports: [],
  providers: [WindowRef],
  declarations: [DatePickerDirective, ScrollDirective, HighlightDirective, MaterialBoxDirective, TapTargetDirective],
  exports: [DatePickerDirective, ScrollDirective, HighlightDirective, MaterialBoxDirective, TapTargetDirective]
})
export class DirectiveModule { }
