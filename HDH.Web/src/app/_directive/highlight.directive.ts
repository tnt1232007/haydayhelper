import { Directive, ElementRef, Renderer, Input, AfterViewInit } from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective implements AfterViewInit {
  @Input() public search: string;
  @Input() public highlightClass: string;

  private EscapeRegex = /[|\\{}()[\]^$+*?.]/g;

  constructor(
    private element: ElementRef,
    private renderer: Renderer) {}

  public ngAfterViewInit() {
    if (!this.highlightClass)
      this.highlightClass = 'font-weight-bold';

    if (!this.search)
      return;

    const input = this.escapeStringRegexp(this.search);
    const output = this.replace(this.element.nativeElement.innerHTML, input);
    this.renderer.setElementProperty(this.element.nativeElement, 'innerHTML', output);
  }

  private replace(str: string, search: string): string {
    const regex = new RegExp('(' + search + ')', 'gi');
    return str.replace(regex, `<span class="${this.highlightClass}">$1</span>`);
  }

  private escapeStringRegexp (str): string {
    return str.replace(this.EscapeRegex, '\\$&');
  }
}
