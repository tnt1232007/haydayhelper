import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appMaterialBox]'
})
export class MaterialBoxDirective {
  constructor(private element: ElementRef) {
    $(this.element.nativeElement).materialbox();
  }
}
