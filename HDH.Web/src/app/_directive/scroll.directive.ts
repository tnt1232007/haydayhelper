import { Directive, Input, ElementRef, HostListener } from '@angular/core';

import { WindowRef } from 'app/_shared/window-ref';

@Directive({
  selector: '[appScroll]'
})
export class ScrollDirective {
  private _className = 'scrolled';

  @Input('scrolledClass') set scrolledClass(className: string) {
    this._className = className || this._className;
  }

  constructor(
    private element: ElementRef,
    private window: WindowRef) { }

  @HostListener('window:scroll', ['$event'])
  private handleScrollEvent(e) {
    const minY = this.window.nativeWindow.outerWidth <= 600 ? 425 : 225;
    if (this.window.nativeWindow.pageYOffset > minY)
      this.element.nativeElement.classList.add(this._className);
    else
      this.element.nativeElement.classList.remove(this._className);
  }
}
