import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appTapTarget]'
})
export class TapTargetDirective {
  constructor(private element: ElementRef) { }

  public open() {
    (<any>$(this.element.nativeElement)).tapTarget('open');
  }

  public close() {
    (<any>$(this.element.nativeElement)).tapTarget('close');
  }
}
