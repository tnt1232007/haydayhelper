import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { AuthService } from 'app/_service/auth/auth.service';
import { FireAuthService } from 'app/_service/auth/fire-auth.service';
import { Constant } from 'app/_shared/constant';

@Injectable()
export class AdminGuard implements CanActivate {
  constructor(
    private router: Router,
    private authService: AuthService,
    private fireAuthService: FireAuthService) { }

  public canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.authService.auth.map(claims => claims != null && claims.role.indexOf(Constant.Administrator) > -1).do(val => {
        if (!val)
          this.router.navigate(['404']);
      });
  }
}
