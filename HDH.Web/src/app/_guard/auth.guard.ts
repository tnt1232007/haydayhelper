import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/merge';

import { AuthService } from 'app/_service/auth/auth.service';
import { FireAuthService } from 'app/_service/auth/fire-auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private authService: AuthService,
    private fireAuthService: FireAuthService) { }

  public canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return Observable.merge(
      this.fireAuthService.auth().map(afUser => afUser != null),
      this.authService.auth.map(claims => claims != null)
    ).do(val => {
      if (!val)
        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
    });
  }
}
