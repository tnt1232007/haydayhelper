import { Injectable, Injector } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { CacheService } from 'app/_service/cache.service';

@Injectable()
export class CacheInterceptor implements HttpInterceptor {
  constructor(private cache: CacheService) { }

  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    switch (req.method) {
      case 'GET':
        break;
      case 'POST':
      case 'PUT':
      case 'DELETE':
        this.cache.clear();
        return next.handle(req);
      default:
        return next.handle(req);
    }

    if (req.headers.has('Skip-Cache')) {
      const headers = req.headers.delete('Skip-Cache');
      const directReq = req.clone({ headers });
      return next.handle(directReq);
    }

    const cachedRes = this.cache.get(req.urlWithParams);
    if (cachedRes)
      return Observable.of(cachedRes);

    return next.handle(req).do(res => {
      if (res instanceof HttpResponse) {
        this.cache.set(req.urlWithParams, res);
      }
    });
  }
}
