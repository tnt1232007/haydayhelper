import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/catch';

import { environment } from 'environments/environment';
import { CommonService } from 'app/_service/common.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(
    private router: Router,
    private commonService: CommonService) { }

  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const started = Date.now();
    return next
      .handle(req)
      .retry(1)
      .do((res: HttpResponse<any>) => {
        if (res instanceof HttpResponse && !environment.production) {
          const elapsed = Date.now() - started;
          console.log(`[${req.method}] ${req.urlWithParams} (${elapsed} ms)`);
        }
      }).catch((err: HttpErrorResponse) => {
        switch (err.status) {
          case 0:
            this.commonService.showToast('Server is Offline', 'error');
            break;
          case 404:
            this.router.navigate(['/404'], { skipLocationChange: true });
            break;
          default:
            this.commonService.showToast(err.error.userMessage, 'error');
            break;
        }
        return Observable.throw(err);
      });
  }
}
