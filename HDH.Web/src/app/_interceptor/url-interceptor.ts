import { Injectable, Injector } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { CommonService } from 'app/_service/common.service';

@Injectable()
export class UrlInterceptor implements HttpInterceptor {
  constructor(private commonService: CommonService) { }

  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const url = req.url;
    if (url.indexOf('http://') === -1 && url.indexOf('https://') === -1)
      req = req.clone({
        url: this.commonService.getApi() + url
      });
    return next.handle(req);
  }
}
