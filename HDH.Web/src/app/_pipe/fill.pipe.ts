import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'fill'
})
export class FillPipe implements PipeTransform {
  public transform(value) {
    if (!value)
      return [];
    value = Math.floor(value);
    return Array.from(Array(value).keys());
  }
}
