import { NgModule } from '@angular/core';
import { FillPipe } from './fill.pipe';

@NgModule({
  imports: [],
  declarations: [FillPipe],
  exports: [FillPipe]
})
export class PipeModule { }
