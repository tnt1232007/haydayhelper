import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import { JwtHelper } from 'angular2-jwt';

import { UserToken } from 'app/_shared/user-token';

@Injectable()
export class AuthService {
  private readonly STORE_KEY = 'haydayhelper:authToken';
  private readonly AUTH_API: string = 'auth';

  public auth = new ReplaySubject<any>(1);
  private uid: any;

  constructor(
    private http: HttpClient,
    private jwtHelper: JwtHelper) { }

  public checkAuthentication(uid: string): boolean {
    if (uid && this.isLoggedIn(uid)) {
      this.setToken(this.getToken(uid));
      return true;
    } else {
      this.setToken(null);
      return false;
    }
  }

  public login(email: string, password: string) {
    return this.http.post<UserToken>(`${this.AUTH_API}/login`, { email: email, password: password }).map(res => {
      this.setToken(res.accessToken);
    });
  }

  public loginWithProvider(user: any, providerToken: any): Observable<any> {
    const options = { headers: new HttpHeaders().set('Authorization', `Bearer ${providerToken}`) };
    return this.http.post<UserToken>(`${this.AUTH_API}/loginWithProvider`, user, options).map(res => {
      this.setToken(res.accessToken);
    });
  }

  public logout() {
    this.setToken(null);
  }

  public isLoggedIn(uid?: string): boolean {
    const token = this.getToken(uid);
    return token && !this.jwtHelper.isTokenExpired(token);
  }

  public getToken(uid?: string): string {
    uid = uid || this.uid;
    if (!uid)
      return null;
    return localStorage.getItem(`${this.STORE_KEY}:${uid}`);
  }

  private setToken(token: string) {
    if (token) {
      const claims = this.jwtHelper.decodeToken(token);
      this.uid = claims.sub;
      localStorage.setItem(`${this.STORE_KEY}:${this.uid}`, token);
      this.auth.next(claims);
    } else {
      localStorage.removeItem(`${this.STORE_KEY}:${this.uid}`);
      this.uid = null;
      this.auth.next(null);
    }
  }
}
