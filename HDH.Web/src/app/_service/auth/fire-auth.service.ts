import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

import { environment } from 'environments/environment';

@Injectable()
export class FireAuthService {
  constructor(
    private afAuth: AngularFireAuth) { }

  public login(email: string, password: string): Observable<string> {
    const subject = new Subject<string>();
    this.afAuth.auth.signInWithEmailAndPassword(email, password)
      .then(afUser => afUser.emailVerified ? subject.next(afUser) : subject.error('Your account has not been verified'))
      .catch((error: any) => {
        let errorMessage: string;
        switch (error.code) {
          case 'auth/invalid-email':
            errorMessage = 'Invalid email address';
            break;
          case 'auth/user-disabled':
            errorMessage = 'Your account has been disabled';
            break;
          case 'auth/user-not-found':
            errorMessage = 'Wrong email or password';
            break;
          case 'auth/wrong-password':
            errorMessage = 'Wrong email or password';
            break;
          default:
            errorMessage = environment.production ? 'Internal server error' : error.message;
            break;
        }
        subject.error(errorMessage);
      });
    return subject.asObservable();
  }

  public logout() {
    this.afAuth.auth.signOut();
  }

  public register(email: string, password: string): Observable<firebase.User> {
    const subject = new Subject<firebase.User>();
    this.afAuth.auth.createUserWithEmailAndPassword(email, password)
      .then(afUser => subject.next(afUser))
      .catch((error: any) => {
        let errorMessage: string;
        switch (error.code) {
          case 'auth/invalid-email':
            errorMessage = 'Invalid email address';
            break;
          case 'auth/email-already-in-use':
            errorMessage = 'This account is existed';
            break;
          case 'auth/weak-password':
            errorMessage = error.message;
            break;
          case 'auth/operation-not-allowed':
          default:
            errorMessage = environment.production ? 'Internal server error' : error.message;
            break;
        }
        subject.error(errorMessage);
      });
    return subject.asObservable();
  }

  public useProvider(provider: string): Observable<firebase.User> {
    const subject = new Subject<firebase.User>();
    let afProvider: firebase.auth.AuthProvider;
    switch (provider) {
      case 'google':
        const ggProvider = new firebase.auth.GoogleAuthProvider();
        ggProvider.addScope('profile');
        ggProvider.addScope('email');
        afProvider = ggProvider;
        break;
      case 'facebook':
        const fbProvider = new firebase.auth.FacebookAuthProvider();
        fbProvider.addScope('public_profile');
        fbProvider.addScope('email');
        afProvider = fbProvider;
        break;
      case 'twitter':
        const twProvider = new firebase.auth.TwitterAuthProvider();
        afProvider = twProvider;
        break;
      case 'github':
        const gitProvider = new firebase.auth.GithubAuthProvider();
        gitProvider.addScope('profile');
        gitProvider.addScope('email');
        afProvider = gitProvider;
        break;
      default:
        subject.error('Invalid provider');
        break;
    }

    this.afAuth.auth.signInWithPopup(afProvider).then(result => {
      if (result && result.user)
        subject.next(result.user);
      else
        subject.error('Provider error');
    }).catch((error: any) => {
      let errorMessage: string;
      switch (error.code) {
        case 'auth/account-exists-with-different-credential':
          errorMessage = error.message;
          break;
        case 'auth/cancelled-popup-request':
          errorMessage = error.message;
          break;
        case 'auth/popup-blocked':
          errorMessage = error.message;
          break;
        case 'auth/popup-closed-by-user':
          errorMessage = error.message;
          break;
        case 'auth/operation-not-allowed':
        case 'auth/operation-not-supported-in-this-environment':
        case 'auth/unauthorized-domain':
        case 'auth/auth-domain-config-required':
        default:
          errorMessage = environment.production ? 'Internal server error' : error.message;
          break;
      }
      subject.error(errorMessage);
    });
    return subject.asObservable();
  }

  public sendConfirmEmail() {
    this.afAuth.auth.currentUser.sendEmailVerification();
  }

  public sendPasswordResetEmail(email: string): Observable<void> {
    const subject = new Subject<void>();
    this.afAuth.auth.fetchProvidersForEmail(email).then(providers => {
      if (!providers || providers.length === 0 || providers[0] !== 'password') {
        subject.error(`An account already exists with the same email address but different sign-in credentials.
                        Sign in using a provider associated with this email address.`);
        return subject;
      }

      this.afAuth.auth.sendPasswordResetEmail(email)
        .then(v => subject.next())
        .catch((error: any) => {
          let errorMessage: string;
          switch (error.code) {
            case 'auth/invalid-email':
              errorMessage = 'Invalid email address';
              break;
            case 'auth/user-not-found':
              errorMessage = 'This account is not existed';
              break;
            default:
              errorMessage = environment.production ? 'Internal server error' : error.message;
              break;
          }
          subject.error(errorMessage);
        });
    }).catch((error: any) => {
      let errorMessage: string;
      switch (error.code) {
        case 'auth/invalid-email':
          errorMessage = 'Invalid email address';
          break;
        default:
          errorMessage = environment.production ? 'Internal server error' : error.message;
          break;
      }
      subject.error(errorMessage);
    });
    return subject.asObservable();
  }

  public isLoggedIn(): boolean {
    return this.afAuth.auth.currentUser ? true : false;
  }

  public getToken(): Observable<string> {
    const subject = new Subject<string>();
    const afUser = this.afAuth.auth.currentUser;
    if (afUser != null)
      afUser.getIdToken().then(token => subject.next(token));
    else
      subject.next('');
    return subject.asObservable();
  }

  public auth(): Observable<firebase.User> {
    return this.afAuth.authState.map(afUser => afUser && afUser.emailVerified ? afUser : null);
  }
}
