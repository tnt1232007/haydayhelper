import { CacheContent } from 'app/_shared/cache-content';

export class CacheService {
  private cache: Map<string, CacheContent> = new Map<string, CacheContent>();
  private readonly DEFAULT_MAX_AGE: number = 300000; // milliseconds = 5 minutes

  public get(key: string): any {
    return this.hasValidCachedValue(key) ? this.cache.get(key).value : null;
  }

  public set(key: string, value: any) {
    this.cache.set(key, { value: value, expiry: Date.now() + this.DEFAULT_MAX_AGE });
  }

  public clear() {
    this.cache.clear();
  }

  private hasValidCachedValue(key: string): boolean {
    if (this.cache.has(key)) {
      if (this.cache.get(key).expiry < Date.now()) {
        this.cache.delete(key);
        return false;
      } else {
        return true;
      }
    } else {
      return false;
    }
  }
}
