import { Injectable, Type, ComponentRef } from '@angular/core';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { MzToastService, MzModalService, MzBaseModal } from 'ng2-materialize';

import { environment } from 'environments/environment';
import { DialogComponent } from 'app/nested-components/dialog/dialog.component';

@Injectable()
export class CommonService {
  private static Toasts: string[] = [];
  private title: string;

  constructor(
    private router: Router,
    private titleService: Title,
    private toastService: MzToastService,
    private modalService: MzModalService) { }

  public getApi(): string {
    return environment.api + '/api/';
  }

  public getTitle(): string {
    return this.title;
  }

  public setTitle(title: string) {
    this.title = title;
    this.titleService.setTitle(this.title ? `${environment.title} - ${this.title}` : environment.short_title);
  }

  public showToast(message: string, severity: string = '') { // warn, error
    let toastClass: string;
    let timeout: number;
    switch (severity) {
      case 'error':
        toastClass = 'red darken-2';
        timeout = 60000;
        if (CommonService.Toasts.indexOf(message) > -1)
          return;
        break;
      case 'warn':
        toastClass = 'amber darken-2';
        timeout = 12000;
        if (CommonService.Toasts.indexOf(message) > -1)
          return;
        break;
      default:
        toastClass = '';
        timeout = 7000;
        break;
    }
    CommonService.Toasts.push(message);
    this.toastService.show(message, timeout, toastClass, () => {
      CommonService.Toasts.splice(CommonService.Toasts.indexOf(message), 1);
    });
  }

  public showModal(
    header: string,
    content: string,
    icon: string = '',
    footerType: string = 'OK',
    callback: (result: boolean) => void = null) {
    this.modalService.open(DialogComponent, { header: header, content: content, icon: icon, footerType: footerType, callback: callback });
  }

  public showUsageChart(type: Type<MzBaseModal>, showTapTarget = false): ComponentRef<MzBaseModal> {
    return this.modalService.open(type, { showTapTarget: showTapTarget });
  }

  public showNetworkGraph(type: Type<MzBaseModal>, key: string, showTapTarget = false): ComponentRef<MzBaseModal> {
    return this.modalService.open(type, { productKey: key, showTapTarget: showTapTarget });
  }
}
