import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ChartService {
  private readonly CHART_API: string = 'chart';

  constructor(private http: HttpClient) { }

  public calcProductUsage(): Observable<any[]> {
    return this.http.get<any[]>(`${this.CHART_API}/calcProductUsage`);
  }

  public calcProductRelationship(): Observable<any> {
    return this.http.get(`${this.CHART_API}/calcProductRelationship`);
  }
}
