import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { Constant } from 'app/_shared/constant';
import { AppOption } from 'app/_shared/app-option';

@Injectable()
export class GlobalService {
  private readonly GLOBAL_API: string = 'global';

  constructor(private http: HttpClient) { }

  public getCountryOptions(keyword?: string): Observable<AppOption[]> {
    return this.http.get<AppOption[]>(`${this.GLOBAL_API}/CountryOptions`, Constant.toHttpParams({
      keyword: keyword
    }));
  }

  public getProductOptions(): Observable<AppOption[]> {
    return this.http.get<AppOption[]>(`${this.GLOBAL_API}/ProductOptions`)
      .do(o => o.forEach(m => {
        m.icon = `assets/products/${m.key}.png`;
      }));
  }

  public getSourceOptions(): Observable<AppOption[]> {
    return this.http.get<AppOption[]>(`${this.GLOBAL_API}/SourceOptions`)
    .do(o => o.forEach(m => {
      m.icon = `assets/sources/${m.key}.png`;
    }));
  }
}
