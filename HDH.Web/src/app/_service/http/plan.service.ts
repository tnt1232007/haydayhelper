import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { Constant } from 'app/_shared/constant';

@Injectable()
export class PlanService {
  private readonly PLAN_API: string = 'plans';

  constructor(private http: HttpClient) { }

  public getPlans(skipCache: boolean = false): Observable<any> {
    let headers = new HttpHeaders();
    if (skipCache)
      headers = headers.set('Skip-Cache', 'true');
    return this.http.get(this.PLAN_API, { headers: headers });
  }

  public insertPlan(plan: any): Observable<any> {
    return this.http.post(`${this.PLAN_API}`, plan);
  }

  public updatePlan(plan: any): Observable<any> {
    return this.http.put(`${this.PLAN_API}/${plan.id}`, plan);
  }

  public deletePlan(plan: any): Observable<any> {
    return this.http.delete(`${this.PLAN_API}/${plan.id}`);
  }

  public calculatePlan(plan: any): Observable<any> {
    return this.http.post(`${this.PLAN_API}/Calculate`, plan);
  }
}
