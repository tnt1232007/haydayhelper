import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { Constant } from 'app/_shared/constant';

@Injectable()
export class ProductService {
  private readonly SOURCE_API: string = 'sources';
  private readonly PRODUCT_API: string = 'products';

  constructor(private http: HttpClient) { }

  public getProducts(keyword: string, sort: string, page: number, pageSize: number): Observable<any> {
    return this.http.get(this.PRODUCT_API, Constant.toHttpParams({
      keyword: this.translateKeywords(keyword),
      sort: this.translateKeywords(sort),
      page: page,
      pageSize: pageSize
    }));
  }

  public getProductsBySource(sourceKey: string, sort: string = ''): Observable<any> {
    return this.http.get(`${this.SOURCE_API}/${sourceKey}/${this.PRODUCT_API}`, Constant.toHttpParams({
      sort: this.translateKeywords(sort)
    }));
  }

  public getProduct(key: string): Observable<any> {
    return this.http.get(`${this.PRODUCT_API}/${key}`);
  }

  public updateProduct(product: any): Observable<any> {
    return this.http.post(`${this.PRODUCT_API}/${product.key}`, product);
  }

  public updateProductPreference(product: any): Observable<any> {
    return this.http.post(`${this.PRODUCT_API}/preference/${product.key}`, product);
  }

  private translateKeywords(expression: string): string {
    expression = expression || '';
    expression = expression.toLowerCase().trim();
    expression = expression.replace('price', 'MaxPrice');
    expression = expression.replace('time', 'ProductionTime');
    expression = expression.replace('rating', 'ProductPreferences.FirstOrDefault().Rating');
    expression = expression.replace('stock', 'ProductPreferences.FirstOrDefault().Stock');
    expression = expression.replace('source', 'SourceKey');
    expression = expression.replace('mastered', 'Source.SourcePreferences.FirstOrDefault().IsMastered');
    return expression;
  }
}
