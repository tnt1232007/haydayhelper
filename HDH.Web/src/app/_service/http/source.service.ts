import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { Constant } from 'app/_shared/constant';

@Injectable()
export class SourceService {
  private readonly SOURCE_API: string = 'sources';

  constructor(private http: HttpClient) { }

  public getSources(keyword: string, sort: string, page: number, pageSize: number): Observable<any> {
    return this.http.get(this.SOURCE_API, Constant.toHttpParams({
      keyword: this.translateKeywords(keyword),
      sort: this.translateKeywords(sort),
      page: page,
      pageSize: pageSize
    }));
  }

  public getSource(key: string): Observable<any> {
    return this.http.get(`${this.SOURCE_API}/${key}`);
  }

  public updateSource(source: any): Observable<any> {
    return this.http.post(`${this.SOURCE_API}/${source.key}`, source);
  }

  public updateSourcePreference(source: any): Observable<any> {
    return this.http.post(`${this.SOURCE_API}/preference/${source.key}`, source);
  }

  private translateKeywords(expression: string): string {
    expression = expression || '';
    expression = expression.toLowerCase().trim();
    expression = expression.replace('mastered', 'SourcePreferences.FirstOrDefault().IsMastered');
    return expression;
  }
}
