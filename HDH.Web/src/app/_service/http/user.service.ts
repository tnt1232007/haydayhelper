import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import 'rxjs/add/operator/map';

@Injectable()
export class UserService {
  public user = new ReplaySubject<any>(1);
  private readonly USER_API: string = 'users';

  constructor(private http: HttpClient) { }

  public getUser(id: string): Observable<any> {
    return this.http.get(`${this.USER_API}/${id}`).map(user => {
      this.user.next(user);
    });
  }

  public updateUser(user: any): Observable<any> {
    return this.http.put(`${this.USER_API}/${user.id}`, user);
  }
}
