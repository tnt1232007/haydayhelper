import { AppOption } from 'app/_shared/app-option';

export class AppNavigation {
  public all: AppOption[];
  public current: AppOption;
  public previous: AppOption;
  public next: AppOption;
  public url = '../{0}';

  public constructor(init?: Partial<AppNavigation>) {
    Object.assign(this, init);
  }
}
