export class AppOption {
  public key: string;
  public name: string;
  public group: string;
  public icon: string;
  public checked: boolean;

  public constructor(init?: Partial<AppOption>) {
    Object.assign(this, init);
  }
}
