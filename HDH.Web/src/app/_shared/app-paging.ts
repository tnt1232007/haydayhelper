export interface AppPaging {
  filter: string;
  sort: string;
  page: number;
  pageSize: number;
  total: number;
  totalPages: number;
  startIndex: number;
  endIndex: number;
}
