export class AppParams {
  public q: string;
  public sort: string;
  public page: number;
  public pageSize: number;

  public constructor(init?: Partial<AppParams>) {
    Object.assign(this, init);
  }
}
