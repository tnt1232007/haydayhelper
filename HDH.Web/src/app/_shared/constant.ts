import { HttpParams } from '@angular/common/http';

import { AppParams } from 'app/_shared/app-params';

export class Constant {
  public static Administrator = 'Administrator';
  public static Staff = 'Staff';
  public static User = 'User';
  public static StartPage = 1;
  public static DefaultPageSize = 10;

  public static toHttpParams(params: any): { params: HttpParams } {
    return { params: Object.getOwnPropertyNames(params).reduce((p, key) => params[key] ? p.set(key, params[key]) : p, new HttpParams()) };
  }

  public static retrieveParams(params: { [key: string]: any }) {
    return new AppParams({
      q: params['q'] || '',
      sort: params['sort'] || '',
      page: params['page'] || Constant.StartPage,
      pageSize: params['pageSize'] || Constant.DefaultPageSize
    });
  }

  public static setupParams(p: AppParams, key: string = null, value: any = null): AppParams {
    const params = { ...p };
    if (key)
      params[key] = value;
    if (!params.q)
      delete params.q;
    if (!params.sort || params.sort === '')
      delete params.sort;
    if (!params.page || params.page === Constant.StartPage)
      delete params.page;
    if (!params.pageSize || params.pageSize === Constant.DefaultPageSize)
      delete params.pageSize;
    return params;
  }
}
