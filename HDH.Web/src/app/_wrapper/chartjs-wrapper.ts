import 'chart.piecelabel.js';

export class ChartjsWrapper {
  private static CurrColorIndex: number;

  private static Colors(trans: number): string[] {
    return [
      'rgba(244, 67, 54, ' + trans + ')',
      'rgba(233, 30, 99, ' + trans + ')',
      'rgba(156, 39, 176, ' + trans + ')',
      'rgba(103, 58, 183, ' + trans + ')',
      'rgba(63, 81, 181, ' + trans + ')',
      'rgba(33, 150, 243, ' + trans + ')',
      'rgba(3, 169, 244, ' + trans + ')',
      'rgba(0, 188, 212, ' + trans + ')',
      'rgba(0, 150, 136, ' + trans + ')',
      'rgba(76, 175, 80, ' + trans + ')',
      'rgba(139, 195, 74, ' + trans + ')',
      'rgba(205, 220, 57, ' + trans + ')',
      'rgba(255, 235, 59, ' + trans + ')',
      'rgba(255, 193, 7, ' + trans + ')',
      'rgba(255, 152, 0, ' + trans + ')',
      'rgba(255, 87, 34, ' + trans + ')',
      'rgba(121, 85, 72, ' + trans + ')',
      'rgba(158, 158, 158, ' + trans + ')',
      'rgba(96, 125, 139, ' + trans + ')'
    ];
  }

  public chartLabels: string[] = [];
  public chartData: any[] = [];
  public chartColors: any[] = [];
  public chartOptions: any;
  public selectedLabel: string;

  constructor(title: string = null) {
    this.chartOptions = {
      title: {
        display: title ? true : false,
        text: title,
        fontSize: 32,
        padding: 32
      },
      tooltips: {
        mode: 'index',
        callbacks: {},
        cornerRadius: 0,
        xPadding: 10,
        yPadding: 10,
        backgroundColor: '#323232',
        position: 'nearest',
        titleFontFamily: '-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif',
        titleFontSize: 15,
        titleMarginBottom: 12
      },
      scales: {
        xAxes: [],
        yAxes: []
      },
      hover: {
        onHover: (event, active) => {
          this.selectedLabel = active.length > 0 ? active[0]._model.label : '';
        }
      }
    };
  }

  public setLabels(labels: string[]) {
    this.chartLabels = labels;
  }

  public setTooltip(labelCallback: (tooltipItem: any, data: any) => string) {
    this.chartOptions.tooltips.callbacks.label = labelCallback;
  }

  public setData(index: number = 0, data: number[]) {
    this.chartData[index].data = data;
  }

  public setScale(index: number = 0, value: number = 0) {
    if (value)
      this.chartOptions.scales.yAxes[index].ticks.suggestedMax = value;
  }

  public setLegend(display: boolean, position: string) {
    this.chartOptions.legend = {
      display: display,
      position: position
    };
  }

  public setColors(index: number = 0, backgroundColor: string = null, borderColor: string = null) {
    let rand: number;
    do {
      rand = Math.floor(Math.random() * (ChartjsWrapper.Colors(0).length - 1));
    } while (rand === ChartjsWrapper.CurrColorIndex);

    if (backgroundColor) {
      this.chartColors[index].backgroundColor = backgroundColor;
    } else {
      this.chartColors[index].backgroundColor = ChartjsWrapper.Colors(0.2)[rand];
      ChartjsWrapper.CurrColorIndex = rand;
    }

    if (borderColor) {
      this.chartColors[index].borderColor = borderColor;
    } else {
      this.chartColors[index].borderColor = ChartjsWrapper.Colors(1.0)[rand];
      ChartjsWrapper.CurrColorIndex = rand;
    }

    switch (this.chartData[index].type) {
      case 'bar':
        break;
      case 'line':
        this.chartColors[index].backgroundColor = this.chartColors[index].borderColor;
        break;
      default:
        this.chartColors = [{ backgroundColor: ChartjsWrapper.Colors(0.6).sort(o => .5 - Math.random()) }];
        break;
    }
  }

  public addYAxes(position: string, yLabel: string): number {
    const axesIndex = this.chartOptions.scales.yAxes.length;

    this.chartOptions.scales.yAxes.push({
      id: 'y-axis-' + axesIndex,
      position: position,
      ticks: {
        beginAtZero: true
      },
      gridLines: {
        display: axesIndex === 0 ? true : false
      },
      scaleLabel: {
        display: yLabel ? true : false,
        labelString: yLabel
      }
    });

    return axesIndex;
  }

  public addDataSet(axesIndex: number, type: string, label: string, labelAfter: string = null) {
    switch (type) {
      case 'bar':
        this.chartData.push({
          data: [],
          type: type,
          borderWidth: 1,
          label: label,
          labelAfter: labelAfter,
          yAxisID: 'y-axis-' + axesIndex
        });
        break;
      case 'line':
        this.chartData.push({
          data: [],
          fill: false,
          type: type,
          label: label,
          labelAfter: labelAfter,
          yAxisID: 'y-axis-' + axesIndex,
          pointRadius: 4,
          pointHoverRadius: 7,
          hitRadius: 24
        });
        break;
      default:
        this.chartData.push({
          data: [],
          label: label
        });
        this.chartOptions.pieceLabel = {
          mode: 'percentage'
        };
        break;
    }

    this.chartColors.push({
      backgroundColor: '',
      borderColor: ''
    });
  }
}
