import 'hammerjs';
import * as vis from 'vis';

export class VisjsWrapper {
  private data: vis.DataSet<any>;
  private network: vis.Network;

  constructor(container: any, type: string) {
    const options = {
      nodes: {
        shape: type,
        borderWidth: 0,
        size: 40,
        shadow: true,
        chosen: {
          node: o => {
            o.size = 50;
            o.borderWidth = 3;
          }
        }
      },
      edges: {
        width: 0.75,
        arrowStrikethrough: false
      },
      physics: {
        barnesHut: {
          avoidOverlap: 0.4
        }
      }
    };
    this.network = new vis.Network(container, { nodes: new vis.DataSet([]), edges: new vis.DataSet([]) }, options);
  }

  public getData(): any[] {
    return this.data.get({ returnType: 'Object' });
  }

  public setData(nodes: any[], edges: any[]) {
    this.data = new vis.DataSet(nodes);
    this.network.setData({ nodes: this.data, edges: new vis.DataSet(edges) });
  }

  public updateData(nodes: any) {
    if (!this.data)
      return;

    if (nodes instanceof Array)
      this.data.update(nodes);
    else {
      const array = [];
      for (const nodeId in nodes) {
        if (nodes.hasOwnProperty(nodeId))
          array.push(nodes[nodeId]);
      }
      this.data.update(array);
    }
  }

  public setOptions(options: vis.Options) {
    this.network.setOptions(options);
  }

  public setEvent(onEvent: vis.NetworkEvents, event: any) {
    this.network.on(onEvent, o => event(o));
  }

  public getNodeAt(x: number, y: number): vis.IdType {
    return this.network.getNodeAt({ x, y });
  }

  public getConnectedNodes(key: vis.IdType): any {
    if (!key)
      return [];
    return this.network.getConnectedNodes(key);
  }

  public setSelectedNode(key: vis.IdType) {
    if (!key)
      return;
    this.network.selectNodes([key]);
  }

  public zoomFit() {
    this.network.fit({
      animation: true
    });
  }

  public zoomFocus(key: string) {
    if (!key)
      return;
    this.setSelectedNode(key);
    this.network.focus(key, {
      scale: 1.5,
      animation: true
    });
  }
}
