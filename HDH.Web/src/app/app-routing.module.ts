import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from 'app/_guard';
import { PageNotFoundComponent } from 'app/components/page-not-found/page-not-found.component';

const routes: Routes = [
  { path: '', loadChildren: 'app/lazy-components/home/home.module#HomeModule' },
  { path: 'login', loadChildren: 'app/lazy-components/login/login.module#LoginModule' },
  { path: 'profile', loadChildren: 'app/lazy-components/profile/profile.module#ProfileModule', canActivate: [AuthGuard] },
  { path: 'product', loadChildren: 'app/lazy-components/products/product.module#ProductModule' },
  { path: 'source', loadChildren: 'app/lazy-components/sources/source.module#SourceModule' },
  { path: 'planning', loadChildren: 'app/lazy-components/planning/planning.module#PlanningModule' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
