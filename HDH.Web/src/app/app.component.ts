import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import 'rxjs/add/operator/takeWhile';

import { environment } from 'environments/environment';
import { AuthService } from 'app/_service/auth/auth.service';
import { FireAuthService } from 'app/_service/auth/fire-auth.service';
import { CommonService } from 'app/_service/common.service';
import { UserService } from 'app/_service/http/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit, OnDestroy {
  private isAlive = true;
  public user: any;

  constructor(
    public router: Router,
    private authService: AuthService,
    private fireAuthService: FireAuthService,
    private commonService: CommonService,
    private userService: UserService) { }

  public ngOnInit() {
    this.fireAuthService.auth().takeWhile(() => this.isAlive).subscribe(afUser => {
      if (afUser) {
        if (this.authService.checkAuthentication(afUser.uid))
          return;

        this.fireAuthService.getToken().takeWhile(() => this.isAlive).subscribe(providerToken => {
          this.authService.loginWithProvider({
            id: afUser.uid,
            email: afUser.email,
            name: afUser.displayName || afUser.email,
            avatar: afUser.photoURL,
            roles: ['User']
          }, providerToken).subscribe();
        });
      } else {
        this.authService.checkAuthentication(null);
        this.user = null;
      }
    });

    this.authService.auth.takeWhile(() => this.isAlive).subscribe(claims => {
      if (claims)
        this.userService.getUser(claims.sub).subscribe();
    });

    this.userService.user.takeWhile(() => this.isAlive).subscribe(user => {
      if (user) {
        this.user = user;
        this.commonService.showToast('Logged in successful');
      }
    });
  }

  public ngOnDestroy() {
    this.isAlive = false;
  }

  public getWebTitle(): string {
    return environment.title;
  }

  public getPageTitle(): string {
    return this.commonService.getTitle();
  }

  public logout() {
    this.commonService.showModal('Logout', 'Are you sure you want to log out?', '', 'YN', result => {
      if (result) {
        this.fireAuthService.logout();
        this.authService.logout();
        this.router.navigate(['/login'], { queryParams: { returnUrl: this.router.url } });
        this.commonService.showToast('Logged out successful');
      }
    });
  }
}
