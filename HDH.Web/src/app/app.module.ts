import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule, Title } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { JwtHelper } from 'angular2-jwt';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { MaterializeModule, MzToastService, MzModalService } from 'ng2-materialize';

import { environment } from 'environments/environment';
import { DirectiveModule } from 'app/_directive/directive.module';
import { NestedModule } from 'app/nested-components/nested.module';
import { AuthGuard, AdminGuard } from 'app/_guard';
import { AuthService, FireAuthService, CacheService, CommonService, GlobalService, UserService, ChartService } from 'app/_service';
import { UrlInterceptor, AuthInterceptor, CacheInterceptor, ErrorInterceptor } from 'app/_interceptor';
import { AppRoutingModule } from 'app/app-routing.module';
import { AppComponent } from 'app/app.component';
import { PageNotFoundComponent } from 'app/components/page-not-found/page-not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    MaterializeModule.forRoot(),
    DirectiveModule,
    NestedModule
  ],
  providers: [
    Title,
    MzToastService,
    MzModalService,
    JwtHelper,
    AuthGuard,
    AdminGuard,
    AuthService,
    FireAuthService,
    CacheService,
    GlobalService,
    ChartService,
    UserService,
    CommonService,
    { provide: HTTP_INTERCEPTORS, useClass: UrlInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: CacheInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
