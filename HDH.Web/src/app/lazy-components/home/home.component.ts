import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { CommonService } from 'app/_service/common.service';
import { UsageChartComponent } from 'app/nested-components/usage-chart/usage-chart.component';
import { NetworkGraphComponent } from 'app/nested-components/network-graph/network-graph.component';

@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public isPulse = [true, true];
  private openDialog: any;

  constructor(
    private commonService: CommonService,
    private router: Router,
    private route: ActivatedRoute) { }

  public ngOnInit() {
    this.commonService.setTitle('Home');

    setTimeout(() => {
      this.route.fragment.subscribe(o => {
        if (this.openDialog) {
          this.openDialog.instance.modal.close();
          this.openDialog = null;
        }

        if (o === 'usageChart') {
          this.openDialog = this.commonService.showUsageChart(UsageChartComponent, this.isPulse[0]);
          this.isPulse[0] = false;
        } else if (o === 'networkGraph') {
          this.openDialog = this.commonService.showNetworkGraph(NetworkGraphComponent, null, this.isPulse[1]);
          this.isPulse[1] = false;
        }

        if (this.openDialog) {
          this.openDialog.instance.modal.onClose.subscribe(m => {
            this.router.navigate([]);
          });
        }
      });
    }, 200);
  }
}
