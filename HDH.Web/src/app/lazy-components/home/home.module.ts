import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterializeModule } from 'ng2-materialize';

import { SourceService } from 'app/_service/http/source.service';
import { HomeRoutingModule } from 'app/lazy-components/home/home-routing.module';
import { HomeComponent } from 'app/lazy-components/home/home.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MaterializeModule.forRoot(),
    HomeRoutingModule
  ],
  declarations: [HomeComponent],
  providers: [SourceService]
})
export class HomeModule { }
