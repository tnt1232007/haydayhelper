import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/takeWhile';

import { environment } from 'environments/environment';
import { AuthService } from 'app/_service/auth/auth.service';
import { FireAuthService } from 'app/_service/auth/fire-auth.service';
import { CommonService } from 'app/_service/common.service';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  public isLoading = false;
  private isAlive = true;

  public user: any = {};
  public providerError: string;
  public siteError: string;
  private returnUrl: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService,
    private fireAuthService: FireAuthService,
    private commonService: CommonService) { }

  public ngOnInit() {
    this.commonService.setTitle('Login');
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';

    this.authService.auth.takeWhile(() => this.isAlive).subscribe(claims => {
      if (claims) {
        this.isLoading = false;
        this.router.navigateByUrl(this.returnUrl);
      }
    });
  }

  public ngOnDestroy() {
    this.isAlive = false;
  }

  public login(email: string, password: string) {
    if (!this.validateForm())
      return;

    this.setEmailError();
    this.isLoading = true;
    this.fireAuthService.login(email, password).subscribe(
      afUser => {},
      error => this.setEmailError(error));
  }

  public register(email: string, password: string) {
    if (!this.validateForm())
      return;

    this.setEmailError();
    this.isLoading = true;
    this.fireAuthService.register(email, password).subscribe(
      afUser => this.confirmEmail(afUser),
      error => this.setEmailError(error));
  }

  public forgotPassword(email: string) {
    if (!this.validateForm(true, false))
      return;

    this.setEmailError();
    this.isLoading = true;
    this.fireAuthService.sendPasswordResetEmail(email).subscribe(v => {
      this.isLoading = false;
      this.commonService.showModal(`Email sent to ${email}`,
        `<h5>To get back into your account, follow the instructions we've sent to your ${email} email address.</h5>
        <br/>
        <em>
          Didn't receive the password reset email?
          Check your spam folder from an from ${environment.no_reply}.
          If you still don't see the email, try again.
        </em>`, 'lock_open');
    }, error => this.setEmailError(error));
  }

  public useProvider(provider) {
    this.setProviderError();
    this.isLoading = true;
    this.fireAuthService.useProvider(provider).subscribe(
      afUser => {
        if (!afUser.emailVerified)
          this.confirmEmail(afUser);
      },
      error => this.setProviderError(error));
  }

  private validateForm(validateEmail = true, validatePassword = true) {
    this.siteError = null;
    if (validatePassword && !this.user.password)
      this.siteError = 'Invalid password';
    if (validateEmail && !this.user.email)
      this.siteError = 'Invalid email address';
    return this.siteError ? false : true;
  }

  private setEmailError(error: string = null) {
    this.isLoading = false;
    this.user.password = '';
    this.siteError = error;
  }

  private setProviderError(error: string = null) {
    this.isLoading = false;
    this.user.password = '';
    this.providerError = error;
  }

  private confirmEmail(afUser: any) {
    this.fireAuthService.sendConfirmEmail();
    this.fireAuthService.logout();
    this.isLoading = false;
    this.commonService.showModal('Verify your email address',
      `<h4>Thank you for using Hay Day Helper!</h4>
      <h5>You are almost done! A verfication message has been sent to ${afUser.email}</h4>
      <br/>
      <em>
        Check your email and follow the link to finish creating your account at HayDayHelper.
        Once your verify your email address, you will be able to update your profile and access all HayDayHelper functionality.
      </em>`, 'check');
  }
}
