import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterializeModule } from 'ng2-materialize';

import { NestedModule } from 'app/nested-components/nested.module';
import { LoginRoutingModule } from 'app/lazy-components/login/login-routing.module';
import { LoginComponent } from 'app/lazy-components/login/login.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MaterializeModule.forRoot(),
    NestedModule,
    LoginRoutingModule
  ],
  declarations: [LoginComponent]
})
export class LoginModule { }
