import { Component, OnInit, OnDestroy, HostListener, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs/Rx';
import { takeWhile } from 'rxjs/operator/takeWhile';
import { delay } from 'rxjs/operator/delay';
import { mergeMap } from 'rxjs/operator/mergeMap';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { MzMediaService } from 'ng2-materialize';
import { DragulaService } from 'ng2-dragula';

import { Constant } from 'app/_shared/constant';
import { AuthService } from 'app/_service/auth/auth.service';
import { CommonService } from 'app/_service/common.service';
import { UserService, GlobalService, PlanService } from 'app/_service';
import { AppOption } from 'app/_shared/app-option';

@Component({
  templateUrl: './planning.component.html',
  styleUrls: ['./planning.component.scss']
})
export class PlanningComponent implements OnInit, OnDestroy {
  private excludeSources: string[] = [
    'special', 'mine', 'maintenance', 'dock',
    'field', 'animal-product', 'feed-mill', 'tree-or-bush'
  ];
  private dragulaContainer = 'products-bag';
  public isLoggedIn = false;
  public isLoading = false;
  public isCalculating = false;
  public isPulse = true;
  private isAlive = true;

  public plans: any[];
  public activePlan: any;
  private level: number;
  private keyword = '';

  public products: any[] = [];
  private allProducts: any[] = [];
  private allSources: any[] = [];
  private calculatePlanSubscription: Subscription;

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private mediaService: MzMediaService,
    private dragulaService: DragulaService,
    private authService: AuthService,
    private commonService: CommonService,
    private globalService: GlobalService,
    private userService: UserService,
    private planService: PlanService) { }

  public ngOnInit() {
    this.commonService.setTitle('Planning');
    this.isLoading = true;

    this.authService.auth.takeWhile(() => this.isAlive).subscribe(claims => {
      this.isLoggedIn = claims ? true : false;
      if (this.isLoggedIn) {
        this.userService.user.takeWhile(() => this.isAlive).subscribe(user => this.level = user ? user.level : 0);
      }

      this.globalService.getProductOptions().subscribe(o => {
        this.products = this.allProducts = o;
        this.allProducts.forEach(product => product.quantity = 1);

        if (this.isLoggedIn) {
          this.reloadPlans();
        } else {
          const temp = localStorage.getItem('plans');
          this.plans = temp && JSON.parse(temp) ? JSON.parse(temp) : [{ products: [] }];
          this.tabChanged([{ id: 'plan-1' }]);
          this.isLoading = false;
        }
      });

      this.globalService.getSourceOptions().subscribe(o => {
        this.allSources = o;
      });
    });

    this.mediaService.isActive('gt-m').subscribe(result => {
      if (this.dragulaService.find(this.dragulaContainer))
        this.dragulaService.destroy(this.dragulaContainer);
      this.dragulaService.setOptions(this.dragulaContainer, {
        moves: (el, container, handle) => result ? el.className.indexOf('no-drag') === -1 : false
      });
    });
    this.dragulaService.dropModel.subscribe(value => this.calculatePlan(this.activePlan));
  }

  public ngOnDestroy() {
    this.dragulaService.destroy(this.dragulaContainer);
    this.isAlive = false;
  }

  @HostListener('window:beforeunload', ['$event'])
  public beforeUnloadHander(event) {
    localStorage.setItem('plans', JSON.stringify(this.plans));
  }

  public keywordChanged(keyword: string): void {
    this.keyword = keyword;
    this.applyFilter();
  }

  // HACK: Using lambda for M.css to recognize 'this' context
  // HACK: Extract the index from DOM output by M.css
  // HACK: Id auto generaed by M.css from Tab name (Plan 1, Plan 2...)
  public tabChanged = (tabDom: any) => {
    const index = +tabDom[0].id.split('-')[1];
    if (index) {
      this.activePlan = this.plans[index - 1];
      this.applyFilter();
    }
  }

  private applyFilter(): void {
    this.products = this.allProducts
      .filter(m => this.excludeSources.indexOf(m.group) === -1)
      .filter(o => o.name.toLowerCase().indexOf(this.keyword.toLowerCase()) > -1)
      .filter(o => this.activePlan.products.every(m => m.key !== o.key));
  }

  public mouseScrolled(planProduct: any, $event: any): boolean {
    if ($event.deltaY < 0)
      this.changeQuantity(planProduct, 1);
    else if ($event.deltaY > 0)
      this.changeQuantity(planProduct, -1);
    return false;
  }

  public addPlanProduct(product: any): void {
    this.activePlan.products.push(product);
    this.applyFilter();
    this.calculatePlan(this.activePlan);
  }

  public removePlanProduct(planProduct: any): void {
    const index = this.activePlan.products.indexOf(planProduct);
    this.activePlan.products.splice(index, 1);
    this.applyFilter();
    this.calculatePlan(this.activePlan);
  }

  public changeQuantity(planProduct: any, delta: number): void {
    planProduct.quantity += delta;
    if (planProduct.quantity < 0)
      planProduct.quantity = 0;
    if (planProduct.quantity > 99)
      planProduct.quantity = 99;
    this.calculatePlan(this.activePlan);
  }

  // HACK: Force reload to re-initialize mz-tab
  public newPlan(): void {
    this.isLoading = true;
    this.changeDetectorRef.detectChanges();
    const newPlan = { products: [] };
    const obs = this.isLoggedIn ? this.planService.insertPlan(newPlan) : Observable.of(newPlan);
    obs.subscribe(o => {
      this.plans.push(o);
      this.activePlan = o;
      this.tabChanged([{ id: `plan-${this.plans.length}` }]);
      this.commonService.showToast('New plan created');
      this.isLoading = false;
    }, error => this.isLoading = false);
  }

  public deletePlanWithConfirmation(plan: any): void {
    if (plan.products && plan.products.length > 0) {
      this.commonService.showModal('Delete Plan', 'Are you sure you want to delete this plan?', '', 'YN', result => {
        if (!result) return;
        this.deletePlan(plan);
      });
    } else {
      this.deletePlan(plan);
    }
  }

  // HACK: Force reload to re-initialize mz-tab
  private deletePlan(plan: any): void {
    this.isLoading = true;
    this.changeDetectorRef.detectChanges();
    const deletedIndex = this.plans.indexOf(plan);
    const obs = this.isLoggedIn ? this.planService.deletePlan(plan) : Observable.of({});
    obs.subscribe(() => {
      this.plans.splice(deletedIndex, 1);
      this.activePlan = this.plans[this.plans.length - 1];
      this.tabChanged([{ id: `plan-${this.plans.length}` }]);
      this.commonService.showToast('Plan deleted');
      this.isLoading = false;
    });
  }

  public reloadPlans(): void {
    this.isLoading = true;
    this.planService.getPlans(true).subscribe(plans => {
      plans.forEach(plan => {
        this.calculatePlan(plan, false);
        this.lookupProductInfo(plan.products);
      });
      if (plans.length === 0)
        plans.push({ products: [] });

      this.plans = plans;
      this.tabChanged([{ id: 'plan-1' }]);
      this.isLoading = false;
    }, error => {
      this.isLoading = false;
    });
  }

  public savePlans(): void {
    this.isLoading = true;
    Observable.forkJoin(this.plans.map(o => this.planService.updatePlan(o))).subscribe(_ => {
      this.commonService.showToast('All plans saved');
      this.isLoading = false;
    }, error => this.isLoading = false);
  }

  public calculatePlan(plan: any, cancellable: boolean = true): void {
    if (cancellable && this.calculatePlanSubscription)
      this.calculatePlanSubscription.unsubscribe();
    if (plan.products.length === 0) {
      this.isPulse = false;
      plan.result = {};
    } else {
      this.isCalculating = true;
      this.isPulse = false;
      this.calculatePlanSubscription = Observable.of({})
        .delay(400)
        .mergeMap(() => this.planService.calculatePlan(plan))
        .subscribe(o => {
          this.lookupProductInfo(o.directIngredients);
          this.lookupProductInfo(o.coreIngredients);
          this.lookupSourceInfo(o.productionTimes);
          plan.result = o;
          this.isCalculating = false;
          this.isPulse = true;
        });
    }
  }

  private lookupProductInfo(products: any[]): void {
    if (!this.allProducts) return;
    products.forEach(o => {
      const p = this.allProducts.find(m => m.key === o.key);
      o.name = p.name;
      o.icon = p.icon;
    });
  }

  private lookupSourceInfo(sources: any[]): void {
    if (!this.allSources) return;
    sources.forEach(o => {
      const p = this.allSources.find(m => m.key === o.key);
      o.name = p.name;
      o.icon = p.icon;
    });
  }
}
