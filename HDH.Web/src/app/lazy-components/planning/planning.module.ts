import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterializeModule, MzTabModule, MzMediaModule } from 'ng2-materialize';
import { DragulaModule } from 'ng2-dragula';

import { DirectiveModule } from 'app/_directive/directive.module';
import { PipeModule } from 'app/_pipe/pipe.module';
import { NestedModule } from 'app/nested-components/nested.module';
import { PlanningRoutingModule } from 'app/lazy-components/planning/planning-routing.module';
import { PlanService } from 'app/_service';
import { PlanningComponent } from 'app/lazy-components/planning/planning.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MaterializeModule.forRoot(),
    MzTabModule,
    MzMediaModule,
    DragulaModule,
    PipeModule,
    NestedModule,
    DirectiveModule,
    PlanningRoutingModule
  ],
  declarations: [PlanningComponent],
  providers: [PlanService]
})
export class PlanningModule { }
