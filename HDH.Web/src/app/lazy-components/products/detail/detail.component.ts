import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/takeWhile';
import 'rxjs/add/operator/switchMap';

import { NetworkGraphComponent } from 'app/nested-components/network-graph/network-graph.component';
import { AuthService } from 'app/_service/auth/auth.service';
import { CommonService } from 'app/_service/common.service';
import { GlobalService } from 'app/_service/http/global.service';
import { ProductService } from 'app/_service/http/product.service';
import { AppNavigation } from 'app/_shared/app-navigation';
import { Constant } from 'app/_shared/constant';
import { AppOption } from 'app/_shared/app-option';

@Component({
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit, OnDestroy {
  public isLoggedIn = false;
  public isAdmin = false;
  public isFirstLoading = false;
  public isLoading = false;
  private isAlive = true;

  public product: any;
  public navigation: AppNavigation = new AppNavigation();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService,
    private commonService: CommonService,
    private globalService: GlobalService,
    private productService: ProductService) { }

  public ngOnInit() {
    this.isFirstLoading = true;

    this.authService.auth.takeWhile(() => this.isAlive).subscribe(claims => {
      this.isLoggedIn = claims ? true : false;
      this.isAdmin = claims && claims.role.indexOf(Constant.Administrator) > -1;

      this.globalService.getProductOptions()
        .takeWhile(() => this.isAlive)
        .subscribe(o => this.navigation.all = o);

      this.route.params
        .takeWhile(() => this.isAlive)
        .do(() => this.isLoading = true)
        .switchMap(params => this.productService.getProduct(params['key']))
        .subscribe(o => {
          this.product = o;
          this.navigation.current = new AppOption({ key: o.key, name: o.name });
          this.commonService.setTitle(this.product.name);
          this.isFirstLoading = false;
          this.isLoading = false;
          window.scrollTo({ top: 0, behavior: 'smooth' });
        }, error => this.isFirstLoading = this.isLoading = false);
    });
  }

  public ngOnDestroy() {
    this.isAlive = false;
  }

  public showNetworkMap() {
    this.commonService.showNetworkGraph(NetworkGraphComponent, this.product.key);
  }

  public searchProducts(keyword: string) {
    if (keyword)
      this.router.navigate(['/product'], { queryParams: { q: keyword } });
  }

  public updateRating(product: any) {
    this.productService.updateProductPreference(product).subscribe(o => {
      this.commonService.showToast('Rating updated');
    });
  }
}
