import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { CommonService, GlobalService } from 'app/_service';
import { SourceService } from 'app/_service/http/source.service';
import { ProductService } from 'app/_service/http/product.service';
import { AppNavigation } from 'app/_shared/app-navigation';
import { AppOption } from 'app/_shared/app-option';

@Component({
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit, OnDestroy {
  public isFirstLoading = false;
  public isLoading = false;
  private isAlive = true;
  public isNew = false;

  public product: any;
  public productOptions: { dataArray: AppOption[] } = { dataArray: [] };
  public sourceOptions: { dataArray: AppOption[] } = { dataArray: [] };
  public maskOptions: any;
  public navigation: AppNavigation = new AppNavigation({ url: '../../{0}/edit' });

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private cdRef: ChangeDetectorRef,
    private commonService: CommonService,
    private globalService: GlobalService,
    private productService: ProductService,
    private sourceService: SourceService) { }

  public ngOnInit() {
    this.isFirstLoading = true;

    this.globalService.getProductOptions()
      .takeWhile(() => this.isAlive)
      .subscribe(o => {
        this.navigation.all = o;
        this.productOptions = { dataArray: o.sort((a, b) => a.key > b.key ? 1 : -1) };
      });

    this.globalService.getSourceOptions()
      .takeWhile(() => this.isAlive)
      .subscribe(o => {
        this.navigation.all = o;
        this.sourceOptions = { dataArray: o.sort((a, b) => a.key > b.key ? 1 : -1) };
      });

    this.maskOptions = {
      mask: raw => [/\d/, '.', /[0-2]/, raw[2] === '2' ? /[0-3]/ : /\d/, ':', /[0-5]/, /\d/, ':', /[0-5]/, /\d/],
      pipe: (conformedValue, config) => {
        let value = config.rawValue;
        const indexesOfPipedChars = [0, 1];
        if (/^\d{2}:\d{2}:\d{2}$/.test(value)) {
          value = '0.' + value;
          return { value: value, indexesOfPipedChars: indexesOfPipedChars };
        } else {
          return conformedValue;
        }
      },
      keepCharPositions: true,
      showMask: true
    };

    this.route.params
      .takeWhile(() => this.isAlive)
      .filter(params => {
        this.isLoading = true;
        if (params['key']) {
          this.isNew = false;
          return true;
        } else {
          this.commonService.setTitle('Create New Product');
          this.product = {
            source: {} = {},
            ingredients: [] = []
          };
          this.isNew = true;
          this.isFirstLoading = false;
          this.isLoading = false;
          return false;
        }
      })
      .switchMap(params => this.productService.getProduct(params['key']))
      .subscribe(o => {
        this.resetIngredients(o.ingredients);
        this.product = o;

        this.navigation.current = new AppOption({ key: o.key, name: o.name });
        this.commonService.setTitle(this.product.name);
        this.isFirstLoading = false;
        this.isLoading = false;
      }, error => {
        this.isFirstLoading = false;
        this.isLoading = false;
      });
  }

  public ngOnDestroy() {
    this.isAlive = false;
  }

  public removeIngredient(index: number) {
    const tmp: any[] = this.product.ingredients;
    tmp.splice(index, 1);
    this.resetIngredients(tmp);
  }

  public newIngredient() {
    const tmp = this.product.ingredients;
    tmp.push({});
    this.resetIngredients(tmp);
  }

  public update() {
    if (!this.product.name) {
      this.commonService.showToast('Product name can\'t be empty', 'warn');
      return;
    }

    this.isLoading = true;
    if (this.isNew)
      this.product.key = this.product.name.toLowerCase().replace(' ', '-');
    this.productService.updateProduct(this.product).subscribe(product => {
      this.commonService.showToast(this.isNew ? 'Product created' : 'Product updated');
      this.router.navigate(['../'], { relativeTo: this.route });
      this.isLoading = false;
    }, error => {
      this.isLoading = false;
    });
  }

  // HACK: Workaround https://github.com/sherweb/ng2-materialize/issues/182
  private resetIngredients(newValue: any) {
    if (this.product) {
      this.product.ingredients = null;
      this.cdRef.detectChanges();
      this.product.ingredients = newValue;
      this.cdRef.detectChanges();
    }
  }
}
