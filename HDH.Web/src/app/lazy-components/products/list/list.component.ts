import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/takeWhile';
import 'rxjs/add/operator/switchMap';

import { Constant } from 'app/_shared/constant';
import { AuthService } from 'app/_service/auth/auth.service';
import { CommonService } from 'app/_service/common.service';
import { UserService } from 'app/_service/http/user.service';
import { ProductService } from 'app/_service/http/product.service';
import { AppParams } from 'app/_shared/app-params';

@Component({
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit, OnDestroy {
  public isLoggedIn = false;
  public isAdmin = false;
  public isLoading = false;
  private isAlive = true;

  public params: AppParams = new AppParams();
  public products: any;
  public level: number;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService,
    private commonService: CommonService,
    private userService: UserService,
    private productService: ProductService) { }

  public ngOnInit() {
    this.commonService.setTitle('Products');
    this.isLoading = true;

    this.authService.auth.takeWhile(() => this.isAlive).subscribe(claims => {
      this.isLoggedIn = claims ? true : false;
      this.isAdmin = claims && claims.role.indexOf(Constant.Administrator) > -1;
      if (this.isLoggedIn)
        this.userService.user.takeWhile(() => this.isAlive).subscribe(user => this.level = user ? user.level : 0);

      this.route.queryParams
        .takeWhile(() => this.isAlive)
        .do(() => this.isLoading = true)
        .do(params => this.params = Constant.retrieveParams(params))
        .switchMap(params => this.productService.getProducts(this.params.q, this.params.sort, this.params.page, this.params.pageSize))
        .subscribe(o => {
          this.products = o;
          this.params = {
            q: this.params.q,
            sort: this.params.sort,
            page: o.paging.page,
            pageSize: o.paging.pageSize
          };
          this.isLoading = false;
        }, error => this.isLoading = false);
    });
  }

  public ngOnDestroy() {
    this.isAlive = false;
  }

  public keywordChanged(keyword: string) {
    this.params.q = keyword || '';
    this.router.navigate([], { queryParams: Constant.setupParams(this.params) });
  }

  public updateRating(product: any) {
    this.productService.updateProductPreference(product).subscribe(o => {
      this.commonService.showToast('Rating updated');
    });
  }

  public setupFilter(expression: string): AppParams {
    return Constant.setupParams(this.params, 'q', (this.params['q'] ? this.params['q'] + ',' : '') + (expression || ''));
  }
}
