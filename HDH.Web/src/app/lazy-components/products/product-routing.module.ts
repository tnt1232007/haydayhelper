import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminGuard } from 'app/_guard';
import { ListComponent } from 'app/lazy-components/products/list/list.component';
import { DetailComponent } from 'app/lazy-components/products/detail/detail.component';
import { FormComponent } from 'app/lazy-components/products/form/form.component';

const routes: Routes = [
  { path: '', component: ListComponent },
  { path: 'new', component: FormComponent, canActivate: [AdminGuard] },
  { path: ':key/edit', component: FormComponent, canActivate: [AdminGuard] },
  { path: ':key', component: DetailComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductRoutingModule { }
