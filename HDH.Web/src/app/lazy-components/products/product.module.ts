import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterializeModule } from 'ng2-materialize';

import { ProductService } from 'app/_service/http/product.service';
import { SourceService } from 'app/_service/http/source.service';
import { PipeModule } from 'app/_pipe/pipe.module';
import { NestedModule } from 'app/nested-components/nested.module';
import { ProductRoutingModule } from 'app/lazy-components/products/product-routing.module';
import { ListComponent } from 'app/lazy-components/products/list/list.component';
import { DetailComponent } from 'app/lazy-components/products/detail/detail.component';
import { FormComponent } from 'app/lazy-components/products/form/form.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MaterializeModule.forRoot(),
    PipeModule,
    NestedModule,
    ProductRoutingModule
  ],
  declarations: [ListComponent, DetailComponent, FormComponent],
  providers: [ProductService, SourceService]
})
export class ProductModule { }
