import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfileComponent } from 'app/lazy-components/profile/profile.component';

const routes: Routes = [
  { path: '', component: ProfileComponent },
  { path: ':uid', component: ProfileComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule { }
