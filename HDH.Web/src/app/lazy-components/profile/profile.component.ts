import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/takeWhile';

import { CommonService } from 'app/_service/common.service';
import { GlobalService } from 'app/_service/http/global.service';
import { UserService } from 'app/_service/http/user.service';
import { AppOption } from 'app/_shared/app-option';

@Component({
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {
  public isLoading = false;
  private isAlive = true;

  public dobOptions: Pickadate.DateOptions;
  public countryOptions: { dataObservable: Observable<AppOption[]> };
  public user: any;

  constructor(
    private route: ActivatedRoute,
    private commonService: CommonService,
    private globalService: GlobalService,
    private userService: UserService) { }

  public ngOnInit() {
    this.isLoading = true;
    this.commonService.setTitle('Profile');

    this.userService.user.takeWhile(() => this.isAlive).subscribe(user => {
      this.user = user;
      this.isLoading = false;
    });
    this.dobOptions = {
      max: new Date(),
      today: '',
      clear: '',
      close: ''
    };
    this.countryOptions = { dataObservable: this.globalService.getCountryOptions() };
  }

  public ngOnDestroy() {
    this.isAlive = false;
  }

  public update(): any {
    return this.userService.updateUser(this.user).map(user => {
      this.commonService.showToast('Profile updated');
    });
  }
}
