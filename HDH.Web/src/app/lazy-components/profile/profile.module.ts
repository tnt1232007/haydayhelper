import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterializeModule } from 'ng2-materialize';

import { DirectiveModule } from 'app/_directive/directive.module';
import { NestedModule } from 'app/nested-components/nested.module';
import { ProfileRoutingModule } from 'app/lazy-components/profile/profile-routing.module';
import { ProfileComponent } from 'app/lazy-components/profile/profile.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MaterializeModule.forRoot(),
    NestedModule,
    DirectiveModule,
    ProfileRoutingModule
  ],
  declarations: [ProfileComponent]
})
export class ProfileModule { }
