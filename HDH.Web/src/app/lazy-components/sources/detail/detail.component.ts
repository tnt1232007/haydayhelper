import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/takeWhile';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/observable/combineLatest';
import { BaseChartDirective } from 'ng2-charts';

import { AuthService } from 'app/_service/auth/auth.service';
import { CommonService } from 'app/_service/common.service';
import { GlobalService } from 'app/_service/http/global.service';
import { SourceService } from 'app/_service/http/source.service';
import { ProductService } from 'app/_service/http/product.service';
import { ChartjsWrapper } from 'app/_wrapper/chartjs-wrapper';
import { Constant } from 'app/_shared/constant';
import { AppNavigation } from 'app/_shared/app-navigation';
import { AppOption } from 'app/_shared/app-option';
import { AppParams } from 'app/_shared/app-params';

@Component({
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit, OnDestroy {
  @ViewChild(BaseChartDirective) public chart: BaseChartDirective;

  public isLoggedIn = false;
  public isAdmin = false;
  public isFirstLoading = false;
  public isLoading = false;
  private isAlive = true;

  public source: any;
  public products: any[];
  public params: AppParams = new AppParams({ sort: '' });
  public navigation: AppNavigation = new AppNavigation();

  public priceChartInfo: ChartjsWrapper = new ChartjsWrapper(null);
  public profitChartInfo: ChartjsWrapper = new ChartjsWrapper(null);
  public timeChartInfo: ChartjsWrapper = new ChartjsWrapper(null);

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService,
    private commonService: CommonService,
    private globalService: GlobalService,
    private sourceService: SourceService,
    private productService: ProductService) { }

  public ngOnInit() {
    this.isFirstLoading = true;

    this.priceChartInfo.addDataSet(0, null, 'Price');
    this.profitChartInfo.addDataSet(1, null, 'Profit/minute');
    this.timeChartInfo.addDataSet(0, null, 'Production Time');

    this.authService.auth.takeWhile(() => this.isAlive).subscribe(claims => {
      this.isLoggedIn = claims ? true : false;
      this.isAdmin = claims && claims.role.indexOf(Constant.Administrator) > -1;

      this.globalService.getSourceOptions()
        .takeWhile(() => this.isAlive)
        .subscribe(o => this.navigation.all = o);

      Observable.combineLatest(this.route.params, this.route.queryParams, (routeParams, queryParams) => ({ routeParams, queryParams }))
        .takeWhile(() => this.isAlive)
        .do(() => this.isLoading = true)
        .do(o => this.params.sort = o.queryParams['sort'] || '')
        .switchMap(o => Observable.forkJoin(this.sourceService.getSource(o.routeParams['key']), this.productService.getProductsBySource(o.routeParams['key'], this.params.sort)))
        .subscribe(([source, products]) => {
          this.source = source;
          this.navigation.current = new AppOption({ key: source.key, name: source.name });
          this.commonService.setTitle(this.source.name);
          this.isFirstLoading = false;

          const chartProducts = products.items.filter(n => n.profit);
          this.priceChartInfo.setLabels(chartProducts.map(n => n.name));
          this.priceChartInfo.setData(0, chartProducts.map(n => n.maxPrice));
          this.profitChartInfo.setLabels(chartProducts.map(n => n.name));
          this.profitChartInfo.setData(0, chartProducts.map(n => n.profit.toFixed(2)));
          this.timeChartInfo.setLabels(chartProducts.map(n => n.name));
          this.timeChartInfo.setData(0, chartProducts.map(n => n.realProductionTimeTicks / 600000000));
          this.priceChartInfo.setColors();
          this.products = products.items;
          this.isLoading = false;
        }, error => {
          this.isFirstLoading = false;
          this.isLoading = false;
        });
    });
  }

  public ngOnDestroy() {
    this.isAlive = false;
  }

  public searchProducts(keyword: string) {
    if (keyword)
      this.router.navigate(['/product'], { queryParams: { q: keyword } });
  }

  public updateRating(product: any) {
    this.productService.updateProductPreference(product).subscribe(o => {
      this.commonService.showToast('Rating updated');
    });
  }

  public updateMastery(source: any) {
    this.sourceService.updateSourcePreference(source).subscribe(o => {
      this.commonService.showToast('Mastery updated');
    });
  }
}
