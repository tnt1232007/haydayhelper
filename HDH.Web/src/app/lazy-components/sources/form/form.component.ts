import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { CommonService, GlobalService } from 'app/_service';
import { SourceService } from 'app/_service/http/source.service';
import { ProductService } from 'app/_service/http/product.service';
import { AppNavigation } from 'app/_shared/app-navigation';
import { AppOption } from 'app/_shared/app-option';

@Component({
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit, OnDestroy {
  public isFirstLoading = false;
  public isLoading = false;
  private isAlive = true;
  public isNew = false;

  public source: any;
  public maskOptions: any;
  public navigation: AppNavigation = new AppNavigation({url: '../../{0}/edit'});

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private cdRef: ChangeDetectorRef,
    private commonService: CommonService,
    private globalService: GlobalService,
    private sourceService: SourceService,
    private productService: ProductService) { }

  public ngOnInit() {
    this.isFirstLoading = true;

    this.globalService.getSourceOptions()
      .takeWhile(() => this.isAlive)
      .subscribe(o => this.navigation.all = o);

    this.maskOptions = {
      mask: raw => [/\d/, '.', /[0-2]/, raw[2] === '2' ? /[0-3]/ : /\d/, ':', /[0-5]/, /\d/, ':', /[0-5]/, /\d/],
      pipe: (conformedValue, config) => {
        let value = config.rawValue;
        const indexesOfPipedChars = [0, 1];
        if (/^\d{2}:\d{2}:\d{2}$/.test(value)) {
          value = '0.' + value;
          return { value: value, indexesOfPipedChars: indexesOfPipedChars };
        } else {
          return conformedValue;
        }
      },
      keepCharPositions: true,
      showMask: true
    };

    this.route.params
      .takeWhile(() => this.isAlive)
      .filter(params => {
        this.isLoading = true;
        if (params['key']) {
          this.isNew = false;
          return true;
        } else {
          this.commonService.setTitle('Create New Source');
          this.source = {
            sourceInstances: [] = [{}],
            products: [] = []
          };
          this.isNew = true;
          this.isFirstLoading = false;
          this.isLoading = false;
          return false;
        }
      })
      .switchMap(params => Observable.forkJoin(this.sourceService.getSource(params['key']), this.productService.getProductsBySource(params['key'])))
      .subscribe(([source, products]) => {
        this.resetInstances(source.sourceInstances);
        this.source = source;
        this.navigation.current = new AppOption({ key: source.key, name: source.name });
        this.commonService.setTitle(this.source.name);
        this.source.products = products.items;
        this.isFirstLoading = false;
        this.isLoading = false;
      }, error => {
        this.isFirstLoading = false;
        this.isLoading = false;
      });
  }

  public ngOnDestroy() {
    this.isAlive = false;
  }

  public removeInstance(index: number) {
    const tmp: any[] = this.source.sourceInstances;
    tmp.splice(index, 1);
    this.resetInstances(tmp);
  }

  public newInstance() {
    const tmp = this.source.sourceInstances;
    tmp.push({});
    this.resetInstances(tmp);
  }

  public update() {
    if (!this.source.name) {
      this.commonService.showToast('Source name can\'t be empty', 'warn');
      return;
    }

    this.isLoading = true;
    if (this.isNew)
      this.source.key = this.source.name.toLowerCase().replace(' ', '-');
    this.sourceService.updateSource(this.source).subscribe(source => {
      this.commonService.showToast(this.isNew ? 'Source created' : 'Source updated');
      this.router.navigate(['../'], { relativeTo: this.route });
      this.isLoading = false;
    }, error => {
      this.isLoading = false;
    });
  }

  // HACK: Workaround https://github.com/sherweb/ng2-materialize/issues/182
  private resetInstances(newValue: any) {
    if (this.source) {
      this.source.sourceInstances = null;
      this.cdRef.detectChanges();
      this.source.sourceInstances = newValue;
      this.cdRef.detectChanges();
    }
  }
}
