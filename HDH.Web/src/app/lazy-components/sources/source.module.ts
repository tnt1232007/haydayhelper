import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterializeModule } from 'ng2-materialize';
import { ChartsModule } from 'ng2-charts';

import { SourceService } from 'app/_service/http/source.service';
import { ProductService } from 'app/_service/http/product.service';
import { NestedModule } from 'app/nested-components/nested.module';
import { SourceRoutingModule } from 'app/lazy-components/sources/source-routing.module';
import { ListComponent } from 'app/lazy-components/sources/list/list.component';
import { DetailComponent } from 'app/lazy-components/sources/detail/detail.component';
import { FormComponent } from 'app/lazy-components/sources/form/form.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MaterializeModule.forRoot(),
    ChartsModule,
    NestedModule,
    SourceRoutingModule
  ],
  declarations: [ListComponent, DetailComponent, FormComponent],
  providers: [SourceService, ProductService]
})
export class SourceModule { }
