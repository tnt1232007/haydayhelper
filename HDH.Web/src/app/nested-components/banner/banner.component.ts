import { Component, Input, Output, EventEmitter, OnInit, OnChanges } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';

import { Constant } from 'app/_shared/constant';
import { AppNavigation } from 'app/_shared/app-navigation';
import { AppParams } from 'app/_shared/app-params';
import 'app/_shared/string-ext';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss']
})
export class BannerComponent implements OnInit, OnChanges {
  @Input() public keyword = '';
  @Input() public title: string;
  @Input() public imageSrc: string;
  @Input() public isLoading: boolean;
  @Input() public navigation: AppNavigation;
  @Input() public params: AppParams;
  @Input() public placeholder: string;
  @Input() public focusEvent: EventEmitter<boolean>;
  @Output() public keywordChanged = new EventEmitter<string>();

  public SWIPE_ACTION = { LEFT: 'swipeleft', RIGHT: 'swiperight', UP: 'swipeup', DOWN: 'swipedown' };
  public keywordInput = new FormControl();
  public setupParams: (p: AppParams, key?: string, value?: any) => AppParams;

  constructor(private router: Router) {
    this.setupParams = Constant.setupParams;
  }

  public ngOnInit() {
    this.keywordInput.valueChanges
      .debounceTime(400)
      .filter(o => this.keyword !== null)
      .subscribe(o => this.keywordChanged.emit(this.keyword));
  }

  public ngOnChanges(changes: any) {
    if (this.navigation && this.navigation.all && this.navigation.current)
      this.navigate(this.navigation.current.key);
  }

  public keywordChanged_(keyword) {
    this.keywordChanged.emit(keyword);
  }

  public navigate(key: string) {
    const currIndex = this.navigation.all.findIndex(o => o.key === key);
    this.imageSrc = this.imageSrc.replace(this.navigation.current.key, key);
    this.title = this.navigation.all[currIndex].name;
    this.navigation.current.key = key;
    this.navigation.previous = this.navigation.all[currIndex - 1];
    this.navigation.next = this.navigation.all[currIndex + 1];
  }

  public swipe(direction: string) {
    switch (direction) {
      case this.SWIPE_ACTION.LEFT:
        if (this.navigation && this.navigation.next) {
          this.navigate(this.navigation.next.key);
          this.router.navigate([this.navigation.url.format(this.navigation.next.key)]);
        }
        break;
      case this.SWIPE_ACTION.RIGHT:
        if (this.navigation && this.navigation.previous) {
          this.navigate(this.navigation.previous.key);
          this.router.navigate([this.navigation.url.format(this.navigation.previous.key)]);
        }
        break;
      default:
        break;
    }
  }
}
