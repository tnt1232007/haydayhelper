import { Component, EventEmitter } from '@angular/core';

import { MzBaseModal } from 'ng2-materialize';

@Component({
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent extends MzBaseModal {
  public icon: string;
  public header: string;
  public content: string;
  public footerType: string;
  public callback: (result: boolean) => void;
}
