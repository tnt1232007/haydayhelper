import { Component, Input, OnInit, OnChanges } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-field',
  templateUrl: './field.component.html',
  styleUrls: ['./field.component.scss']
})
export class FieldComponent implements OnInit, OnChanges {
  @Input() public obj: any;
  @Input() public type: string;
  @Input() public prop: string;
  @Input() public label: string;
  @Input() public isLoading: boolean;
  @Input() public options: any;
  @Input() private update: Observable<any>;

  public tmpValue: any;

  constructor() { }

  public ngOnInit() {
    this.type = this.type || 'text';
    switch (this.type) {
      case 'select':
        if (this.options && this.options.dataObservable) {
          this.isLoading = true;
          this.options.dataObservable.subscribe(o => {
            this.options.dataArray = o;
            this.isLoading = false;
          });
        }
        break;
      case 'autocomplete':
        if (this.options && this.options.dataObservable) {
          this.isLoading = true;
          this.options.dataObservable.subscribe(o => {
            const data = {};
            o.forEach(option => {
              data[option.name] = option.icon;
            });
            this.options = {
              data: data,
              onAutocomplete: value => this.tmpValue = value,
              minLength: 0
            };
            this.isLoading = false;
          });
        }
        break;
      case 'date':
      case 'mask':
      case 'custom':
      default:
        break;
    }
  }

  public ngOnChanges(changes: any) {
    if (changes.obj)
      this.tmpValue = this.obj[this.prop];
  }

  public isModified(): boolean {
    if (!this.update) {
      this.obj[this.prop] = this.tmpValue;
      return false;
    }
    return JSON.stringify(this.obj[this.prop]) !== JSON.stringify(this.tmpValue);
  }

  public accept(): boolean {
    this.isLoading = true;
    this.obj[this.prop] = this.tmpValue;
    this.update.subscribe(o => {
      this.isLoading = false;
    });
    return false;
  }

  public cancel(): boolean {
    this.tmpValue = this.obj[this.prop];
    return false;
  }
}
