import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MaterializeModule, MzToastService } from 'ng2-materialize';
import { ChartsModule } from 'ng2-charts';
import { TextMaskModule } from 'angular2-text-mask';

import { DirectiveModule } from 'app/_directive/directive.module';
import { DialogComponent } from 'app/nested-components/dialog/dialog.component';
import { BannerComponent } from 'app/nested-components/banner/banner.component';
import { FieldComponent } from 'app/nested-components/field/field.component';
import { SortHeaderComponent } from 'app/nested-components/table/sort-header.component';
import { TableComponent } from 'app/nested-components/table/table.component';
import { PagingComponent } from 'app/nested-components/paging/paging.component';
import { RatingComponent } from 'app/nested-components/rating/rating.component';
import { NetworkGraphComponent } from 'app/nested-components/network-graph/network-graph.component';
import { UsageChartComponent } from 'app/nested-components/usage-chart/usage-chart.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MaterializeModule.forRoot(),
    ChartsModule,
    TextMaskModule,
    DirectiveModule
  ],
  declarations: [DialogComponent, BannerComponent, FieldComponent, SortHeaderComponent, TableComponent, PagingComponent, RatingComponent, UsageChartComponent, NetworkGraphComponent],
  exports: [DialogComponent, BannerComponent, FieldComponent, SortHeaderComponent, TableComponent, PagingComponent, RatingComponent, UsageChartComponent, NetworkGraphComponent],
  entryComponents: [DialogComponent, UsageChartComponent, NetworkGraphComponent]
})
export class NestedModule { }
