import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { MzBaseModal } from 'ng2-materialize';

import { TapTargetDirective } from 'app/_directive/tap-target.directive';
import { VisjsWrapper } from 'app/_wrapper/visjs-wrapper';
import { ChartService } from 'app/_service/http/chart.service';

@Component({
  templateUrl: './network-graph.component.html',
  styleUrls: ['./network-graph.component.scss']
})
export class NetworkGraphComponent extends MzBaseModal implements OnInit {
  @ViewChild('modal') private modal: any;
  @ViewChild('menu') private menu: any;
  @ViewChild(TapTargetDirective) private tapTarget: TapTargetDirective;

  private nodes: any[];
  private edges: any[];
  private highlightState = false;

  public graph: VisjsWrapper;
  public productKey: string;
  public selectedProduct: string;
  public showTapTarget: boolean;
  public progress = 0;

  constructor(
    private router: Router,
    private chartService: ChartService) {
    super();
  }

  public ngOnInit() {
    this.initialize();
    this.chartService.calcProductRelationship().subscribe(o => {
      this.nodes = o.nodes.map(m => ({ id: m.key, title: m.name, image: 'assets/products/' + m.key + '.png', group: m.sourceKey }));
      this.edges = o.edges.map(m => ({
        from: m.ingredientKey,
        to: m.productKey,
        _label: m.quantity === 0 ? '' : parseFloat(m.quantity.toFixed(2)),
        dashes: m.quantity === 0,
        arrows: m.quantity === 0 ? '' : 'to',
        chosen: m.quantity === 0 ? { edge: n => n.borderWidth = 1 } : {}
      }));
      this.optionChanged();
      this.dataChanged();
    });
  }

  public getTitle(id: string): string {
    if (this.nodes)
      return this.nodes.find(o => o.id === id).title;
    return '';
  }

  public navigateTo(key: string) {
    if (key === this.productKey || (!key && !this.productKey)) {
      // Go no where
      this.graph.zoomFocus(this.productKey || this.nodes[Math.floor(Math.random() * this.nodes.length)].id);
      return;
    } else if (!key) {
      // Go Home
      this.initialize();
      this.router.navigate(['/']);
    } else if (!this.productKey) {
      // Go from Home
      this.productKey = key;
      this.initialize();
      this.router.navigate(['/product', key]);
    } else {
      // Go between Product
      this.router.navigate(['/product', key]);
    }

    this.productKey = key;
    this.optionChanged();
    this.dataChanged();
  }

  private initialize() {
    this.selectedProduct = this.productKey;
    this.graph = new VisjsWrapper(document.getElementById('network-graph'), 'circularImage');

    this.graph.setEvent('select', o => this.selectedProduct = o.nodes.length > 0 ? o.nodes[0] : this.productKey);
    this.graph.setEvent('oncontext', o => {
      const target = this.graph.getNodeAt(o.pointer.DOM.x, o.pointer.DOM.y);
      if (target) {
        this.graph.setSelectedNode(target);
        this.selectedProduct = target.toString();
      }
      this.menu.open();
      setTimeout(m => {
        document.getElementById(this.menu.id).style.left = o.pointer.DOM.x + 'px';
        document.getElementById(this.menu.id).style.top = o.pointer.DOM.y + 'px';
      }, 50);
    });
    this.graph.setEvent('stabilizationProgress', o => this.progress = o.iterations * 100 / o.total);
    this.graph.setEvent('stabilizationIterationsDone', () => {
      this.progress = 100;
      if (this.showTapTarget) {
        this.tapTarget.open();
        this.showTapTarget = false;
      }
      setTimeout(o => {
        if (this.selectedProduct && this.selectedProduct === this.productKey)
          this.graph.zoomFocus(this.selectedProduct);
      }, 500);
    });
    this.graph.setEvent('doubleClick', o => {
      if (o.nodes.length > 0)
        this.navigateTo(o.nodes[0]);
    });
  }

  private optionChanged() {
    if (!this.productKey) {
      const options = {
        nodes: {
          size: 20,
          shadow: false,
          chosen: {
            node: o => {
              o.size = 30;
            }
          }
        },
        edges: {
          smooth: {
            enabled: true,
            type: 'continuous',
            roundness: 0.5
          },
          chosen: {
            edge: o => {
              o.width = 4;
            }
          }
        },
        layout: {
          improvedLayout: false
        },
        physics: {
          barnesHut: {
            gravitationalConstant: -64000,
            springLength: 320
          }
        }
      };
      this.graph.setOptions(options);
      this.graph.setEvent('select', o => {
        const nodes = this.graph.getData();
        if (o.nodes.length > 0) {
          const selectedNode = o.nodes[0];

          for (const id in nodes) {
            if (!nodes.hasOwnProperty(id))
              continue;
            nodes[id].color = 'rgba(200,200,200,0.5)';
            nodes[id].shape = 'dot';
            nodes[id].label = undefined;
          }

          const relativeNodes = this.nodes.filter(m => m.group === nodes[selectedNode].group).map(n => n.id);
          const firstLevelNodes = this.graph.getConnectedNodes(selectedNode);
          const secondLevelNodes = firstLevelNodes.map(m => this.graph.getConnectedNodes(m)).reduce((a, b) => a.concat(b));

          secondLevelNodes.forEach(m => {
            nodes[m].color = 'rgba(150,150,150,0.75)';
            nodes[m].shape = 'dot';
            nodes[m].label = nodes[m].title;
          });

          firstLevelNodes.forEach(m => {
            nodes[m].color = undefined;
            nodes[m].shape = 'circularImage';
            nodes[m].label = nodes[m].title;
          });

          relativeNodes.forEach(m => {
            nodes[m].color = undefined;
            nodes[m].shape = 'circularImage';
            nodes[m].label = nodes[m].title;
          });

          nodes[selectedNode].color = undefined;
          nodes[selectedNode].shape = 'circularImage';
          nodes[selectedNode].label = nodes[selectedNode].title;

          this.highlightState = true;
        } else if (this.highlightState) {
          for (const id in nodes) {
            if (!nodes.hasOwnProperty(id))
              continue;
            nodes[id].color = undefined;
            nodes[id].shape = 'circularImage';
            nodes[id].label = undefined;
          }
          this.highlightState = false;
        }
        this.graph.updateData(nodes);
      });
    }
  }

  private dataChanged() {
    if (!this.nodes || !this.edges)
      return;

    if (this.productKey) {
      const relatedIngredients = this.edges.filter(o => o.from === this.productKey).map(o => o.to)
        .concat(this.edges.filter(o => o.to === this.productKey).map(o => o.from))
        .concat(this.productKey);
      const relatedSource = this.nodes.find(o => o.id === this.productKey).group;
      this.edges.forEach(o => o.label = o._label);
      this.graph.setData(
        this.nodes.filter(o => relatedIngredients.some(m => m === o.id) || o.group === relatedSource),
        this.edges.filter(o => o.from === this.productKey || o.to === this.productKey || o.dashes)
      );
    } else {
      this.edges.forEach(o => o.label = '');
      this.graph.setData(this.nodes.filter(o => o.group !== 'special'), this.edges);
    }
  }
}
