import { Component, Input, Output, OnChanges, EventEmitter } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { AnonymousSubject } from 'rxjs/Subject';

import { Constant } from 'app/_shared/constant';

@Component({
  selector: 'app-paging',
  templateUrl: './paging.component.html',
  styleUrls: ['./paging.component.scss']
})
export class PagingComponent implements OnChanges {
  @Input() public id: string;
  @Input() public total: number;
  @Input() public params: any;

  public page: number;
  public pageSize: number;
  public pagesShow = 7;
  public pager: any = {};
  public setupParams: any;

  constructor() {
    this.setupParams = Constant.setupParams;
  }

  public ngOnChanges(changes: any) {
    this.page = +this.params.page;
    this.pageSize = +this.params.pageSize;
    this.pager = this.getPager(this.total, this.page, this.pageSize);
  }

  public isFirst(page: number): boolean {
    return page === 1;
  }

  public isLast(page: number): boolean {
    return page === this.pager.totalPages;
  }

  public isCurrent(page: number): boolean {
    return page === this.pager.page;
  }

  public isShow(page: number): boolean {
    if (this.isFirst(page) || this.isLast(page))
      return true;
    if (this.pager.page <= Math.ceil(this.pagesShow / 2))
      return page <= this.pagesShow - 1;
    else if (this.pager.page >= this.pager.totalPages - Math.ceil(this.pagesShow / 2) + 1)
      return page >= this.pager.totalPages - this.pagesShow + 2;
    else
      return Math.abs(page - this.pager.page) <= Math.ceil(this.pagesShow / 2) - 2;
  }

  public isShowAsDot(page: number): boolean {
    if (this.isFirst(page) || this.isLast(page))
      return false;
    if (this.pager.page <= Math.ceil(this.pagesShow / 2))
      return page === this.pagesShow - 1;
    else if (this.pager.page >= this.pager.totalPages - Math.ceil(this.pagesShow / 2) + 1)
      return page === this.pager.totalPages - this.pagesShow + 2;
    else
      return Math.abs(page - this.pager.page) === Math.ceil(this.pagesShow / 2) - 2;
  }

  private getPager(total: number, page: number = 1, pageSize: number = 10) {
    if (!total || !page || !pageSize)
      return {};

    const totalPages = Math.ceil(total / pageSize) || 1;
    const pages = Array.from(Array(totalPages).keys()).map(o => o + 1);
    const pageTotal = page === totalPages ? total % pageSize : pageSize;
    const startIndex = (page - 1) * pageSize + 1;
    const endIndex = startIndex + pageSize > total ? total : startIndex + pageSize - 1;

    const pageSizeOptions = [];
    let pageSizeOption = 2.5;
    do {
      pageSizeOption *= 2;
      pageSizeOptions.push(pageSizeOption);
    } while (pageSizeOption <= total);

    return {
      total: total,
      pageSize: pageSize,
      pages: pages,
      page: page,
      totalPages: totalPages,
      pageTotal: pageTotal,
      startIndex: startIndex,
      endIndex: endIndex,
      pageSizeOptions: pageSizeOptions
    };
  }
}
