import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: RatingComponent,
      multi: true
    }]
})
export class RatingComponent implements ControlValueAccessor, OnInit {
  @Input() private max: number;
  @Input() public stateOn: string;
  @Input() public stateOff: string;
  @Input() private readonly: boolean;

  @Output() private change: EventEmitter<number> = new EventEmitter<number>();

  public range: Array<any>;
  public currValue: number;
  private preValue: number;

  private onChange = (_: any) => { };
  private onTouched = () => { };

  public ngOnInit() {
    this.max = this.max || 5;
    this.stateOn = this.stateOn || 'star';
    this.stateOff = this.stateOff || 'star_border';

    this.range = [];
    for (let i = 0; i < this.max; i++) {
      this.range.push({});
    }
  }

  public writeValue(value: number) {
    this.preValue = value;
    this.currValue = value;
  }

  public select(value: number) {
    if (!this.readonly && value >= 0 && value <= this.range.length) {
      this.writeValue(value);
      this.onChange(value);
      this.change.emit(value);
    }
  }

  public enter(value: number) {
    if (!this.readonly) {
      this.currValue = value;
    }
  }

  public leave() {
    this.currValue = this.preValue;
  }

  public registerOnChange(fn: (_: any) => {}) {
    this.onChange = fn;
  }

  public registerOnTouched(fn: () => {}) {
    this.onTouched = fn;
  }
}
