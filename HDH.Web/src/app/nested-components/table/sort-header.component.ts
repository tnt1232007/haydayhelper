import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Constant } from 'app/_shared/constant';
import { AppParams } from 'app/_shared/app-params';

@Component({
  selector: 'app-sort-header',
  templateUrl: './sort-header.component.html',
  styleUrls: ['./sort-header.component.scss']
})
export class SortHeaderComponent implements OnInit {
  @Input() public field: string;
  @Input() public header: string;
  @Input() public disabled: boolean;
  @Input() public params: AppParams;

  public setupParams: (p: AppParams, key?: string, value?: any) => AppParams;

  constructor() {
    this.setupParams = Constant.setupParams;
  }

  public ngOnInit() {
    this.header = this.header || this.field.charAt(0).toUpperCase() + this.field.slice(1);
  }

  public isSortBy(field): boolean {
    if (!this.params.sort)
      return false;
    const sort = this.readSortString(this.params.sort);
    return sort.field.toLowerCase() === field.toLowerCase();
  }

  public isSortAscending(field): boolean {
    if (!this.isSortBy(field))
      return false;
    const sort = this.readSortString(this.params.sort);
    return sort.isAscending;
  }

  private readSortString(sort: string) {
    if (sort) {
      const paramsSort = sort.split(' ');
      return { field: paramsSort[0], isAscending: paramsSort.length === 1 };
    }
  }
}
