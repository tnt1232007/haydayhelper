import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AppParams } from 'app/_shared/app-params';
import { AppPaging } from 'app/_shared/app-paging';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  @Input() public type: string;
  @Input() public data: any[];
  @Input() public highlight: string;
  @Input() public isLoading: boolean;
  @Input() public isLoggedIn: boolean;
  @Input() public paging: AppPaging;
  @Input() public params: AppParams;

  @Output() private update: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  public ngOnInit() { }

  public onUpdate(item: any) {
    this.update.emit(item);
  }
}
