import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { MzBaseModal, MzMediaService } from 'ng2-materialize';

import { TapTargetDirective } from 'app/_directive/tap-target.directive';
import { ChartjsWrapper } from 'app/_wrapper/chartjs-wrapper';
import { GlobalService } from 'app/_service/http/global.service';
import { ChartService } from 'app/_service/http/chart.service';

@Component({
  templateUrl: './usage-chart.component.html',
  styleUrls: ['./usage-chart.component.scss']
})
export class UsageChartComponent extends MzBaseModal implements OnInit {
  @ViewChild('modal') private modal: any;
  @ViewChild('menu') private menu: any;
  @ViewChild(TapTargetDirective) private tapTarget: TapTargetDirective;

  public productChart: ChartjsWrapper;
  public sources: any[];
  public products: any[];

  public selectedSource: any;
  public selectedProduct: any;
  public showTapTarget: boolean;
  public sortBy = 'time';
  public showTop = 20;
  public checkState = true;
  public isLoading = false;

  private excludeSources: string[] = [
    'special', 'feed-mill', 'animal-product', 'smelter', 'mine',
    'maintenance', 'dock', 'net-maker', 'lure-workbench', 'duck-salon', 'lobster-pool'
  ];
  private specialSources: string[] = ['field', 'tree-or-bush'];

  constructor(
    private router: Router,
    public mediaService: MzMediaService,
    private globalService: GlobalService,
    private chartService: ChartService) {
    super();
  }

  public ngOnInit() {
    this.isLoading = true;
    this.productChart = new ChartjsWrapper(null);
    this.productChart.addYAxes('left', 'Used Count (time)');
    this.productChart.addYAxes('right', 'Required time (hours)');
    this.productChart.addDataSet(0, 'bar', 'Used Count', 'times as ingredients');
    this.productChart.addDataSet(1, 'line', 'Required Time', 'hours to produced');

    this.globalService.getSourceOptions().subscribe(sources => {
      sources = sources.filter(o => this.excludeSources.indexOf(o.key) === -1);
      sources.forEach(o => o.checked = this.specialSources.indexOf(o.key) === -1);
      this.sources = sources;

      this.chartService.calcProductUsage().subscribe(products => {
        this.products = products.filter(o => this.excludeSources.indexOf(o.sourceKey) === -1);
        this.sourceChanged();
        this.isLoading = false;
        if (this.showTapTarget) {
          this.tapTarget.open();
          this.showTapTarget = false;
        }
      });
    });
  }

  public getSelectedSourcesCount() {
    return this.sources ? this.sources.filter(o => o.checked === true).length : 0;
  }

  public checkAll() {
    this.sources.forEach(o => o.checked = this.checkState);
    this.sourceChanged();
  }

  public oncontext($event) {
    this.menu.open();
    setTimeout(m => {
      document.getElementById(this.menu.id).style.left = $event.x + 'px';
      document.getElementById(this.menu.id).style.top = $event.y + 'px';
    }, 50);
    this.selectedProduct = this.productChart.selectedLabel ? this.products.find(o => o.name === this.productChart.selectedLabel) : null;
    this.selectedSource = this.selectedProduct ? this.sources.find(o => o.key === this.selectedProduct.sourceKey) : null;
    $event.preventDefault();
  }

  public hideSource(key: string) {
    this.sources.find(o => o.key === this.selectedProduct.sourceKey).checked = false;
    this.sourceChanged();
  }

  public randomizeColors() {
    this.sourceChanged();
  }

  public sourceChanged() {
    const selectedSources = this.sources.filter(o => o.checked === true).map(o => o.key);
    const selectedProducts = this.products
      .filter(o => selectedSources.indexOf(o.sourceKey) > -1)
      .sort((a, b) => {
        const compare = this.sortBy === 'time' ? b.timeRequired - a.timeRequired : b.usedCount - a.usedCount;
        return compare === 0 ? a.name - b.name : compare;
      })
      .slice(0, this.showTop);
    this.productChart.setLabels(selectedProducts.map(o => o.name));
    this.productChart.setData(0, selectedProducts.map(o => o.usedCount));
    this.productChart.setData(1, selectedProducts.map(o => o.timeRequired));
    this.productChart.setColors(0);
    this.productChart.setColors(1);
    this.productChart.setLegend(true, 'bottom');
    this.productChart.setScale(1, Math.max(...selectedProducts.map(o => o.timeRequired)) * 1.2);
    this.productChart.setTooltip((tooltipItem, data) => {
      const value = parseFloat(tooltipItem.yLabel.toFixed(2));
      return ` ${data.datasets[tooltipItem.datasetIndex].label}: ${value} ${data.datasets[tooltipItem.datasetIndex].labelAfter}`;
    });
  }
}
