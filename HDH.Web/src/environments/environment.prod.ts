// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: true,
  api: 'https://tnt-haydayhelper-api.herokuapp.com',
  no_reply: 'noreply@tnt-haydayhelper.firebaseapp.com',
  title: 'Hay Day Helper',
  short_title: 'HDH',
  firebase: {
    apiKey: 'AIzaSyAx1fMhXOKPiuZwGccLtdYhIFmUyk1Pe6w',
    authDomain: 'tnt-haydayhelper.firebaseapp.com',
    databaseURL: 'https://tnt-haydayhelper.firebaseio.com',
    projectId: 'tnt-haydayhelper',
    storageBucket: 'tnt-haydayhelper.appspot.com',
    messagingSenderId: '33192763345'
  }
};
