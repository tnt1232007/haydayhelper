try
{
    $path = If ($args[0]) {$args[0]} Else {$pwd}
    $files = Get-ChildItem $path -Filter *API
    Push-Location $($files[0].Name)
    dotnet restore
    dotnet publish --configuration Release
    git push https://heroku:7fa1106f-fdb0-4afb-adf5-89c71fc45cbd@git.heroku.com/tnt-haydayhelper-api.git HEAD
}
finally
{
    Pop-Location
}