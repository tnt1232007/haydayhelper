try
{
    $path = If ($args[0]) {$args[0]} Else {$pwd}
    $files = Get-ChildItem $path -Filter *API
    Push-Location $($files[0].Name)
    $Env:ASPNETCORE_ENVIRONMENT = "Development"
    dotnet watch run
}
finally
{
    Pop-Location
}