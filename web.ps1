try
{
    $path = If ($args[0]) {$args[0]} Else {$pwd}
    $files = Get-ChildItem $path -Filter *Web
    Push-Location $($files[0].Name)
    npm i
    npm start
}
finally
{
    Pop-Location
}